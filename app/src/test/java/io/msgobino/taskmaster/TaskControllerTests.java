package io.msgobino.taskmaster;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import io.msgobino.taskmaster.data.core.Task;
import io.msgobino.taskmaster.data.core.TasksList;
import io.msgobino.taskmaster.data.modifier.TaskModifier;
import io.msgobino.taskmaster.utils.TestFilesProvider;
import io.msgobino.taskmaster.viewmodel.tasks.TaskController;

public class TaskControllerTests {
    @Test
    public void taskAdditionControl() throws IOException {
        TasksList tasks = TasksList.fromFile(TestFilesProvider.tasksParsingTestFile());

        TaskController taskController = new TaskController();
        taskController.setTasksListProvider(() -> tasks);

        Task task = new Task("This task has been created by a task controller instance");
        taskController.createTask(task);

        Assert.assertTrue(tasks.contains(task));
    }

    @Test
    public void taskRemovalControl() throws IOException {
        TasksList tasks = TasksList.fromFile(TestFilesProvider.tasksParsingTestFile());

        TaskController taskController = new TaskController();
        taskController.setTasksListProvider(() -> tasks);


        Task firstTaskOfTheList = tasks.stream()
                .findFirst()
                .orElse(Task.NULL_TASK);

        Assert.assertTrue(tasks.contains(firstTaskOfTheList));

        taskController.deleteTasks(List.of(firstTaskOfTheList));
        Assert.assertFalse(tasks.contains(firstTaskOfTheList));
    }

    @Test
    public void settingTaskCompletedControl() throws IOException {
        TasksList tasks = TasksList.fromFile(TestFilesProvider.tasksParsingTestFile());

        TaskController taskController = new TaskController();
        taskController.setTasksListProvider(() -> tasks);


        Task firstTaskOfTheList = tasks.stream()
                .findFirst()
                .orElse(Task.NULL_TASK);

        Assert.assertTrue(tasks.contains(firstTaskOfTheList));

        Task completedTask = TaskModifier.fromTask(firstTaskOfTheList).setCompleted().buildTask();
        taskController.replaceTask(firstTaskOfTheList, completedTask);
        Assert.assertTrue(tasks.contains(completedTask));
    }
}
