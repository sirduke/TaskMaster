package io.msgobino.taskmaster;

import org.junit.Assert;
import org.junit.Test;

import io.msgobino.taskmaster.data.core.Task;
import io.msgobino.taskmaster.editor.CursorInfo;
import io.msgobino.taskmaster.editor.textinjectors.TokenInjector;

public class TextInjectionTests {
    public static final String TEST_TOKEN = "token";
    public static final String ANOTHER_TEST_TOKEN = "another token";

    @Test
    public void textAppend() {
        Task task = new Task("This is a task");
        CursorInfo cursorInfo = CursorInfo.after(task.content());

        TokenInjector tokenInjector = new TokenInjector(task.content(), cursorInfo);

        int firstAppendIndex = tokenInjector.append(TEST_TOKEN);
        Assert.assertEquals(task.content() + " " + TEST_TOKEN + "  ", tokenInjector.toString());
        Assert.assertEquals(task.content().length() + TEST_TOKEN.length() + 2, firstAppendIndex);

        int secondAppendIndex = tokenInjector.append(ANOTHER_TEST_TOKEN);
        Assert.assertEquals(task.content() + "  " + ANOTHER_TEST_TOKEN + "  " + TEST_TOKEN + "  ", tokenInjector.toString());
        Assert.assertEquals(task.content().length() + ANOTHER_TEST_TOKEN.length() + 3, secondAppendIndex);
    }

    @Test
    public void textInjection() {
        String fixedTaskContent = "This is a";
        String tokenToReplace = "task";
        Task task = new Task(fixedTaskContent + " " + tokenToReplace);
        CursorInfo cursorInfo = CursorInfo.after(task.content());

        TokenInjector tokenInjector = new TokenInjector(task.content(), cursorInfo);

        int firstAppendIndex = tokenInjector.inject(TEST_TOKEN);
        Assert.assertEquals(fixedTaskContent + " " + TEST_TOKEN + "  ", tokenInjector.toString());
        Assert.assertEquals(fixedTaskContent.length() + TEST_TOKEN.length() + 2, firstAppendIndex);
    }

    @Test
    public void textInjectionBetween() {
        String prefix = "This is a";
        String tokenToReplace = " task ";
        String suffix = "whose content must be modified";
        Task task = new Task(prefix + tokenToReplace + suffix);
        CursorInfo cursorInfo = CursorInfo.after(prefix + " ");

        TokenInjector tokenInjector = new TokenInjector(task.content(), cursorInfo);

        int firstAppendIndex = tokenInjector.inject(TEST_TOKEN);
        Assert.assertEquals(prefix + " " + TEST_TOKEN + "  " + suffix, tokenInjector.toString());
        Assert.assertEquals(prefix.length() + TEST_TOKEN.length() + 2, firstAppendIndex);
    }

    @Test
    public void textRemoval() {
        String prefix = "This is a";
        String tokenToReplace = " task ";
        String suffix = "whose content must be modified";
        Task task = new Task(prefix + tokenToReplace + suffix);
        CursorInfo cursorInfo = CursorInfo.after(prefix + "   ");

        TokenInjector tokenInjector = new TokenInjector(task.content(), cursorInfo);

        int firstAppendIndex = tokenInjector.remove(tokenToReplace.trim());
        Assert.assertEquals(prefix + "  " + suffix, tokenInjector.toString());
        Assert.assertEquals(prefix.length() + 1, firstAppendIndex);
    }
}
