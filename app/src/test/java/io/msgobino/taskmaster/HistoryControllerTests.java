package io.msgobino.taskmaster;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.function.Supplier;

import io.msgobino.taskmaster.data.core.Task;
import io.msgobino.taskmaster.data.core.TasksList;
import io.msgobino.taskmaster.utils.TestFilesProvider;
import io.msgobino.taskmaster.viewmodel.tasks.SingleLevelHistoryController;
import io.msgobino.taskmaster.viewmodel.tasks.UndoController;

public class HistoryControllerTests {
    @Test
    public void takeSnapshot() throws IOException {
        TasksList tasksList = TasksList.fromFile(TestFilesProvider.tasksParsingTestFile());
        TasksList supportingTasksList = TasksList.detached();

        Supplier<TasksList.ExportedTasks> exportedTasksSupplier = tasksList::exportTasks;
        UndoController historyController = new SingleLevelHistoryController();
        ((SingleLevelHistoryController) historyController).setTasksListExporter(exportedTasksSupplier);

        historyController.takeSnapshot("A test snapshot.");
        supportingTasksList.importTasks(historyController.getPresent());
        Assert.assertEquals(
                tasksList.get(),
                supportingTasksList.get()
        );
    }

    @Test
    public void undo() throws IOException {
        TasksList tasksList = TasksList.fromFile(TestFilesProvider.tasksParsingTestFile());
        TasksList supportingTasksList = TasksList.detached();

        Supplier<TasksList.ExportedTasks> exportedTasksSupplier = tasksList::exportTasks;
        UndoController historyController = new SingleLevelHistoryController();
        ((SingleLevelHistoryController) historyController).setTasksListExporter(exportedTasksSupplier);

        historyController.takeSnapshot("Before modifications");
        tasksList.createTask(new Task("A succinctly created task."));
        historyController.takeSnapshot("After modifications");

        historyController.undo();
        supportingTasksList.importTasks(historyController.getPresent());
        Assert.assertNotEquals(
                tasksList.get(),
                supportingTasksList.get()
        );

        tasksList.removeTask(new Task("A succinctly created task."));
        Assert.assertEquals(
                tasksList.get(),
                supportingTasksList.get()
        );
    }
}
