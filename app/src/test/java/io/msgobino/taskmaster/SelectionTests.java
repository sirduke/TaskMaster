package io.msgobino.taskmaster;

import org.junit.Assert;
import org.junit.Test;

import io.msgobino.taskmaster.selection.Selection;

public class SelectionTests {
    @Test
    public void selectItem() {
        Selection selection = new Selection();
        for (int i = 0; i < 100; i++) {
            Assert.assertFalse(selection.selected(i));
        }

        selection.select(0);
        Assert.assertTrue(selection.selected(0));
        Assert.assertFalse(selection.selected(1));

        selection.select(0);
        Assert.assertTrue(selection.selected(0));
        Assert.assertFalse(selection.selected(1));

        selection.select(1);
        Assert.assertTrue(selection.selected(0));
        Assert.assertTrue(selection.selected(1));
        for (int i = 2; i < 100; i++) {
            Assert.assertFalse(selection.selected(i));
        }

        selection.deselect(1);
        Assert.assertTrue(selection.selected(0));
        for (int i = 1; i < 100; i++) {
            Assert.assertFalse(selection.selected(i));
        }

        selection.deselect(0);
        for (int i = 0; i < 100; i++) {
            Assert.assertFalse(selection.selected(i));
        }

        selection.select(100);
        for (int i = 0; i < 99; i++) {
            Assert.assertFalse(selection.selected(i));
        }
        Assert.assertTrue(selection.selected(100));
        selection.deselect(100);

        selection.selectUpTo(1000);
        for (int i = 0; i < 999; i++) {
            Assert.assertTrue(selection.selected(i));
        }
        Assert.assertFalse(selection.selected(1000));
    }
}
