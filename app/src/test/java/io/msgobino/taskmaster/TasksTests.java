package io.msgobino.taskmaster;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import io.msgobino.taskmaster.data.analyzer.MissingDatesException;
import io.msgobino.taskmaster.data.analyzer.TaskAnalyzer;
import io.msgobino.taskmaster.data.core.Priority;
import io.msgobino.taskmaster.data.core.Task;
import io.msgobino.taskmaster.data.core.TaskTokens;
import io.msgobino.taskmaster.data.core.TasksList;
import io.msgobino.taskmaster.data.grader.CachedFuzzyTaskGrader;
import io.msgobino.taskmaster.data.grader.GraderComparator;
import io.msgobino.taskmaster.data.grader.TaskGrader;
import io.msgobino.taskmaster.data.modifier.TaskModifier;
import io.msgobino.taskmaster.utils.TestFilesProvider;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class TasksTests {

    @Test
    public void tasksLoading() throws IOException {
        Assert.assertEquals(
                TestFilesProvider.tasksParsingTestFileContent(),
                TasksList.fromFile(TestFilesProvider.tasksParsingTestFile()).toString()
        );
    }

    @Test
    public void taskAdd() throws IOException {
        TasksList tasksList = TasksList.fromFile(TestFilesProvider.tasksParsingTestFile());
        Set<Task> originalTasks = new HashSet<>(tasksList.get());
        Task newTask = new Task("(C) this task has a lower priority +TestingAddingTasks");
        tasksList.createTask(newTask);
        List<Task> tasks = tasksList.get();

        originalTasks.add(newTask);
        Assert.assertEquals(originalTasks.stream().sorted().collect(Collectors.toList()), tasks);
    }

    @Test
    public void taskRemoval() throws IOException {
        TasksList tasksList = TasksList.fromFile(TestFilesProvider.tasksParsingTestFile());
        List<Task> originalTasks = new ArrayList<>(tasksList.get());
        Task toRemove = new Task("(A) test task #1");
        tasksList.removeTask(toRemove);
        List<Task> tasks = tasksList.get();

        originalTasks.remove(toRemove);
        Assert.assertEquals(originalTasks, tasks);
    }


    @Test
    public void taskReplacement() throws IOException {
        TasksList tasksList = TasksList.fromFile(TestFilesProvider.tasksParsingTestFile());
        tasksList.replaceTask(new Task("(A) test task #1"), new Task("(A) test task #1, but has more information"));

        Assert.assertTrue(tasksList.contains("1970-01-01 test task #2 with @context, date and +Projects"));
        Assert.assertTrue(tasksList.contains("(A) test task #2 with @context"));
        Assert.assertTrue(tasksList.contains("(B) 2024-01-19 test +Task with some +Projects and a bit of @context and a due:2024-01-19 custom attribute"));
        Assert.assertTrue(tasksList.contains("(A) test task #1, but has more information"));
        Assert.assertTrue(tasksList.contains("x (B) this is an inactive +Task"));
    }

    @Test
    public void taskAnalyzer() throws MissingDatesException {
        Task task = new Task("x 2024-02-28 2024-01-03 this is a @complete +Task with a @context and another +Project due:2024-05-22 custom:attr pri:C");
        TaskAnalyzer analyzer = TaskAnalyzer.analyze(task);

        Assert.assertFalse(analyzer.isActive());
        Assert.assertEquals(Priority.fromLetter("C"), analyzer.getPriority());
        Assert.assertEquals(LocalDate.of(2024, 1, 3), analyzer.getCreationDate());
        Assert.assertEquals(LocalDate.of(2024, 2, 28), analyzer.getCompletionDate());
        Assert.assertTrue(analyzer.getProjects().contains("+Task"));
        Assert.assertTrue(analyzer.getProjects().contains("+Project"));
        Assert.assertTrue(analyzer.getContexts().contains("@context"));
        Assert.assertEquals("2024-05-22", analyzer.getCustomAttributes().get("due"));
        Assert.assertEquals(56, analyzer.getTaskDuration());
        Assert.assertEquals(124, analyzer.getRemainingTimeFrom(LocalDate.of(2024, 1, 19)));
    }

    @Test
    public void taskAnalyzerIncomplete() {
        Task task = new Task("(C) 2024-02-28 this is a task");
        TaskAnalyzer analyzer = TaskAnalyzer.analyze(task);

        Assert.assertTrue(analyzer.isActive());
        Assert.assertEquals(Priority.fromLetter("C"), analyzer.getPriority());
        Assert.assertEquals(LocalDate.of(2024, 2, 28), analyzer.getCreationDate());
        Assert.assertNull(analyzer.getCompletionDate());
        Assert.assertEquals(0, analyzer.getProjects().size());
        Assert.assertEquals(0, analyzer.getContexts().size());
        Assert.assertEquals(0, analyzer.getCustomAttributes().size());
        Assert.assertThrows(MissingDatesException.class, analyzer::getTaskDuration);
        Assert.assertThrows(MissingDatesException.class, () -> analyzer.getRemainingTimeFrom(LocalDate.EPOCH));
    }


    @Test
    public void taskAnalyzerOnlyCreationDate() {
        Task task = new Task("2024-02-28 this is a task");
        TaskAnalyzer analyzer = TaskAnalyzer.analyze(task);

        Assert.assertTrue(analyzer.isActive());
        Assert.assertEquals(Priority.none(), analyzer.getPriority());
        Assert.assertEquals(LocalDate.of(2024, 2, 28), analyzer.getCreationDate());
        Assert.assertNull(analyzer.getCompletionDate());
        Assert.assertEquals(0, analyzer.getProjects().size());
        Assert.assertEquals(0, analyzer.getContexts().size());
        Assert.assertEquals(0, analyzer.getCustomAttributes().size());
        Assert.assertThrows(MissingDatesException.class, analyzer::getTaskDuration);
        Assert.assertThrows(MissingDatesException.class, () -> analyzer.getRemainingTimeFrom(LocalDate.EPOCH));
    }

    @Test
    public void taskShape() {
        Assert.assertEquals(
                TaskAnalyzer.TaskShape.PRI,
                TaskAnalyzer.analyze("(A) this is a task").getTaskShape()
        );
        Assert.assertEquals(
                TaskAnalyzer.TaskShape.DATE,
                TaskAnalyzer.analyze("1970-01-01 this is a task").getTaskShape()
        );
        Assert.assertEquals(
                TaskAnalyzer.TaskShape.PRI_DATE,
                TaskAnalyzer.analyze("(B) 1970-01-01 this is a task").getTaskShape()
        );
        Assert.assertEquals(
                TaskAnalyzer.TaskShape.CONTENT_ONLY,
                TaskAnalyzer.analyze("this is a task").getTaskShape()
        );
        Assert.assertEquals(
                TaskAnalyzer.TaskShape.X_DATE,
                TaskAnalyzer.analyze("x 1970-01-01 this is a task").getTaskShape()
        );
        Assert.assertEquals(
                TaskAnalyzer.TaskShape.X,
                TaskAnalyzer.analyze("x this is a task").getTaskShape()
        );
        Assert.assertEquals(
                TaskAnalyzer.TaskShape.X_DATE_DATE,
                TaskAnalyzer.analyze("x 1980-01-01 1970-01-01 this is a task").getTaskShape()
        );

        Task tokens = new Task("(A) 2024-01-01 this is a task");
        TaskModifier modifier = TaskModifier.fromTask(tokens);
        Assert.assertEquals(
                TaskAnalyzer.TaskShape.PRI_DATE,
                TaskAnalyzer.analyze(modifier.buildTask()).getTaskShape()
        );
        modifier.setPriority(Priority.none());
        Assert.assertEquals(
                TaskAnalyzer.TaskShape.DATE,
                TaskAnalyzer.analyze(modifier.buildTask()).getTaskShape()
        );
        modifier.setCompleted(LocalDate.of(2021, 7, 11));
        Assert.assertEquals(
                TaskAnalyzer.TaskShape.X_DATE_DATE,
                TaskAnalyzer.analyze(modifier.buildTask()).getTaskShape()
        );
    }


    @Test
    public void taskAnalyzerNoPriority() throws MissingDatesException {
        Task task = new Task("x 2024-02-28 2024-01-03 this is a task");
        TaskAnalyzer analyzer = TaskAnalyzer.analyze(task);

        Assert.assertFalse(analyzer.isActive());
        Assert.assertEquals(Priority.none(), analyzer.getPriority());
        Assert.assertEquals(LocalDate.of(2024, 2, 28), analyzer.getCompletionDate());
        Assert.assertEquals(LocalDate.of(2024, 1, 3), analyzer.getCreationDate());
        Assert.assertEquals(0, analyzer.getProjects().size());
        Assert.assertEquals(0, analyzer.getContexts().size());
        Assert.assertEquals(0, analyzer.getCustomAttributes().size());
        Assert.assertEquals(56, analyzer.getTaskDuration());
        Assert.assertThrows(MissingDatesException.class, () -> analyzer.getRemainingTimeFrom(LocalDate.EPOCH));
    }

    @Test
    public void toggleCreationDate() {
        Task task = new Task("(A) 2024-02-03 this task has a creation date");
        TaskModifier modifier = TaskModifier.fromTask(task);
        modifier.removeCreationDate();
        Assert.assertEquals(
                "(A) this task has a creation date",
                modifier.buildTask().content()
        );
        modifier.setCreationDate(LocalDate.of(2024, 2, 3));
        Assert.assertEquals(
                task.content(),
                modifier.buildTask().content()
        );
    }

    @Test
    public void completionDateManipulation() {
        Task task = new Task("x 2024-02-05 2024-01-19 this task has been completed");
        TaskModifier modifier = TaskModifier.fromTask(task);
        modifier.removeCompletionDate();
        Assert.assertEquals(
                "x 2024-01-19 this task has been completed",
                modifier.buildTask().content()
        );
        modifier.setCompletionDate(LocalDate.of(2024, 2, 6));
        Assert.assertEquals(
                "x 2024-02-06 2024-01-19 this task has been completed",
                modifier.buildTask().content()
        );
        modifier.setPriority(Priority.fromLetter("A"));
        modifier.removeCompletionDate();
        Assert.assertEquals(
                "x 2024-01-19 this task has been completed pri:A",
                modifier.buildTask().content()
        );
        modifier.setCompletionDate(LocalDate.of(2024, 2, 7));
        Assert.assertEquals(
                "x 2024-02-07 2024-01-19 this task has been completed pri:A",
                modifier.buildTask().content()
        );
        modifier.setActive();
        Assert.assertEquals(
                "(A) 2024-01-19 this task has been completed",
                modifier.buildTask().content()
        );
        modifier.setCompleted(LocalDate.of(2024, 8, 12));
        Assert.assertEquals(
                "x 2024-08-12 2024-01-19 this task has been completed pri:A",
                modifier.buildTask().content()
        );
    }

    @Test
    public void modifyPriority() {
        Task task = new Task("(A) this task has a priority");
        TaskModifier modifier = TaskModifier.fromTask(task);
        modifier.setPriority(Priority.fromLetter("B"));
        Assert.assertEquals(
                "(B) this task has a priority",
                modifier.buildTask().content()
        );
        modifier.setPriority(Priority.fromLetter("C"));
        Assert.assertEquals(
                "(C) this task has a priority",
                modifier.buildTask().content()
        );
        modifier.setPriority(Priority.fromLetter("A"));
        Assert.assertEquals(
                "(A) this task has a priority",
                modifier.buildTask().content()

        );
        modifier.setPriority(Priority.none());
        Assert.assertEquals(
                "this task has a priority",
                modifier.buildTask().content()
        );
        modifier.setPriority(Priority.fromLetter("A"));
        Assert.assertEquals(
                "(A) this task has a priority",
                modifier.buildTask().content()
        );
        modifier.setCompleted();
        modifier.setPriority(Priority.fromLetter("B"));
        Assert.assertEquals(
                "x this task has a priority pri:B",
                modifier.buildTask().content()
        );
    }

    @Test
    public void modifyDueDate() {
        Task task = new Task("this task has a due date of due:2024-02-03");
        Task taskWithNoDue = new Task("this task does not have a due date");
        TaskModifier modifier = TaskModifier.fromTask(task);
        TaskModifier modifierNoDue = TaskModifier.fromTask(taskWithNoDue);
        modifier.setDueDate(LocalDate.of(2024, 2, 4));
        Assert.assertEquals(
                "this task has a due date of due:2024-02-04",
                modifier.toString()
        );
        modifier.setDueDate(LocalDate.of(2024, 2, 4));
        Assert.assertEquals(
                "this task has a due date of due:2024-02-04",
                modifier.toString()
        );
        modifier.setDueDate(null);
        Assert.assertEquals(
                "this task has a due date of",
                modifier.toString()
        );
        modifierNoDue.setDueDate(LocalDate.of(2024, 2, 3));
        Assert.assertEquals(
                "this task does not have a due date due:2024-02-03",
                modifierNoDue.toString()
        );
    }

    @Test
    public void tokenManipulation() {
        Task task = new Task("this task has some +Token +Token @test");
        TaskModifier modifier = TaskModifier.fromTask(task);
        modifier.removeToken("+Token");
        Assert.assertEquals(
                "this task has some @test",
                modifier.toString()
        );
        Assert.assertTrue(TaskAnalyzer.analyze(task).contains("@test"));
        Assert.assertTrue(TaskAnalyzer.analyze(task).contains("+Token"));
        Assert.assertFalse(TaskAnalyzer.analyze(task).contains("+Pizza"));
        modifier.removeToken("@test");
        Assert.assertEquals(
                "this task has some",
                modifier.toString()
        );
        modifier.appendToken("+Token");
        Assert.assertEquals(
                "this task has some +Token",
                modifier.toString()
        );

        Task taskTwo = new Task("this task has some +Token +Token @test +Tokens");
        TaskModifier modifierTwo = TaskModifier.fromTask(taskTwo);
        modifierTwo.removeToken("+Token");
        Assert.assertEquals(
                "this task has some @test +Tokens",
                modifierTwo.toString()
        );
        modifierTwo.appendToken("+Token");
        Assert.assertEquals(
                "this task has some @test +Tokens +Token",
                modifierTwo.toString()
        );
    }

    @Test
    public void toggleCompleted() {
        Task task = new Task("x this is a completed task");
        TaskModifier modifier = TaskModifier.fromTask(task);
        modifier.toggleCompleted();
        Assert.assertEquals(
                "this is a completed task",
                modifier.buildTask().content()
        );
        modifier.toggleCompleted();
        Assert.assertEquals(
                "x this is a completed task",
                modifier.buildTask().content()
        );
    }

    @Test
    public void taskTokens() {
        Task task = new Task("x this is a @completed task with +Priority pri:A");
        List<String> list = new ArrayList<>(Arrays.asList(task.content().split(" +")));
        Assert.assertEquals(list, Arrays.stream(task.content().split(" +")).collect(Collectors.toList()));
    }

    @Test
    public void gradingTest() throws IOException {
        TasksList tasksList = TasksList.fromFile(TestFilesProvider.tasksDateTestFile());
        String query = "task with some projects";
        TaskGrader grader = new CachedFuzzyTaskGrader(new TaskTokens(query).asStringList());
        tasksList.withComparator(new GraderComparator(grader));
        Assert.assertEquals(
                "(B) 2024-01-19 test +Task with some +Projects and a bit of @context and a due:2024-01-19 custom attribute",
                tasksList.get().get(0).content()
        );
        Assert.assertEquals(
                "1970-01-01 test task #2 with @context, date and +Projects",
                tasksList.get().get(1).content()
        );
        Assert.assertEquals(
                "(A) 2022-06-28 test task #2 with @context",
                tasksList.get().get(2).content()
        );
    }
}
