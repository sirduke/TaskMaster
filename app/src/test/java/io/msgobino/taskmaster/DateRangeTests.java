package io.msgobino.taskmaster;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

import io.msgobino.taskmaster.utils.DateRange;

public class DateRangeTests {
    @Test
    public void startsToday() {
        DateRange dateRange = new DateRange(LocalDate.now(), LocalDate.now().plusDays(3));
        Assert.assertTrue(dateRange.startsToday());
        DateRange startsTomorrow = new DateRange(LocalDate.now().plusDays(1), LocalDate.now().plusDays(3));
        Assert.assertFalse(startsTomorrow.startsToday());
    }

    @Test
    public void isInRange() {
        DateRange dateRange = new DateRange(LocalDate.now(), LocalDate.now().plusDays(3));
        LocalDate yesterday = LocalDate.now().minusDays(1);
        LocalDate today = LocalDate.now();
        LocalDate tomorrow = LocalDate.now().plusDays(1);
        LocalDate dayAfterTomorrow = LocalDate.now().plusDays(2);
        LocalDate threeDaysFromNow = LocalDate.now().plusDays(3);
        LocalDate fourDaysFromNow = LocalDate.now().plusDays(4);
        Assert.assertFalse(dateRange.isInRange(yesterday));
        Assert.assertTrue(dateRange.isInRange(today));
        Assert.assertTrue(dateRange.isInRange(tomorrow));
        Assert.assertTrue(dateRange.isInRange(dayAfterTomorrow));
        Assert.assertTrue(dateRange.isInRange(threeDaysFromNow));
        Assert.assertFalse(dateRange.isInRange(fourDaysFromNow));
    }


    @Test
    public void singleDayRange() {
        DateRange dateRange = new DateRange(LocalDate.now(), LocalDate.now());
        Assert.assertTrue(dateRange.singleDayRange());
    }

    @Test
    public void endBeforeAfterAdjustments() {
        DateRange dateRange = new DateRange(LocalDate.now(), LocalDate.now().minusDays(10));
        Assert.assertTrue(dateRange.singleDayRange());
    }

    @Test
    public void daysOffset() {
        LocalDate date = LocalDate.of(2024, 3, 30);
        DateRange dateRange = new DateRange(date, date.plusDays(3));
        Assert.assertEquals(
                (int) dateRange.start().toEpochDay() - LocalDate.now().toEpochDay(),
                dateRange.startDaysFromToday()
        );

        Assert.assertEquals(
                (int) dateRange.end().toEpochDay() - LocalDate.now().toEpochDay(),
                dateRange.endDaysFromToday()
        );
    }
}
