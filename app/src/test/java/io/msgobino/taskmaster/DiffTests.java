package io.msgobino.taskmaster;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import io.msgobino.taskmaster.adapter.AdapterUtils;
import io.msgobino.taskmaster.adapter.diff.AddDiff;
import io.msgobino.taskmaster.adapter.diff.ChangeDiff;
import io.msgobino.taskmaster.adapter.diff.DelDiff;
import io.msgobino.taskmaster.adapter.diff.Diff;
import io.msgobino.taskmaster.adapter.diff.MoveDiff;
import io.msgobino.taskmaster.data.core.Task;

public class DiffTests {
    @Test
    public void simpleAddDiff() {
        List<Task> someTasks = List.of(
                new Task("This is a simple task"),
                new Task("This is another simple task")
        );

        List<Task> someTasksWithAddition = new ArrayList<>(someTasks);
        someTasksWithAddition.add(new Task("This is a simple task that has been added"));

        List<Diff> diffs = AdapterUtils.makeDiff(someTasks, someTasksWithAddition);
        Assert.assertEquals(1, diffs.size());
        Assert.assertTrue(diffs.get(0) instanceof AddDiff);

        someTasksWithAddition.add(new Task("This is yet another simple task that has been added"));

        List<Diff> diffsTwo = AdapterUtils.makeDiff(someTasks, someTasksWithAddition);
        Assert.assertEquals(2, diffsTwo.size());
        diffsTwo.forEach(diff -> Assert.assertTrue(diff instanceof AddDiff));
    }

    @Test
    public void simpleDelDiff() {
        Task toRemove = new Task("This is yet another simple task that has been added");
        List<Task> someTasks = new ArrayList<>(List.of(
                new Task("This is a simple task"),
                new Task("This is another simple task"),
                new Task("This is a simple task that has been added"),
                toRemove
        ));

        List<Task> someTasksWithDeletion = new ArrayList<>(someTasks);
        someTasksWithDeletion.remove(toRemove);

        List<Diff> diffs = AdapterUtils.makeDiff(someTasks, someTasksWithDeletion);
        Assert.assertEquals(1, diffs.size());
        Assert.assertTrue(diffs.get(0) instanceof DelDiff);
    }


    @Test
    public void simpleMoveDiff() {
        Task toMove = new Task("This is yet another simple task that has been moved");
        List<Task> someTasks = new ArrayList<>(List.of(
                new Task("This is a simple task"),
                new Task("This is another simple task"),
                toMove
        ));

        List<Task> someTasksWithMove = new ArrayList<>(someTasks);
        someTasksWithMove.remove(toMove);
        someTasksWithMove.add(0, toMove);

        List<Diff> diffs = AdapterUtils.makeDiff(someTasks, someTasksWithMove);
        Assert.assertEquals(1, diffs.size());
        Assert.assertTrue(diffs.get(0) instanceof MoveDiff);
        Assert.assertEquals(2, diffs.get(0).getPosition());
        Assert.assertEquals(0, ((MoveDiff) diffs.get(0)).getAfterPosition());
    }

    @Test
    public void simpleChangeDiff() {
        String searchQuery = "search query";
        Task containingQuery = new Task("This task contains the search query and should generate a change diff");
        Task partialContainingQuery = new Task("This task contains the query as well and should generate another change diff");
        Task notContainingQuery = new Task("This task is a simple task");
        List<Task> tasks = List.of(
                containingQuery,
                partialContainingQuery,
                notContainingQuery
        );

        List<ChangeDiff> changeDiffs = AdapterUtils.makeSearchSyntaxHighlightDiff(tasks, searchQuery);
        Assert.assertEquals(2, changeDiffs.size());
    }
}
