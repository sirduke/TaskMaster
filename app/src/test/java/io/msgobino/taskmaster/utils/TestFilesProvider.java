package io.msgobino.taskmaster.utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TestFilesProvider {
    public static File tasksParsingTestFile() {
        return new File("./src/test/java/io/msgobino/taskmaster/tasksParsingTestFile");
    }

    public static String tasksParsingTestFileContent() throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get("./src/test/java/io/msgobino/taskmaster/tasksParsingTestFile"));
        return new String(encoded, StandardCharsets.UTF_8).trim();
    }

    public static File tasksDateTestFile() {
        return new File("./src/test/java/io/msgobino/taskmaster/tasksDateTestFile");
    }
}
