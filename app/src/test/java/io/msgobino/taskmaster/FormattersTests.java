package io.msgobino.taskmaster;

import org.junit.Assert;
import org.junit.Test;

import io.msgobino.taskmaster.utils.messages.Formatters;

public class FormattersTests {
    @Test
    public void surroundAdd() {
        String toSurround = "This is a text to surround with asterisks";
        Assert.assertEquals("*" + toSurround + "*", Formatters.surroundAdd(toSurround, "*"));
    }

    @Test
    public void surround() {
        String toSurround = "This is a text to surround with brackets";
        Assert.assertEquals("(" + toSurround + ")", Formatters.surroundBrackets(toSurround));
    }
}
