package io.msgobino.taskmaster.topbar;

import static androidx.core.content.ContextCompat.getSystemService;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.progressindicator.CircularProgressIndicator;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.TaskMasterApp;
import io.msgobino.taskmaster.data.core.Task;
import io.msgobino.taskmaster.data.modifier.TaskModifier;
import io.msgobino.taskmaster.selection.SelectionHolder;
import io.msgobino.taskmaster.settings.KnownPreferencesManager;
import io.msgobino.taskmaster.settings.SettingsActivity;
import io.msgobino.taskmaster.utils.Pair;
import io.msgobino.taskmaster.utils.delegate.Setter;
import io.msgobino.taskmaster.utils.dialogs.ConfirmationDialogBuilder;
import io.msgobino.taskmaster.utils.dialogs.FilePickerDialogBuilder;
import io.msgobino.taskmaster.utils.messages.Formatters;
import io.msgobino.taskmaster.utils.messages.Snackbars;
import io.msgobino.taskmaster.utils.storage.StorageDestination;
import io.msgobino.taskmaster.utils.storage.SyncStorageDirectory;
import io.msgobino.taskmaster.viewmodel.search.SearchViewModel;
import io.msgobino.taskmaster.viewmodel.selection.SelectionViewModel;
import io.msgobino.taskmaster.viewmodel.tasks.TaskController;
import io.msgobino.taskmaster.viewmodel.tasks.TasksViewModel;
import io.msgobino.taskmaster.viewmodel.tasks.TodoTxtFileUtils;
import io.msgobino.taskmaster.viewmodel.tasks.UndoController;

public class TopBarFragment extends Fragment implements SelectionHolder.OnSelectionChangeListener {
    public static final String TAG = "TOP_BAR_FRAGMENT";
    public static final String CLIP_DATA_LABEL = "SELECTED_TASKS_CLIP";
    private MaterialToolbar toolbar;
    private TaskController taskController;
    private UndoController historyController;
    private SelectionHolder selectionHolder;
    private Setter<String> searchQuerySetter;
    private String searchQuery;
    private Runnable onNavigateToSettings;
    private CircularProgressIndicator cip;
    private TasksViewModel viewModel;
    private Consumer<Toolbar> syncDrawerWithActionBar = toolbar -> {
    };
    private TextView fileNameTextView;
    private Menu menu;

    public void setCurrentMenuAndUpdate(Menu menu) {
        this.menu = menu;
        updateTopBar();
    }

    public void setSyncDrawerWithActionBar(Consumer<Toolbar> syncDrawerWithActionBar) {
        this.syncDrawerWithActionBar = syncDrawerWithActionBar;
        syncDrawerWithActionBar.accept(toolbar);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        onNavigateToSettings = () -> requireActivity().startActivity(new Intent(requireActivity(), SettingsActivity.class));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.topbar_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        viewModel = new ViewModelProvider(requireActivity()).get(TasksViewModel.class);
        taskController = viewModel.getTaskController();
        historyController = viewModel.getHistoryController();

        initialToolbarSetup(view);

        SearchViewModel searchViewModel = new ViewModelProvider(requireActivity()).get(SearchViewModel.class);
        searchViewModel.getLiveSearchQuery().observe(getViewLifecycleOwner(), searchQuery -> this.searchQuery = searchQuery);
        searchQuerySetter = searchViewModel.getSearchQuerySetter();

        SelectionViewModel selectionViewModel = new ViewModelProvider(requireActivity()).get(SelectionViewModel.class);
        selectionHolder = selectionViewModel.getSelectionHolder();
        selectionHolder.addOnSelectionChangeListener(this);

        cip = view.findViewById(R.id.circular_progress_indicator);
        viewModel.getLiveIsBusy().observe(getViewLifecycleOwner(), isBusy -> {
            if (isBusy) {
                cip.setVisibility(View.VISIBLE);
            } else {
                cip.setVisibility(View.GONE);
            }
        });

        super.onViewCreated(view, savedInstanceState);
    }

    private void initialToolbarSetup(@NonNull View view) {
        toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) requireActivity()).setSupportActionBar(toolbar);
        Objects.requireNonNull(((AppCompatActivity) requireActivity()).getSupportActionBar())
                .setDisplayShowTitleEnabled(false);

        toolbar.inflateMenu(R.menu.topbar_default);

        fileNameTextView = toolbar.findViewById(R.id.toolbar_file_name);
        fileNameTextView.setOnClickListener(v -> (new FilePickerDialogBuilder(getContext(), getViewLifecycleOwner(), input -> {
            String previousFilename = KnownPreferencesManager.getTodoTxtFilename();
            String previousEncodedUri = KnownPreferencesManager.getTodoTxtPath();

            KnownPreferencesManager.saveTodoTxtFilename(Formatters.appendFilenameSuffix(input));

            updateTopBarTitle();
            TaskMasterApp.defer(() -> {
                viewModel = new ViewModelProvider(requireActivity()).get(TasksViewModel.class);
                viewModel.loadFromSettings();
                viewModel.fetch();

                if (KnownPreferencesManager.getIsAutomaticFileCleaning())
                    TodoTxtFileUtils.deleteFileIfEmpty(previousEncodedUri, previousFilename);
            });
        })).build().show());

        syncDrawerWithActionBar.accept(toolbar);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        toolbar.inflateMenu(R.menu.topbar_default);
        setCurrentMenuAndUpdate(menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onDestroyView() {
        selectionHolder.removeOnSelectionChangeListener(this);
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        updateTopBarTitle();
        super.onResume();
    }

    private final Runnable deleteAllTasks = () -> {
        List<Task> confirmedSelection = selectionHolder.getSelectedItems();

        ConfirmationDialogBuilder dialogBuilder = new ConfirmationDialogBuilder(
                requireActivity(),
                confirmedSelection,
                task -> getString(R.string.delete_1_task),
                numberOfSelected -> getString(R.string.delete) + " " + numberOfSelected + " " + getString(R.string.tasks),
                task -> getString(R.string.permanently_delete) + " " + getString(R.string.task) + " "
                        + Formatters.surroundAdd(task.content(), "'") + "?",
                numberOfSelected -> getString(R.string.permanently_delete) + " " + numberOfSelected + " " + getString(R.string.tasks) + "?",
                (d, w) -> {
                    List<Task> confirmedSelectionCopy = new ArrayList<>(confirmedSelection);

                    taskController.deleteTasks(confirmedSelectionCopy);

                    selectionHolder.clearSelection();

                    historyController.takeSnapshot(getString(R.string.deleting) + " " + confirmedSelectionCopy.size() + " " + buildWhatMessage(confirmedSelectionCopy));

                    showUndoSnackbar(confirmedSelectionCopy, "Deleted");
                    d.dismiss();
                },
                (d, w) -> d.cancel()
        );

        dialogBuilder.build()
                .show();
    };

    private String buildWhatMessage(List<Task> confirmedSelectionCopy) {
        return confirmedSelectionCopy.size() < 2
                ? getString(R.string.task) + " '" + confirmedSelectionCopy.get(0) + "'"
                : getString(R.string.tasks);
    }

    private void showUndoSnackbar(List<Task> confirmedSelectionCopy, String verb) {
        String what = buildWhatMessage(confirmedSelectionCopy);
        String message = verb + " " + confirmedSelectionCopy.size() + " " + what;
        Snackbars.UNDO_SNACKBAR_FACTORY.build(requireView(), message, historyController)
                .show();
    }

    private final Runnable markAsCompleted = () -> {
        List<Task> confirmedSelection = selectionHolder.getSelectedItems();

        ConfirmationDialogBuilder dialogBuilder = new ConfirmationDialogBuilder(
                requireActivity(),
                confirmedSelection,
                task -> getString(R.string.mark_1_task_as_completed),
                numberOfSelected -> getString(R.string.mark) + " " + numberOfSelected + " " + getString(R.string.as_completed),
                task -> getString(R.string.mark_task) + " "
                        + Formatters.surroundAdd(task.content(), "'") + " " + getString(R.string.as_completed) + "?",
                numberOfSelected -> getString(R.string.mark) + " " + numberOfSelected + getString(R.string.tasks_as_completed),
                (d, w) -> {
                    List<Pair<Task, Task>> replacementPairs = confirmedSelection.stream()
                            .map(task -> new Pair<>(task, TaskModifier.fromTask(task).setCompleted(LocalDate.now()).buildTask()))
                            .collect(Collectors.toList());

                    taskController.replaceTasks(replacementPairs);

                    selectionHolder.clearSelection();

                    historyController.takeSnapshot(getString(R.string.mark).toLowerCase() + " " + confirmedSelection.size() + " " + getString(R.string.completed).toLowerCase());

                    showUndoSnackbar(confirmedSelection, getString(R.string.mark_completed));
                    d.dismiss();
                },
                (d, w) -> d.cancel()
        );

        dialogBuilder.build()
                .show();
    };

    private final Runnable share = () -> {
        Intent intent = new Intent(Intent.ACTION_SEND);
        String sharedContent = selectionHolder.getSelectedItems().stream()
                .map(Task::content)
                .collect(Collectors.joining("\n"));
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, sharedContent);
        requireActivity().startActivity(intent);

        selectionHolder.clearSelection();
    };

    private final BiConsumer<String, List<Task>> exporter = (outputFileName, toExport) -> {
        String encodedUri = KnownPreferencesManager.getTodoTxtPath();
        SyncStorageDirectory syncDir = SyncStorageDirectory.fromEncodedUri(encodedUri);
        String actualFileName = outputFileName.trim().isEmpty()
                ? KnownPreferencesManager.Defaults.TODO_TXT_FILENAME
                : outputFileName + KnownPreferencesManager.Defaults.FILE_SUFFIX;
        try {
            StorageDestination outputDestination = syncDir.buildStorageDestination(actualFileName);
            TasksViewModel.ExportUtility exportUtility = viewModel.getExportUtility(outputDestination);
            TaskMasterApp.defer(() -> {
                try {
                    exportUtility.exportTasks(toExport);
                } catch (IOException e) {
                    Snackbars.INFO_SNACKBAR_FACTORY.build(requireView(), getString(R.string.cannot_export) + ": " + e.getMessage());
                }
            });
        } catch (IOException e) {
            Snackbars.INFO_SNACKBAR_FACTORY.build(requireView(), getString(R.string.cannot_export) + ": " + e.getMessage());
        }
    };

    private final Runnable export = () -> {
        AlertDialog dialog = (new FilePickerDialogBuilder(requireActivity(), getViewLifecycleOwner(), outputFileName -> {
            String actualFileName = outputFileName.trim().isEmpty()
                    ? KnownPreferencesManager.Defaults.TODO_TXT_FILENAME
                    : outputFileName + KnownPreferencesManager.Defaults.FILE_SUFFIX;
            AlertDialog confirmationSubDialog = new ConfirmationDialogBuilder(
                    requireActivity(),
                    selectionHolder.getSelectedItems(),
                    task -> getString(R.string.export_1_task),
                    numberOfSelected -> getString(R.string.export) + " " + numberOfSelected + " " + getString(R.string.tasks),
                    task -> getString(R.string.really_export_task) + " '" + task + "' " + getString(R.string.to) + " " + actualFileName + "?",
                    numberOfSelected -> getString(R.string.really_export) + " "
                            + numberOfSelected + " "
                            + getString(R.string.tasks) + " "
                            + getString(R.string.to) + " "
                            + actualFileName + "?",
                    (d, w) -> {
                        List<Task> confirmedSelectionCopy = new ArrayList<>(selectionHolder.getSelectedItems());

                        exporter.accept(outputFileName, confirmedSelectionCopy);
                        selectionHolder.clearSelection();

                        d.dismiss();
                    },
                    (d, w) -> d.cancel()
            ).build();
            confirmationSubDialog.show();
        })).build();

        dialog.show();
    };

    private final Runnable moveToAnotherFile = () -> {
        AlertDialog dialog = new FilePickerDialogBuilder(requireActivity(), getViewLifecycleOwner(), outputFileName -> {
            String actualFileName = outputFileName.trim().isEmpty()
                    ? KnownPreferencesManager.Defaults.TODO_TXT_FILENAME
                    : outputFileName + KnownPreferencesManager.Defaults.FILE_SUFFIX;
            AlertDialog confirmationSubDialog = new ConfirmationDialogBuilder(
                    requireActivity(),
                    selectionHolder.getSelectedItems(),
                    task -> getString(R.string.export_and_move_1_task),
                    numberOfSelected -> getString(R.string.export_and_move) + " " + numberOfSelected + " " + getString(R.string.tasks),
                    task -> getString(R.string.really_move_task) + " '" + task + "' " + getString(R.string.to) + " " + actualFileName + "?",
                    numberOfSelected -> getString(R.string.really_export_and_move) + " "
                            + numberOfSelected + " "
                            + getString(R.string.tasks) + " "
                            + getString(R.string.to) + " "
                            + actualFileName + "?",
                    (d, w) -> {
                        List<Task> confirmedSelectionCopy = new ArrayList<>(selectionHolder.getSelectedItems());

                        exporter.accept(outputFileName, confirmedSelectionCopy);
                        taskController.deleteTasks(confirmedSelectionCopy);

                        String what = confirmedSelectionCopy.size() < 2
                                ? getString(R.string.task)
                                : getString(R.string.tasks);
                        historyController.takeSnapshot(getString(R.string.deleting) + " " + confirmedSelectionCopy.size() + " " + what);

                        selectionHolder.clearSelection();
                        d.dismiss();
                    },
                    (d, w) -> d.cancel()
            ).build();
            confirmationSubDialog.show();
        }).build();

        dialog.show();
    };

    private final Runnable copyToClipboard = () -> {
        String textToCopy = selectionHolder.getSelectedItems().stream()
                .map(Task::content)
                .collect(Collectors.joining("\n"));

        ClipboardManager clipboardManager = getSystemService(requireActivity(), ClipboardManager.class);
        ClipData clipData = ClipData.newPlainText(CLIP_DATA_LABEL, textToCopy);
        if (clipboardManager != null) {
            clipboardManager.setPrimaryClip(clipData);
        }

        Snackbars.INFO_SNACKBAR_FACTORY.build(requireView(), getString(R.string.copied_to_clipboard))
                .show();

        selectionHolder.clearSelection();
    };


    private void updateTopBar() {
        toolbar.getMenu().clear();
        if (selectionHolder.selectedCount() < 1) {
            toolbar.inflateMenu(R.menu.topbar_default);
            updateTopBarTitle();

            toolbar.setOnMenuItemClickListener(onNormalMenuItemClickListener);

            setSearchView();
        } else {
            toolbar.inflateMenu(R.menu.topbar_selection);
            toolbar.setOnMenuItemClickListener(onSelectionMenuItemClickListener);
            updateTopBarTitle();
        }
    }


    private String buildSelectionModeTitle(int selectedItemsCount) {
        if (selectedItemsCount > 1)
            return "Selected " + selectedItemsCount + " items";
        else
            return "Selected " + selectedItemsCount + " item";
    }

    public void updateTopBarTitle() {
        if (selectionHolder.selectedCount() < 1) {
            fileNameTextView.setText(KnownPreferencesManager.getTodoTxtFilename());
            fileNameTextView.setClickable(true);
        } else {
            fileNameTextView.setText(buildSelectionModeTitle(selectionHolder.selectedCount()));
            fileNameTextView.setClickable(false);
        }
    }

    private void setSearchView() {
        if (Objects.isNull(menu))
            return;

        MenuItem menuItem = menu.findItem(R.id.app_bar_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        Objects.requireNonNull(searchView).setMaxWidth(Integer.MAX_VALUE);

        Objects.requireNonNull(searchView).setQuery(searchQuery, false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchQuerySetter.set(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchQuerySetter.set(newText);
                return false;
            }
        });
        searchView.setIconified(searchQuery.isEmpty());
    }


    private final Toolbar.OnMenuItemClickListener onNormalMenuItemClickListener = item -> {
        if (item.getItemId() == R.id.app_bar_settings) {
            onNavigateToSettings.run();
        }
        return true;
    };

    private final Toolbar.OnMenuItemClickListener onSelectionMenuItemClickListener = item -> {
        int id = item.getItemId();
        if (id == R.id.selectionbar_delete_all_button) {
            deleteAllTasks.run();
            return true;
        } else if (id == R.id.selectionbar_mark_completed_button) {
            markAsCompleted.run();
            return true;
        } else if (id == R.id.selectionbar_select_all_button) {
            selectionHolder.selectAll();
            return true;
        } else if (id == R.id.selectionbar_share_button) {
            share.run();
            return true;
        } else if (id == R.id.selectionbar_export_button) {
            export.run();
            return true;
        } else if (id == R.id.selectionbar_export_and_move_button) {
            moveToAnotherFile.run();
            return true;
        } else if (id == R.id.selectionbar_copy_button) {
            copyToClipboard.run();
            return true;
        } else {
            return false;
        }
    };


    @Override
    public void onSelectionChange(List<Integer> positions) {
        updateTopBar();
    }
}
