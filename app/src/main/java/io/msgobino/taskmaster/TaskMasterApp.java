package io.msgobino.taskmaster;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.util.TypedValue;

import androidx.annotation.ColorInt;
import androidx.annotation.StyleRes;
import androidx.appcompat.app.AppCompatActivity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

import io.msgobino.taskmaster.settings.KnownPreferencesManager;

public class TaskMasterApp extends Application {
    @Override
    public void onCreate() {
        application = this;
        updateTheme();
        super.onCreate();
    }

    private static Application application;
    private static Theme currentTheme = Theme.MATERIAL_YOU;
    private static final ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    public static final String LOG_ERROR_TAG = "TASKMASTER_ERROR";

    public static Context getContext() {
        return application;
    }

    public static void defer(Runnable runnable) {
        executorService.execute(runnable);
    }

    public static void loadTheme(AppCompatActivity activity) {
        activity.setTheme(currentTheme.themeResId);
    }

    public static void setSplashScreenTheme(AppCompatActivity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            activity.getSplashScreen().setSplashScreenTheme(currentTheme.themeResId);
        }
    }

    public static void updateTheme() {
        currentTheme = KnownPreferencesManager.getTheme();
        currentTheme.apply();
    }

    public static Theme getCurrentTheme() {
        return currentTheme;
    }


    public static @ColorInt int getColorByAttr(int resId) {
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = application.getTheme();
        theme.resolveAttribute(resId, typedValue, true);
        return typedValue.data;
    }

    public static String getStringById(int resId) {
        return getContext().getResources().getString(resId);
    }

    public static class Colors {

        private Colors() {
        }

        public static final Supplier<Integer> NULL_COLOR = () -> null;
        public static final Supplier<Integer> DEFAULT_TEXT_COLOR = () -> getColorByAttr(R.attr.textColorPrimary);
        public static final Supplier<Integer> COMPLETED_TEXT_COLOR = () -> getColorByAttr(R.attr.completedForeground);
        public static final Supplier<Integer> PROJECT_COLOR = () -> getColorByAttr(R.attr.projectForeground);
        public static final Supplier<Integer> CONTEXT_COLOR = () -> getColorByAttr(R.attr.contextForeground);
        public static final Supplier<Integer> CUSTOM_ATTR_COLOR = () -> getColorByAttr(R.attr.customAttrForeground);
        public static final Supplier<Integer> DATE_COLOR = () -> getColorByAttr(R.attr.dateForeground);
        public static final Supplier<Integer> PRIORITY_COLOR = () -> getColorByAttr(R.attr.priorityForeground);
        public static final Supplier<Integer> SEARCH_QUERY_COLOR = NULL_COLOR;
        public static final Supplier<Integer> BUTTON_OUTLINE_TEXT_COLOR = () -> TaskMasterApp.getColorByAttr(R.attr.colorButtonOutlineAccent);
    }

    public static class TextStyles {
        public static final Supplier<Integer> DEFAULT_TYPEFACE = () -> Typeface.NORMAL;
        public static final Supplier<Integer> DATE_TYPEFACE = DEFAULT_TYPEFACE;
        public static final Supplier<Integer> CUSTOM_ATTR_TYPEFACE = DEFAULT_TYPEFACE;
        public static final Supplier<Integer> PROJECT_TYPEFACE = () -> Typeface.ITALIC;
        public static final Supplier<Integer> CONTEXT_TYPEFACE = () -> Typeface.ITALIC;
        public static final Supplier<Integer> PRIORITY_TYPEFACE = () -> Typeface.BOLD;
        public static final Supplier<Integer> SEARCH_QUERY_TYPEFACE = () -> Typeface.BOLD;

        private TextStyles() {
        }


    }

    public enum Theme {
        MATERIAL_YOU(R.style.Base_Theme_TaskMaster),
        MATERIAL_YOU_AMOLED(R.style.Base_Theme_TaskMaster_Amoled),
        ROSEWATER(R.style.Base_Theme_TaskMaster_Rosewater),
        PINE(R.style.Base_Theme_TaskMaster_Pine),
        TIMBER(R.style.Base_Theme_TaskMaster_Timber),
        DRACULA(R.style.Base_Theme_TaskMaster_Dracula),
        CATPPUCCIN(R.style.Base_Theme_TaskMaster_Catppuccin),
        VINTAGE(R.style.Base_Theme_TaskMaster_Vintage),
        CALM(R.style.Base_Theme_TaskMaster_Calm),
        HIGH_CONTRAST(R.style.Base_Theme_TaskMaster_HighContrast),
        ULTRAMARINE(R.style.Base_Theme_TaskMaster_Default);
        private final int themeResId;

        Theme(int resId) {
            this.themeResId = resId;
        }

        public @StyleRes int getResource() {
            return themeResId;
        }

        public void apply() {
            application.setTheme(themeResId);
        }
    }

}
