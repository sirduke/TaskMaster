package io.msgobino.taskmaster.viewmodel.selection;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import io.msgobino.taskmaster.adapter.ListAdapter;
import io.msgobino.taskmaster.data.core.Task;
import io.msgobino.taskmaster.selection.SelectionHolder;

public class SelectionViewModel extends ViewModel implements SelectionHolder.OnSelectionChangeListener {
    private final SelectionHolder selectionHolder = new SelectionHolder();
    private final MutableLiveData<List<Task>> selectedTasks = new MutableLiveData<>(selectionHolder.getSelectedItems());
    private ListAdapter<Task> currentListAdapterReference;

    public void init() {
        selectionHolder.addOnSelectionChangeListener(this);
    }

    public void attachListAdapter(ListAdapter<Task> listAdapter) {
        if (currentListAdapterReference != null)
            selectionHolder.removeOnSelectionChangeListener(listAdapter);
        selectionHolder.attachListAdapter(listAdapter);
        selectionHolder.addOnSelectionChangeListener(listAdapter);
        currentListAdapterReference = listAdapter;
    }

    public SelectionHolder getSelectionHolder() {
        return selectionHolder;
    }

    @Override
    public void onSelectionChange(List<Integer> positions) {
        selectedTasks.postValue(selectionHolder.getSelectedItems());
    }

    @Override
    protected void onCleared() {
        selectionHolder.removeOnSelectionChangeListener(this);
        super.onCleared();
    }
}
