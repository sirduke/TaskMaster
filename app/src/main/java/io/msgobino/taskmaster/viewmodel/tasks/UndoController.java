package io.msgobino.taskmaster.viewmodel.tasks;

import java.util.function.Consumer;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.TaskMasterApp;
import io.msgobino.taskmaster.data.core.TasksList;

public interface UndoController {
    String UNDO_PREFIX = TaskMasterApp.getStringById(R.string.undo) + " ";
    String NO_UNDO = TaskMasterApp.getStringById(R.string.nothing_to_undo);

    void addOnHistoryChangeListener(OnHistoryChangeListener onHistoryChangeListener);

    void removeOnHistoryChangeListener(OnHistoryChangeListener onHistoryChangeListener);

    void setNotificationsProducer(Consumer<String> notificationsProducer);
    TasksList.ExportedTasks getPresent();

    void takeSnapshot(String description);

    void undo();

    boolean canUndo();

    @FunctionalInterface
    interface OnHistoryChangeListener {
        void onHistoryChange();
    }
}
