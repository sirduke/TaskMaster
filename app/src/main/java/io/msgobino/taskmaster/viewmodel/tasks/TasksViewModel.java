package io.msgobino.taskmaster.viewmodel.tasks;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.function.Predicate;

import io.msgobino.taskmaster.TaskMasterApp;
import io.msgobino.taskmaster.data.analyzer.TaskAnalyzer;
import io.msgobino.taskmaster.data.core.Task;
import io.msgobino.taskmaster.data.core.TasksList;
import io.msgobino.taskmaster.settings.KnownPreferencesManager;
import io.msgobino.taskmaster.utils.storage.StorageDestination;

public class TasksViewModel extends ViewModel implements FilterController.OnApplyFiltersListener,
        TaskController.OnTasksChangeListener,
        UndoController.OnHistoryChangeListener,
        SortController.OnSortListener {
    private TasksList tasksList;
    private final MutableLiveData<List<Task>> listLiveData = new MutableLiveData<>(List.of());
    private final MutableLiveData<Set<String>> liveContexts = new MutableLiveData<>(Set.of());
    private final MutableLiveData<Set<String>> liveProjects = new MutableLiveData<>(Set.of());
    private final MutableLiveData<Integer> liveFilteredTasksCount = new MutableLiveData<>(0);
    private final MutableLiveData<Boolean> liveIsBusy = new MutableLiveData<>(false);
    private String savedTodoTxtEncodedUri = "";
    private String savedTodoTxtFileName = "";
    private final FilterController filterController;
    private final TaskController taskController;
    private final SingleLevelHistoryController historyController;
    private final SortController sortController;
    private Consumer<Exception> onSyncFailureStrategy = e -> {
        throw new RuntimeException(e);
    };


    public TasksViewModel() {
        taskController = new TaskController();
        filterController = new FilterController();
        historyController = new SingleLevelHistoryController();
        sortController = new SortController();

        taskController.addOnTasksChangeListener(this);
        filterController.addOnApplyFiltersListener(this);
        historyController.addOnHistoryChangeListener(this);
        sortController.addOnSortListener(this);
    }

    @Override
    protected void onCleared() {
        taskController.removeOnTasksChangeListener(this);
        filterController.removeOnApplyFiltersListener(this);
        historyController.removeOnHistoryChangeListener(this);
        sortController.removeOnSortListener(this);
        super.onCleared();
    }

    public void setOnSyncFailureStrategy(Consumer<Exception> onSyncFailureStrategy) {
        this.onSyncFailureStrategy = onSyncFailureStrategy;
    }

    public void loadFromSettings() {
        imBusyWorkBuilder(() -> {
            String todoTxtEncodedUri = KnownPreferencesManager.getTodoTxtPath();
            String todoTxtFileName = KnownPreferencesManager.getTodoTxtFilename();

            try {
                if (Objects.isNull(todoTxtEncodedUri)) {
                    tasksList = TodoTxtFileUtils.buildDefaultTaskList();
                    postLoadingOperations();
                } else if (!savedTodoTxtEncodedUri.equals(todoTxtEncodedUri) || !savedTodoTxtFileName.equals(todoTxtFileName)) {
                    savedTodoTxtEncodedUri = todoTxtEncodedUri;
                    savedTodoTxtFileName = todoTxtFileName;
                    tasksList = TodoTxtFileUtils.buildTaskListFromPreferences(todoTxtEncodedUri, todoTxtFileName);
                    postLoadingOperations();
                }
            } catch (Exception e) {
                onSyncFailureStrategy.accept(e);
            }
        }).run();
    }

    private void postLoadingOperations() {
        taskController.setTasksListProvider(() -> tasksList);
        historyController.setTasksListExporter(() -> tasksList.exportTasks());
    }


    private void updateLiveTaskList() {
        if (Objects.isNull(filterController)) {
            List<Task> sorted = sortController.apply(tasksList);
            listLiveData.postValue(sorted);
            liveFilteredTasksCount.postValue(sorted.size());
        } else {
            List<Task> filtered = filterController.filter(tasksList);
            List<Task> sorted = sortController.apply(filtered);
            listLiveData.postValue(sorted);
            liveFilteredTasksCount.postValue(sorted.size());
        }
    }

    private void updateContextsAndProjects() {
        Set<String> currentContexts = collectContexts(Set.of(task -> TaskAnalyzer.analyze(task).isActive()));
        if (!Objects.equals(liveContexts.getValue(), currentContexts))
            liveContexts.postValue(currentContexts);

        Set<String> currentProjects = collectProjects(Set.of(task -> TaskAnalyzer.analyze(task).isActive()));
        if (!Objects.equals(liveProjects.getValue(), currentProjects))
            liveProjects.postValue(currentProjects);
    }

    private void updateLiveData() {
        updateLiveTaskList();
        updateContextsAndProjects();
    }

    public void fetch() {
        TaskMasterApp.defer(imBusyWorkBuilder(() -> {
            try {
                tasksList.fetch();
                updateLiveData();
            } catch (Exception e) {
                onSyncFailureStrategy.accept(e);
            }
        }));
    }

    private void sync() {
        TaskMasterApp.defer(imBusyWorkBuilder(() -> {
            try {
                updateLiveData();
                tasksList.commit();
            } catch (Exception e) {
                onSyncFailureStrategy.accept(e);
            }
        }));
    }

    public LiveData<List<Task>> getLiveList() {
        return listLiveData;
    }

    public LiveData<Boolean> getLiveIsBusy() {
        return liveIsBusy;
    }

    public LiveData<Integer> getLiveFilteredTasksCount() {
        return liveFilteredTasksCount;
    }

    private Set<String> collectContexts(Set<Predicate<Task>> filters) {
        SortedSet<String> contexts = new TreeSet<>();
        tasksList.stream()
                .filter(task -> filters.stream().allMatch(filter -> filter.test(task)))
                .map(TaskAnalyzer::analyze)
                .map(TaskAnalyzer::getContexts)
                .forEach(contexts::addAll);
        return contexts;
    }

    private Set<String> collectProjects(Set<Predicate<Task>> filters) {
        SortedSet<String> projects = new TreeSet<>();
        tasksList.stream()
                .filter(task -> filters.stream().allMatch(filter -> filter.test(task)))
                .map(TaskAnalyzer::analyze)
                .map(TaskAnalyzer::getProjects)
                .forEach(projects::addAll);
        return projects;
    }

    public LiveData<Set<String>> liveContexts() {
        return liveContexts;
    }

    public Set<String> contexts() {
        return liveContexts.getValue();
    }

    public LiveData<Set<String>> liveProjects() {
        return liveProjects;
    }

    public Set<String> projects() {
        return liveProjects.getValue();
    }

    public FilterController getFilterController() {
        return filterController;
    }

    public TaskController getTaskController() {
        return taskController;
    }

    public UndoController getHistoryController() {
        return historyController;
    }

    public SortController getSortController() {
        return sortController;
    }

    @Override
    public void onApplyFilters() {
        TaskMasterApp.defer(imBusyWorkBuilder(this::updateLiveData));
    }

    @Override
    public void onTasksChangeListener() {
        sync();
    }

    @Override
    public void onHistoryChange() {
        tasksList.importTasks(historyController.getPresent());
        sync();
    }

    @Override
    public void onSort() {
        updateLiveTaskList();
    }

    private Runnable imBusyWorkBuilder(Runnable work) {
        return () -> {
            liveIsBusy.postValue(true);
            work.run();
            liveIsBusy.postValue(false);
        };
    }

    public ExportUtility getExportUtility(StorageDestination storageDestination) {
        return new ExportUtility(storageDestination);
    }

    public class ExportUtility {
        private final StorageDestination storageDestination;

        public ExportUtility(StorageDestination storageDestination) {
            this.storageDestination = storageDestination;
        }

        public void exportTasks(List<Task> toImport) throws IOException {
            liveIsBusy.postValue(true);
            TasksList supportTasksList = TasksList.fromStorageDestination(storageDestination);
            toImport.forEach(supportTasksList::createTask);
            supportTasksList.commit();
            liveIsBusy.postValue(false);
        }
    }
}
