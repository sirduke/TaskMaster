package io.msgobino.taskmaster.viewmodel.tasks;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Supplier;

import io.msgobino.taskmaster.data.core.TasksList;

public class SingleLevelHistoryController implements UndoController {
    private final Set<OnHistoryChangeListener> historyChangeListeners = new HashSet<>();
    private TasksSnapshot previous = null;
    private TasksSnapshot current = null;
    private Consumer<String> notificationsProducer = s -> {
    };
    boolean hasUndone;
    private Supplier<TasksList.ExportedTasks> tasksListExporter;

    @Override
    public void addOnHistoryChangeListener(OnHistoryChangeListener onHistoryChangeListener) {
        historyChangeListeners.add(onHistoryChangeListener);
    }

    @Override
    public void removeOnHistoryChangeListener(OnHistoryChangeListener onHistoryChangeListener) {
        historyChangeListeners.remove(onHistoryChangeListener);
    }

    public void setTasksListExporter(Supplier<TasksList.ExportedTasks> tasksListExporter) {
        this.tasksListExporter = tasksListExporter;
        current = new TasksSnapshot(tasksListExporter.get(), "History root");
        hasUndone = false;
    }

    @Override
    public void setNotificationsProducer(Consumer<String> notificationsProducer) {
        this.notificationsProducer = s -> notificationsProducer.accept(UNDO_PREFIX + s);
    }

    @Override
    public TasksList.ExportedTasks getPresent() {
        if (hasUndone)
            return previous.snapshot();
        else
            return current.snapshot();
    }

    @Override
    public void takeSnapshot(String description) {
        if (canUndo())
            previous = current;
        current = new TasksSnapshot(tasksListExporter.get(), description);
        hasUndone = false;
    }

    @Override
    public void undo() {
        if (canUndo()) {
            hasUndone = true;
            notificationsProducer.accept(current.description());
            notifyHistoryChange();
        }

    }

    @Override
    public boolean canUndo() {
        return !hasUndone;
    }

    private void notifyHistoryChange() {
        historyChangeListeners.forEach(OnHistoryChangeListener::onHistoryChange);
    }
}
