package io.msgobino.taskmaster.viewmodel.search;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import io.msgobino.taskmaster.utils.delegate.Setter;

public class SearchViewModel extends ViewModel {
    private final MutableLiveData<String> liveSearchQuery = new MutableLiveData<>("");

    public LiveData<String> getLiveSearchQuery() {
        return liveSearchQuery;
    }

    public Setter<String> getSearchQuerySetter() {
        return liveSearchQuery::postValue;
    }
}
