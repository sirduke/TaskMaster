package io.msgobino.taskmaster.viewmodel.tasks;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import io.msgobino.taskmaster.TaskMasterApp;
import io.msgobino.taskmaster.data.core.TasksList;
import io.msgobino.taskmaster.utils.storage.StorageDestination;
import io.msgobino.taskmaster.utils.storage.SyncStorageDirectory;

public class TodoTxtFileUtils {

    private TodoTxtFileUtils() {
    }

    public static final String DEFAULT_FILE_NAME = "todo.txt";


    private static DocumentFile getDataDirFromUri(Uri folderUri) {
        return DocumentFile.fromTreeUri(TaskMasterApp.getContext(), folderUri);
    }

    public static File getDefaultFile() throws IOException {
        File directory = TaskMasterApp.getContext().getFilesDir();
        File file = new File(directory.getAbsolutePath() + File.separator + DEFAULT_FILE_NAME);
        //noinspection ResultOfMethodCallIgnored
        file.createNewFile();
        return file;
    }

    @NonNull
    public static TasksList buildTaskListFromPreferences(String todoTxtEncodedUri, String todoTxtFileName) throws IOException {
        TasksList tasksList;
        if (Objects.isNull(todoTxtEncodedUri)) {
            tasksList = buildDefaultTaskList();
        } else {
            SyncStorageDirectory storageDirectory = SyncStorageDirectory.fromEncodedUri(todoTxtEncodedUri);
            StorageDestination storageFile = storageDirectory.buildStorageDestination(todoTxtFileName);

            TaskMasterApp.getContext()
                    .getContentResolver()
                    .takePersistableUriPermission(storageDirectory.getDirectoryUri(), (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION));
            tasksList = TasksList.fromStorageDestination(storageFile);
        }
        return tasksList;
    }

    @NonNull
    public static TasksList buildDefaultTaskList() throws IOException {
        return TasksList.fromFile(getDefaultFile());
    }

    public static void deleteFileIfEmpty(String encodedUri, String filename) {
        try {
            TasksList tasksList = buildTaskListFromPreferences(encodedUri, filename);
            int numberOfTasksInFile = tasksList.size();
            if (numberOfTasksInFile < 1) {
                SyncStorageDirectory supportSyncStorageDirectory = SyncStorageDirectory.fromEncodedUri(encodedUri);
                supportSyncStorageDirectory.deleteFile(filename);
            }
        } catch (IOException e) {
            Log.d(TaskMasterApp.LOG_ERROR_TAG, "Could not delete file " + filename + " due to " + Objects.requireNonNull(e.getMessage()));
        }
    }
}
