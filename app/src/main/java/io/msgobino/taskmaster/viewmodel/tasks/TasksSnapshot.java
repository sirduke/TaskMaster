package io.msgobino.taskmaster.viewmodel.tasks;

import io.msgobino.taskmaster.data.core.TasksList;

public record TasksSnapshot(TasksList.ExportedTasks snapshot, String description) {
}
