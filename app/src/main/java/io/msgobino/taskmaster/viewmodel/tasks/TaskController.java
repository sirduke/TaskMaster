package io.msgobino.taskmaster.viewmodel.tasks;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

import io.msgobino.taskmaster.data.core.Task;
import io.msgobino.taskmaster.data.core.TasksList;
import io.msgobino.taskmaster.utils.Pair;

public class TaskController {
    private Supplier<TasksList> tasksListProvider;
    private final Set<OnTasksChangeListener> onTasksChangeListeners = new HashSet<>();

    public void setTasksListProvider(Supplier<TasksList> tasksListProvider) {
        this.tasksListProvider = tasksListProvider;
    }

    public void addOnTasksChangeListener(OnTasksChangeListener onTasksChangeListener) {
        onTasksChangeListeners.add(onTasksChangeListener);
    }

    public void removeOnTasksChangeListener(OnTasksChangeListener onTasksChangeListener) {
        onTasksChangeListeners.remove(onTasksChangeListener);
    }

    private void notifyListeners() {
        onTasksChangeListeners.forEach(OnTasksChangeListener::onTasksChangeListener);
    }

    public void createTask(Task task) {
        tasksListProvider.get()
                .createTask(task);
        notifyListeners();
    }

    public void replaceTask(Task toReplace, Task replacement) {
        if (!toReplace.equals(replacement)) {
            tasksListProvider.get()
                    .replaceTask(toReplace, replacement);
            notifyListeners();
        }
    }

    public void deleteTasks(List<Task> tasks) {
        tasks.forEach(task -> tasksListProvider.get().removeTask(task));
        notifyListeners();
    }


    public void replaceTasks(List<Pair<Task, Task>> pairs) {
        pairs.forEach(pair -> tasksListProvider.get()
                .replaceTask(pair.first(), pair.second()));
        notifyListeners();
    }

    @FunctionalInterface
    public interface OnTasksChangeListener {
        void onTasksChangeListener();
    }
}
