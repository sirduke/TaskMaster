package io.msgobino.taskmaster.viewmodel.tasks;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.msgobino.taskmaster.data.core.Task;
import io.msgobino.taskmaster.data.core.TasksList;

public class SortController {
    private Comparator<Task> comparator = Comparator.naturalOrder();
    private final Set<OnSortListener> listeners = new HashSet<>();

    public void addOnSortListener(OnSortListener onSortListener) {
        listeners.add(onSortListener);
    }

    public void removeOnSortListener(OnSortListener onSortListener){
        listeners.remove(onSortListener);
    }

    public void sortWith(Comparator<Task> comparator) {
        this.comparator = comparator;
        listeners.forEach(OnSortListener::onSort);
    }

    public List<Task> apply(TasksList tasksList) {
        List<Task> fromTasksList = tasksList.get();
        fromTasksList.sort(comparator);
        return new ArrayList<>(fromTasksList);
    }

    public List<Task> apply(List<Task> tasks) {
        tasks.sort(comparator);
        return new ArrayList<>(tasks);
    }

    @FunctionalInterface
    public interface OnSortListener {
        void onSort();
    }
}
