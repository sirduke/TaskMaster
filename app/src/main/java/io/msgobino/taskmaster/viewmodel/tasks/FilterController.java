package io.msgobino.taskmaster.viewmodel.tasks;

import androidx.annotation.NonNull;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import io.msgobino.taskmaster.data.analyzer.TaskAnalyzer;
import io.msgobino.taskmaster.data.core.Task;
import io.msgobino.taskmaster.data.core.TasksList;
import io.msgobino.taskmaster.utils.DateRange;
import io.msgobino.taskmaster.utils.factory.Factory;

public class FilterController {
    public static final String ACTIVES_ONLY_TAG = "ACTIVES_ONLY";
    private final Map<String, Predicate<Task>> tagFilterMap = new HashMap<>();
    private final Set<String> keywordSet = new HashSet<>();
    private final Set<OnApplyFiltersListener> onApplyFiltersListeners = new HashSet<>();
    private KeywordMode keywordMode = KeywordMode.ANY;
    public static final String DUE_DATE_RANGE_TAG = "DUE_DATE_RANGE";


    public FilterController() {
        activesOnly();
    }

    private void notifyListeners() {
        onApplyFiltersListeners.forEach(OnApplyFiltersListener::onApplyFilters);
    }

    public List<Task> filter(TasksList tasksList) {
        return tasksList.stream()
                .parallel()
                .filter(task -> filters().stream().allMatch(filter -> filter.test(task)))
                .filter(task -> keywordMode.build(keywordSet).test(task) || keywordSet.isEmpty())
                .collect(Collectors.toList());
    }

    public Set<Predicate<Task>> filters() {
        return new HashSet<>(tagFilterMap.values());
    }

    public void addFilter(String tag, Predicate<Task> predicate) {
        tagFilterMap.put(tag, predicate);
        notifyListeners();
    }

    public void removeFilter(String tag) {
        tagFilterMap.remove(tag);
        notifyListeners();
    }

    public void addKeyword(String keyword) {
        keywordSet.add(keyword);
        notifyListeners();
    }

    public void removeKeyword(String keyword) {
        keywordSet.remove(keyword);
        notifyListeners();
    }

    public void activesOnly() {
        addFilter(ACTIVES_ONLY_TAG, task -> TaskAnalyzer.analyze(task).isActive());
    }

    public void activesAndCompleted() {
        removeFilter(ACTIVES_ONLY_TAG);
    }

    public void pendingWithDueDateRange(DateRange dueDateRange) {
        addFilter(DUE_DATE_RANGE_TAG, task -> {
            TaskAnalyzer analyzer = TaskAnalyzer.analyze(task);
            LocalDate dueDate = analyzer.getDueDate();
            return dueDate != null && dueDateRange.isInRange(dueDate);
        });
    }

    public void noPending() {
        removeFilter(DUE_DATE_RANGE_TAG);
    }

    public void ofToken(String keyword) {
        addKeyword(keyword);
    }

    public void removeOfToken(String keyword) {
        removeKeyword(keyword);
    }

    public boolean isActivesOnly() {
        return Objects.nonNull(tagFilterMap.get(ACTIVES_ONLY_TAG));
    }

    public boolean isPending() {
        return Objects.nonNull(tagFilterMap.get(DUE_DATE_RANGE_TAG));
    }

    public boolean isKeyword(String keyword) {
        return keywordSet.contains(keyword);
    }

    public void clearKeywordFilters() {
        keywordSet.clear();
        notifyListeners();
    }

    public Set<String> getActiveKeywordsFilters() {
        return keywordSet;
    }

    public void addOnApplyFiltersListener(OnApplyFiltersListener onApplyFiltersListener) {
        onApplyFiltersListeners.add(onApplyFiltersListener);
    }

    public void removeOnApplyFiltersListener(OnApplyFiltersListener onApplyFiltersListener) {
        onApplyFiltersListeners.remove(onApplyFiltersListener);
    }

    public void setKeywordFilterMode(KeywordMode keywordMode) {
        this.keywordMode = keywordMode;
        notifyListeners();
    }

    @FunctionalInterface
    public interface OnApplyFiltersListener {
        void onApplyFilters();
    }

    public enum KeywordMode {
        ALL(keywordSet -> task -> keywordSet.stream().allMatch(task::contains)),
        ANY(keywordSet -> task -> keywordSet.stream().anyMatch(task::contains));
        private final Factory<Set<String>, Predicate<Task>> keywordFilterFactory;

        KeywordMode(Factory<Set<String>, Predicate<Task>> keywordFilterFactory) {
            this.keywordFilterFactory = keywordFilterFactory;
        }

        public Predicate<Task> build(Set<String> keywordSet) {
            return keywordFilterFactory.build(keywordSet);
        }

        @NonNull
        @Override
        public String toString() {
            return switch (this) {
                case ALL -> "ALL";
                case ANY -> "ANY";
            };
        }
    }
}
