package io.msgobino.taskmaster.data.core;

import androidx.annotation.NonNull;

import java.util.Objects;

import io.msgobino.taskmaster.utils.RegularExpressions;
import io.msgobino.taskmaster.utils.messages.Formatters;

public class Priority {
    private final String priority;

    private Priority(String priority) {
        this.priority = priority;
    }

    public static Priority none() {
        return new Priority("");
    }


    @NonNull
    @Override
    public String toString() {
        return priority;
    }

    public static Priority fromToken(String token) {
        if (token.matches(RegularExpressions.PRIORITY_REGEX)) {
            return new Priority(token);
        } else {
            return Priority.none();
        }
    }

    public static Priority fromLetter(String letter) {
        if (letter.length() != 1)
            return Priority.none();

        if (letter.matches("[A-Z]"))
            return new Priority(Formatters.surroundBrackets(letter));
        else
            return Priority.none();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Priority priority1 = (Priority) o;
        return Objects.equals(priority, priority1.priority);
    }

    @Override
    public int hashCode() {
        return Objects.hash(priority);
    }

    public String asLetter() {
        return priority.isEmpty()
                ? ""
                : priority.substring(1, 2);
    }
}
