package io.msgobino.taskmaster.data.core;

import androidx.annotation.NonNull;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import io.msgobino.taskmaster.utils.storage.StorageDestination;

public class TasksList {
    private Set<Task> tasks;
    private Comparator<Task> comparator = Comparator.naturalOrder();
    private final StorageFetcher storageFetcher;
    private final StorageCommitter storageCommitter;

    private TasksList(StorageFetcher storageFetcher, StorageCommitter storageCommitter) throws IOException {
        this.storageFetcher = storageFetcher;
        this.storageCommitter = storageCommitter;
        tasks = storageFetcher.fetch();
    }

    public ExportedTasks exportTasks() {
        return new ExportedTasks(tasks);
    }


    public void importTasks(ExportedTasks exportedTasks) {
        this.tasks = exportedTasks.getTasks();
    }

    public static TasksList detached() throws IOException {
        return new TasksList(
                HashSet::new,
                toCommit -> {
                }
        );
    }

    public static TasksList fromFile(File file) throws IOException {
        return new TasksList(
                () -> {
                    Set<Task> internalSetImpl = new HashSet<>();
                    try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                        br.lines().forEach(line -> internalSetImpl.add(new Task(line)));
                    }
                    return filterSpuriousTasks(internalSetImpl);
                },
                toCommit -> {
                    try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
                        bw.write(toCommit);
                        bw.flush();
                    }
                }
        );
    }

    public static TasksList fromStorageDestination(StorageDestination storageDestination) throws IOException {
        return new TasksList(
                () -> {
                    Set<Task> internalSetImpl = new HashSet<>();
                    try (BufferedReader br = new BufferedReader(new InputStreamReader(storageDestination.inputStream()))) {
                        br.lines().forEach(line -> internalSetImpl.add(new Task(line)));
                    }
                    return filterSpuriousTasks(internalSetImpl);
                },
                toCommit -> {
                    try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(storageDestination.outputStream()))) {
                        bw.write(toCommit);
                        bw.flush();
                    }
                }
        );
    }

    private static Set<Task> filterSpuriousTasks(Set<Task> toClean) {
        Set<Task> internalSetImpl = new HashSet<>(toClean);
        internalSetImpl.remove(Task.NULL_TASK);
        return internalSetImpl;
    }

    public void withComparator(Comparator<Task> comparator) {
        this.comparator = comparator;
    }

    private String prepareCommitOutput() {
        return tasks.stream().sorted(Comparator.naturalOrder())
                .map(Task::content)
                .collect(Collectors.joining("\n"));
    }

    public void commit() throws IOException {
        String toCommit = prepareCommitOutput();
        storageCommitter.commit(toCommit);
    }

    public void fetch() throws IOException {
        tasks = storageFetcher.fetch();
    }

    public int size() {
        return tasks.size();
    }

    public List<Task> get() {
        return tasks.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    public boolean contains(String taskContent) {
        return tasks.contains(new Task(taskContent));
    }

    public boolean contains(Task task) {
        return tasks.contains(task);
    }

    public void removeTask(Task task) {
        tasks.remove(task);
    }

    public void createTask(Task task) {
        if (!task.equals(Task.NULL_TASK))
            tasks.add(task);
    }


    public void replaceTask(Task toReplace, Task replacement) {
        tasks.remove(toReplace);
        if (!replacement.equals(Task.NULL_TASK))
            tasks.add(replacement);
    }

    public Stream<Task> stream() {
        return tasks.stream()
                .sorted(comparator);
    }

    @NonNull
    @Override
    public String toString() {
        return prepareCommitOutput();
    }

    public static class ExportedTasks {
        private final Set<Task> tasks;

        public ExportedTasks(Set<Task> tasks) {
            this.tasks = new HashSet<>(tasks);
        }

        protected Set<Task> getTasks() {
            return new HashSet<>(tasks);
        }
    }

}
