package io.msgobino.taskmaster.data.core;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public class TaskTokens implements Iterable<String> {
    private final List<String> tokens;
    public static final String NULL_TOKEN = "";

    public TaskTokens(Task task) {
        tokens = new ArrayList<>(Arrays.asList(task.content().trim().split(" +")));
    }

    public TaskTokens(String taskContent) {
        tokens = new ArrayList<>(Arrays.asList(taskContent.trim().split(" +")));
    }

    public String get(int i) {
        try {
            return tokens.get(i);
        } catch (IndexOutOfBoundsException e) {
            return NULL_TOKEN;
        }
    }

    public int indexOf(String token) {
        return tokens.indexOf(token);
    }

    public void insert(int i, String token) {
        if (token.equals(NULL_TOKEN))
            return;

        try {
            tokens.add(i, token);
        } catch (IndexOutOfBoundsException e) {
            if (i < 0)
                tokens.add(0, token);
            else
                append(token);
        }
    }

    public void append(String token) {
        if (token.equals(NULL_TOKEN))
            return;

        tokens.add(token);
    }

    public void remove(int i) {
        try {
            tokens.remove(i);
        } catch (IndexOutOfBoundsException e) {
            if (i < 0)
                tokens.remove(0);
            else
                tokens.remove(tokens.size() - 1);
        }
    }

    public void remove(String token) {
        if (token.equals(NULL_TOKEN))
            return;

        boolean maybeStillToRemove = true;
        while (maybeStillToRemove) {
            maybeStillToRemove = tokens.remove(token);
        }
    }

    public boolean contains(String token) {
        if (token.equals(NULL_TOKEN))
            return false;

        return tokens.contains(token);
    }

    public void replaceAt(int i, String token) {
        remove(i);
        insert(i, token);
    }

    public List<String> asStringList() {
        return tokens;
    }

    public int size() {
        return tokens.size();
    }

    public Stream<String> stream() {
        return tokens.stream();
    }

    @NonNull
    @Override
    public Iterator<String> iterator() {
        return tokens.iterator();
    }

    @NonNull
    @Override
    public String toString() {
        return String.join(" ", tokens);
    }
}
