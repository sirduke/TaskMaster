package io.msgobino.taskmaster.data.analyzer;

import androidx.annotation.NonNull;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Calendar;

public record TaskTime(int hour, int minute) {
    public static final String TIME_PATTERN = "HH.mm";

    public static TaskTime parse(String timeString) throws DateTimeParseException {
        LocalTime localTime = LocalTime.parse(timeString, DateTimeFormatter.ofPattern(TIME_PATTERN));
        return new TaskTime(localTime.getHour(), localTime.getMinute());
    }

    public static TaskTime now() {
        Calendar calendar = Calendar.getInstance();
        return new TaskTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
    }

    public TaskTime {
        if (hour < 0 || hour > 23) {
            throw new RuntimeException("Wrong hour format");
        }
        if (minute < 0 || minute > 59) {
            throw new RuntimeException("Wrong minute format");
        }
    }

    private String formatAsTwoDigits(int number) {
        if (0 <= number && number < 10)
            return "0" + number;
        else {
            return String.valueOf(number);
        }
    }

    @NonNull
    @Override
    public String toString() {
        return formatAsTwoDigits(hour()) + "." + formatAsTwoDigits(minute());
    }
}
