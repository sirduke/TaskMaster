package io.msgobino.taskmaster.data.grader;

import java.util.Comparator;

import io.msgobino.taskmaster.data.core.Task;

public class GraderComparator implements Comparator<Task> {
    private final TaskGrader grader;

    public GraderComparator(TaskGrader grader) {
        this.grader = grader;
    }

    @Override
    public int compare(Task first, Task second) {
        if (first.equals(second)) {
            return 0;
        }
        return Integer.compare(grader.score(second), grader.score(first));
    }
}
