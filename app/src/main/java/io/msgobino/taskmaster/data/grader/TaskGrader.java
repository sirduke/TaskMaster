package io.msgobino.taskmaster.data.grader;

import io.msgobino.taskmaster.data.core.Task;

@FunctionalInterface
public interface TaskGrader {
    int score(Task task);
}
