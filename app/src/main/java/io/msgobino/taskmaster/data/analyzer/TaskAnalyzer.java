package io.msgobino.taskmaster.data.analyzer;

import java.time.LocalDate;
import java.util.Map;
import java.util.Set;

import io.msgobino.taskmaster.data.core.Priority;
import io.msgobino.taskmaster.data.core.Task;
import io.msgobino.taskmaster.data.core.TaskTokens;
import io.msgobino.taskmaster.utils.RegularExpressions;

public interface TaskAnalyzer {

    static TaskAnalyzer analyze(Task task) {
        return new TokenBasedTaskAnalyzer(task);
    }

    static TaskAnalyzer analyze(TaskTokens tokens) {
        return new TokenBasedTaskAnalyzer(tokens);
    }

    static TaskAnalyzer analyze(String taskContent) {
        return new TokenBasedTaskAnalyzer(new TaskTokens(taskContent));
    }

    boolean isActive();

    TokenBasedTaskAnalyzer.TaskShape getTaskShape();

    LocalDate getCreationDate();

    LocalDate getCompletionDate();

    LocalDate getDueDate();

    TaskTime getTime();

    long getTaskDuration() throws MissingDatesException;

    Map<String, String> getCustomAttributes();

    String getAttribute(String key);

    long getRemainingTimeFrom(LocalDate date) throws MissingDatesException;

    Priority getPriority();

    Set<String> getProjects();

    Set<String> getContexts();

    boolean contains(String token);

    enum TaskShape {
        X_DATE_DATE,
        X_DATE,
        X,
        DATE,
        PRI_DATE,
        PRI,
        CONTENT_ONLY;

        public static TaskShape determineShape(TaskTokens tokens) {
            if (startsWithDate(tokens))
                return DATE;
            else if (startsWithPriorityDate(tokens))
                return PRI_DATE;
            else if (startsWithPriority(tokens))
                return PRI;
            else if (startsWithCrossDateDate(tokens))
                return X_DATE_DATE;
            else if (startsWithCrossDate(tokens))
                return X_DATE;
            else if (startsWithCross(tokens))
                return X;
            else
                return CONTENT_ONLY;
        }

        private static boolean startsWithCrossDateDate(TaskTokens tokens) {
            return startsWithCrossDate(tokens) && tokens.get(2).matches(RegularExpressions.DATE_REGEX);
        }

        private static boolean startsWithCrossDate(TaskTokens tokens) {
            return startsWithCross(tokens) && tokens.get(1).matches(RegularExpressions.DATE_REGEX);
        }

        private static boolean startsWithCross(TaskTokens tokens) {
            return tokens.get(0).matches("x");
        }

        private static boolean startsWithDate(TaskTokens tokens) {
            return tokens.get(0).matches(RegularExpressions.DATE_REGEX);
        }

        private static boolean startsWithPriority(TaskTokens tokens) {
            return !Priority.fromToken(tokens.get(0)).equals(Priority.none());
        }

        private static boolean startsWithPriorityDate(TaskTokens tokens) {
            return startsWithPriority(tokens) && tokens.get(1).matches(RegularExpressions.DATE_REGEX);
        }
    }
}
