package io.msgobino.taskmaster.data.core;

import java.io.IOException;
import java.util.Set;

@FunctionalInterface
public interface StorageFetcher {
    Set<Task> fetch() throws IOException;
}
