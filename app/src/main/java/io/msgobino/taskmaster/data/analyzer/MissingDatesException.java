package io.msgobino.taskmaster.data.analyzer;

public class MissingDatesException extends Exception {
    public MissingDatesException() {
    }

    public MissingDatesException(String message) {
        super(message);
    }

    public MissingDatesException(String message, Throwable cause) {
        super(message, cause);
    }

    public MissingDatesException(Throwable cause) {
        super(cause);
    }

    public MissingDatesException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
