package io.msgobino.taskmaster.data.grader;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiPredicate;

import io.msgobino.taskmaster.data.analyzer.TaskAnalyzer;
import io.msgobino.taskmaster.data.core.Task;

public class CachedFuzzyTaskGrader implements TaskGrader {
    private final Set<String> tokens;
    public static final int STARTING_SCORE = 1;
    public static final int COMPLETE_MATCH = 25;
    public static final int IGNORECASE_COMPLETE_MATCH = 18;
    public static final int TOKENS_SIZE_BONUS = 2;
    public static final int MATCHING_TOKEN_BONUS = 3;
    public static final int IGNORECASE_MATCHING_TOKEN_BONUS = 2;
    public static final int CONTAINS_TOKEN_BONUS = 1;
    public static final int IGNORECASE_CONTAINS_TOKEN_BASELINE = 10;
    public static final int NO_COMPLETE_MATCH_PENALTY = -5;
    public static final int COMPLETED_TASK_PENALTY = -25;
    private final Map<Task, Integer> taskScoreMap = new HashMap<>();

    public CachedFuzzyTaskGrader(List<String> tokens) {
        this.tokens = new HashSet<>(tokens);
    }

    public CachedFuzzyTaskGrader(String reference) {
        this.tokens = new HashSet<>(
                Arrays.asList(reference.trim().split(" "))
        );
    }

    /**
     * @noinspection DataFlowIssue
     */
    @Override
    public int score(Task task) {
        if (tokens.isEmpty()) {
            return STARTING_SCORE;
        }

        // Retrieves previously computed score, if present
        Integer fromMap = taskScoreMap.get(task);
        if (Objects.nonNull(fromMap)) {
            return fromMap;
        }

        int score = STARTING_SCORE;

        if (taskIsCompleted(task))
            score = score + COMPLETED_TASK_PENALTY;

        if (completeMatch(task)) {
            score = score + COMPLETE_MATCH;
            score = score + tokens.size() * TOKENS_SIZE_BONUS;
        } else if (ignorecaseCompleteMatch(task)) {
            score = score + IGNORECASE_COMPLETE_MATCH;
            score = score + tokens.size() * TOKENS_SIZE_BONUS;
        } else {
            score = score + NO_COMPLETE_MATCH_PENALTY;
        }

        String[] precomputedSplits = task.content().split(" ");
        int totalSumOfTokenScores = tokens.stream()
                .map(token -> gradeSingleToken(token, precomputedSplits))
                .reduce(0, Integer::sum);

        score = score + totalSumOfTokenScores;

        taskScoreMap.put(task, score);
        return score;
    }

    private boolean taskIsCompleted(Task task) {
        return !TaskAnalyzer.analyze(task).isActive();
    }

    private int gradeSingleToken(String token, String[] precomputedSplits) {
        int tokenScore = 0;
        if (token.length() > 1) {
            tokenScore = tokenScore + tokenScorer(precomputedSplits, token, String::equals, MATCHING_TOKEN_BONUS);
            tokenScore = tokenScore + tokenScorer(precomputedSplits, token, String::equalsIgnoreCase, IGNORECASE_MATCHING_TOKEN_BONUS);
            tokenScore = tokenScore + tokenScorer(precomputedSplits, token, String::contains, CONTAINS_TOKEN_BONUS);
            tokenScore = tokenScore + tokenScorer(precomputedSplits, token, (s1, s2) -> s1.toLowerCase().contains(s2.toLowerCase()), IGNORECASE_CONTAINS_TOKEN_BASELINE);
        }

        return tokenScore;
    }

    private int tokenScorer(String[] tokens, String token, BiPredicate<String, String> tokenFilter, int scoreToAssign) {
        return Arrays.stream(tokens)
                .filter(taskToken -> tokenFilter.test(taskToken, token))
                .map(equal -> scoreToAssign)
                .reduce(0, Integer::sum);
    }


    private boolean completeMatch(Task task) {
        String joined = String.join(" ", tokens);
        return task.content().contains(joined);
    }

    private boolean ignorecaseCompleteMatch(Task task) {
        String joined = String.join(" ", tokens).toLowerCase();
        return task.content().toLowerCase().contains(joined);
    }
}
