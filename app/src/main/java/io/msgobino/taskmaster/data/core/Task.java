package io.msgobino.taskmaster.data.core;

import androidx.annotation.NonNull;

import java.util.Objects;

public record Task(String content) implements Comparable<Task> {
    public static final Task NULL_TASK = new Task("");
    public static final Task NULL_COMPLETED_TASK = new Task("x");

    public Task(String content) {
        this.content = content.trim();
    }

    public boolean contains(String string) {
        return content.contains(string);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Objects.equals(content, task.content);
    }

    @NonNull
    @Override
    public String toString() {
        return content;
    }

    @Override
    public int compareTo(Task other) {
        return this.content.compareTo(other.content);
    }
}
