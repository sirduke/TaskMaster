package io.msgobino.taskmaster.data.analyzer;

import static java.time.temporal.ChronoUnit.DAYS;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import io.msgobino.taskmaster.data.core.Priority;
import io.msgobino.taskmaster.data.core.Task;
import io.msgobino.taskmaster.data.core.TaskTokens;
import io.msgobino.taskmaster.utils.RegularExpressions;

public class TokenBasedTaskAnalyzer implements TaskAnalyzer {
    private final TaskTokens tokens;

    protected TokenBasedTaskAnalyzer(Task task) {
        tokens = new TaskTokens(task);
    }

    protected TokenBasedTaskAnalyzer(TaskTokens tokens) {
        this.tokens = tokens;
    }

    @Override
    public boolean isActive() {
        if (tokens.size() > 0) {
            return !tokens.get(0).equals("x");
        } else {
            return true;
        }
    }


    @Override
    public TaskShape getTaskShape() {
        return TaskShape.determineShape(tokens);
    }

    @Override
    public LocalDate getCreationDate() {
        return switch (getTaskShape()) {
            case DATE -> LocalDate.parse(tokens.get(0));
            case PRI_DATE, X_DATE -> LocalDate.parse(tokens.get(1));
            case X_DATE_DATE -> LocalDate.parse(tokens.get(2));
            default -> null;
        };
    }

    @Override
    public LocalDate getCompletionDate() {
        if (TaskShape.determineShape(tokens) == TaskShape.X_DATE_DATE)
            return LocalDate.parse(tokens.get(1));
        else
            return null;
    }

    @Override
    public LocalDate getDueDate() {
        try {
            return LocalDate.parse(getAttribute("due"));
        } catch (DateTimeParseException | NullPointerException e) {
            return null;
        }
    }

    @Override
    public TaskTime getTime() {
        TaskTime taskTime;
        String timeAttribute = getAttribute("time");
        try {
            taskTime = TaskTime.parse(timeAttribute);
        } catch (DateTimeParseException | NullPointerException e) {
            return null;
        }
        return taskTime;
    }

    @Override
    public long getTaskDuration() throws MissingDatesException {
        LocalDate completionDate = getCompletionDate();
        LocalDate creationDate = getCreationDate();
        if (Objects.nonNull(completionDate) && Objects.nonNull(creationDate)) {
            return DAYS.between(creationDate, completionDate);
        } else {
            throw new MissingDatesException("Cannot compute task duration because of missing date interval");
        }
    }

    @Override
    public Map<String, String> getCustomAttributes() {
        HashMap<String, String> attributeMap = new HashMap<>();
        Pattern pattern = RegularExpressions.Precompiled.CUSTOM_ATTR_PATTERN;
        tokens.stream()
                .filter(token -> pattern.matcher(token).matches())
                .forEach(token -> {
                    String[] split = token.split(":");
                    attributeMap.put(split[0], split[1]);
                });
        return attributeMap;
    }

    @Override
    public String getAttribute(String key) {
        String attribute = getCustomAttributes().get(key);
        if (Objects.nonNull(attribute))
            return attribute;
        else
            return TaskTokens.NULL_TOKEN;
    }

    @Override
    public long getRemainingTimeFrom(LocalDate date) throws MissingDatesException {
        try {
            LocalDate dueDate = LocalDate.parse(getAttribute("due"));
            return DAYS.between(date, dueDate);
        } catch (DateTimeParseException | NullPointerException e) {
            throw new MissingDatesException(e);
        }
    }

    @Override
    public Priority getPriority() {
        switch (TaskShape.determineShape(tokens)) {
            case PRI, PRI_DATE -> {
                return Priority.fromToken(tokens.get(0));
            }
            default -> {
                String priorityAttribute = getAttribute("pri");
                return Priority.fromLetter(priorityAttribute);
            }
        }
    }

    @Override
    public Set<String> getProjects() {
        Pattern pattern = RegularExpressions.Precompiled.PROJECT_PATTERN;
        return tokens.stream()
                .filter(token -> pattern.matcher(token).matches())
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getContexts() {
        Pattern pattern = RegularExpressions.Precompiled.CONTEXT_PATTERN;
        return tokens.stream()
                .filter(token -> pattern.matcher(token).matches())
                .collect(Collectors.toSet());
    }

    @Override
    public boolean contains(String token) {
        return tokens.contains(token);
    }


}
