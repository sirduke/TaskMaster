package io.msgobino.taskmaster.data.modifier;

import androidx.annotation.NonNull;

import java.time.LocalDate;
import java.util.Objects;

import io.msgobino.taskmaster.data.analyzer.TaskAnalyzer;
import io.msgobino.taskmaster.data.analyzer.TaskTime;
import io.msgobino.taskmaster.data.core.Priority;
import io.msgobino.taskmaster.data.core.Task;
import io.msgobino.taskmaster.data.core.TaskTokens;

public class TaskModifier {
    private final TaskTokens tokens;

    private TaskModifier(Task task) {
        tokens = new TaskTokens(task);
    }

    public static TaskModifier fromTask(Task task) {
        return new TaskModifier(task);
    }

    private TaskModifier(String taskContent) {
        tokens = new TaskTokens(taskContent);
    }

    public static TaskModifier fromContent(String taskContent) {
        return new TaskModifier(taskContent);
    }


    public TaskModifier setActive() {
        TaskAnalyzer analyzer = TaskAnalyzer.analyze(tokens);
        Priority priority = analyzer.getPriority();
        removeCompletionDate();
        if (!analyzer.isActive()) {
            tokens.remove(0);
        }
        setAttribute("pri", TaskTokens.NULL_TOKEN);
        setPriority(priority);

        return this;
    }

    public TaskModifier setCompleted() {
        TaskAnalyzer analyzer = TaskAnalyzer.analyze(tokens);
        Priority priority = analyzer.getPriority();
        setPriority(Priority.none());
        if (analyzer.isActive()) {
            tokens.insert(0, "x");
        }
        setPriority(priority);

        return this;
    }

    public TaskModifier toggleCompleted(LocalDate completionDate) {
        if (tokens.get(0).equals("x")) {
            setActive();
        } else {
            setCompleted(completionDate);
        }

        return this;
    }

    public TaskModifier toggleCompleted() {
        if (tokens.get(0).equals("x")) {
            setActive();
        } else {
            setCompleted();
        }

        return this;
    }

    public TaskModifier setCompleted(LocalDate completionDate) {
        setCompleted();
        setCompletionDate(completionDate);

        return this;
    }

    public TaskModifier setPriority(Priority priority) {
        TaskAnalyzer analyzer = TaskAnalyzer.analyze(tokens);
        TaskAnalyzer.TaskShape shape = analyzer.getTaskShape();

        if (shape == TaskAnalyzer.TaskShape.PRI || shape == TaskAnalyzer.TaskShape.PRI_DATE) {
            tokens.replaceAt(0, priority.toString());
        } else if (!analyzer.isActive()) {
            setAttribute("pri", priority.asLetter());
        } else {
            tokens.insert(0, priority.toString());
        }

        return this;
    }

    public TaskModifier setAttribute(String key, String attribute) {
        TaskAnalyzer analyzer = TaskAnalyzer.analyze(tokens);
        if (analyzer.getAttribute(key).equals(TaskTokens.NULL_TOKEN)) {
            if (!attribute.equals(TaskTokens.NULL_TOKEN))
                tokens.append(key + ":" + attribute);
        } else {
            int removalIndex = -1;
            for (int i = 0; i < tokens.size(); i++) {
                if (tokens.get(i).startsWith(key + ":")) {
                    if (attribute.equals(TaskTokens.NULL_TOKEN)) {
                        removalIndex = i;
                    } else {
                        tokens.replaceAt(i, key + ":" + attribute);
                        return this;
                    }
                }
            }
            if (removalIndex != -1)
                tokens.remove(removalIndex);
        }

        return this;
    }

    public TaskModifier setCreationDate(LocalDate creationDate) {
        String creationDateString = TaskTokens.NULL_TOKEN;
        if (Objects.nonNull(creationDate))
            creationDateString = creationDate.toString();


        TaskAnalyzer analyzer = TaskAnalyzer.analyze(tokens);

        switch (analyzer.getTaskShape()) {
            case CONTENT_ONLY:
                tokens.insert(0, creationDateString);
                return this;
            case X:
            case PRI:
                tokens.insert(1, creationDateString);
                return this;
            case DATE:
                tokens.replaceAt(0, creationDateString);
                return this;
            case PRI_DATE:
            case X_DATE:
                tokens.replaceAt(1, creationDateString);
                return this;
            case X_DATE_DATE:
                tokens.replaceAt(2, creationDateString);
            default:
                return this;
        }
    }

    public TaskModifier setTime(TaskTime taskTime) {
        setAttribute("time", taskTime.toString());
        return this;
    }

    public TaskModifier removeCreationDate() {
        TaskAnalyzer analyzer = TaskAnalyzer.analyze(tokens);
        switch (analyzer.getTaskShape()) {
            case DATE -> {
                tokens.remove(0);
                return this;
            }
            case X_DATE, PRI_DATE -> {
                tokens.remove(1);
                return this;
            }
            default -> {
                return this;
            }
        }
    }

    public TaskModifier setCompletionDate(LocalDate completionDate) {
        TaskAnalyzer analyzer = TaskAnalyzer.analyze(tokens);
        String completionDateString = TaskTokens.NULL_TOKEN;
        if (Objects.nonNull(completionDate))
            completionDateString = completionDate.toString();

        switch (analyzer.getTaskShape()) {
            case X_DATE:
                tokens.insert(1, completionDateString);
            case X_DATE_DATE:
                tokens.replaceAt(1, completionDateString);
            default:
        }
        return this;
    }

    public TaskModifier setDueDate(LocalDate dueDate) {
        if (Objects.nonNull(dueDate))
            setAttribute("due", dueDate.toString());
        else
            setAttribute("due", TaskTokens.NULL_TOKEN);

        return this;
    }

    public TaskModifier removeToken(String token) {
        tokens.remove(token);
        return this;
    }

    public TaskModifier appendToken(String token) {
        tokens.append(token);
        return this;
    }

    public TaskModifier removeCompletionDate() {
        TaskAnalyzer analyzer = TaskAnalyzer.analyze(tokens);
        if (analyzer.getTaskShape() == TaskAnalyzer.TaskShape.X_DATE_DATE) {
            tokens.remove(1);
        }

        return this;
    }

    public Task buildTask() {
        return new Task(tokens.toString());
    }

    @NonNull
    @Override
    public String toString() {
        return tokens.toString();
    }
}
