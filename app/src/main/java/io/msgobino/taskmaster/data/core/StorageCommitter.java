package io.msgobino.taskmaster.data.core;

import java.io.IOException;

@FunctionalInterface
public interface StorageCommitter {
    void commit(String toCommit) throws IOException;
}
