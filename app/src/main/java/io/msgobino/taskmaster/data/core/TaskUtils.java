package io.msgobino.taskmaster.data.core;

import io.msgobino.taskmaster.data.modifier.TaskModifier;

public class TaskUtils {
    private TaskUtils() {
    }

    public static Task formatted(String taskContent) {
        return TaskModifier.fromContent(taskContent).buildTask();
    }
}
