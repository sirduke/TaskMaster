package io.msgobino.taskmaster.filters;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.button.MaterialButtonToggleGroup;
import com.google.android.material.datepicker.MaterialDatePicker;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.selection.SelectionHolder;
import io.msgobino.taskmaster.settings.KnownPreferencesManager;
import io.msgobino.taskmaster.utils.DateRange;
import io.msgobino.taskmaster.utils.ViewGroups;
import io.msgobino.taskmaster.utils.factory.BiFactory;
import io.msgobino.taskmaster.utils.messages.Formatters;
import io.msgobino.taskmaster.viewmodel.selection.SelectionViewModel;
import io.msgobino.taskmaster.viewmodel.tasks.FilterController;
import io.msgobino.taskmaster.viewmodel.tasks.TasksViewModel;

public class FiltersFragment extends Fragment implements FilterController.OnApplyFiltersListener {
    private View view;
    private FilterController filterController;
    private SelectionHolder selectionHolder;
    private Set<String> contexts = new HashSet<>();
    private Set<String> projects = new HashSet<>();
    private LinearLayout tokenColumnFilterLinearLayout;
    private MaterialButton clearFiltersButton;
    private MaterialButton allButton;
    private MaterialButton anyButton;
    private SwitchCompat showCompletedSwitch;
    private SwitchCompat pendingSwitch;
    private MaterialButton dueDateRangePickerButton;
    private DateRange dueDateRange = KnownPreferencesManager.getPendingDueDateRange();
    private final MaterialDatePicker<Pair<Long, Long>> datePicker = buildMaterialDatePicker();
    private final MaterialButton.OnCheckedChangeListener allButtonOnCheckedChangeListener = (materialButton, b) -> setKeywordMode(FilterController.KeywordMode.ALL);
    private final MaterialButton.OnCheckedChangeListener anyButtonOnCheckedChangeListener = (materialButton, b) -> setKeywordMode(FilterController.KeywordMode.ANY);

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.filter_drawer_layout, container, false);
        return view;
    }

    private void setKeywordMode(FilterController.KeywordMode keywordMode) {
        filterController.setKeywordFilterMode(keywordMode);
        KnownPreferencesManager.saveKeywordMode(keywordMode);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        allButton = view.findViewById(R.id.keywords_filter_mode_button_all);
        anyButton = view.findViewById(R.id.keywords_filter_mode_button_any);
        tokenColumnFilterLinearLayout = view.findViewById(R.id.contexts_and_projects_row_filter_linear_layout);

        TasksViewModel viewModel = new ViewModelProvider(requireActivity()).get(TasksViewModel.class);
        filterController = viewModel.getFilterController();
        LiveData<Integer> liveFilteredTasksCount = viewModel.getLiveFilteredTasksCount();
        SelectionViewModel selectionViewModel = new ViewModelProvider(requireActivity()).get(SelectionViewModel.class);
        selectionHolder = selectionViewModel.getSelectionHolder();

        TextView titleTextView = view.findViewById(R.id.filters_drawer_title);
        liveFilteredTasksCount.observe(getViewLifecycleOwner(), filteredTasksCount -> {
            String endText = "";
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                if (filteredTasksCount != 1)
                    endText = " " + getString(R.string.tasks);
                else
                    endText = " " + getString(R.string.task);
            }
            String titleText = getString(R.string.filters_text) + " " + Formatters.surroundBrackets(filteredTasksCount + endText);
            titleTextView.setText(titleText);
        });


        Observer<Set<String>> contextsObserver = contexts -> {
            this.contexts = contexts;
            rebuildTokenColumn();
        };
        Observer<Set<String>> projectsObserver = projects -> {
            this.projects = projects;
            rebuildTokenColumn();
        };
        viewModel.liveContexts().observe(getViewLifecycleOwner(), contextsObserver);
        viewModel.liveProjects().observe(getViewLifecycleOwner(), projectsObserver);

        buildSwitches();
        buildClearFiltersButton();
    }

    @Override
    public void onResume() {
        FilterController.KeywordMode keywordMode = KnownPreferencesManager.getKeywordMode();

        MaterialButtonToggleGroup buttonToggleGroup = view.findViewById(R.id.keywords_filter_mode_button_group);

        allButton.addOnCheckedChangeListener(allButtonOnCheckedChangeListener);
        anyButton.addOnCheckedChangeListener(anyButtonOnCheckedChangeListener);

        if (keywordMode == FilterController.KeywordMode.ALL) {
            buttonToggleGroup.check(R.id.keywords_filter_mode_button_all);
            allButton.setChecked(true);
            anyButton.setChecked(false);
        } else {
            buttonToggleGroup.check(R.id.keywords_filter_mode_button_any);
            allButton.setChecked(false);
            anyButton.setChecked(true);
        }

        filterController.setKeywordFilterMode(keywordMode);
        filterController.addOnApplyFiltersListener(this);

        onApplyFilters();

        super.onResume();
    }

    private void buildClearFiltersButton() {
        clearFiltersButton = view.findViewById(R.id.clear_filters_button);
        clearFiltersButton.setOnClickListener(v -> filterController.clearKeywordFilters());
        clearFiltersButton.setVisibility(View.GONE);
    }

    private void buildSwitches() {
        showCompletedSwitch = view.findViewById(R.id.show_completed_switch);
        showCompletedSwitch.setChecked(!filterController.isActivesOnly());
        showCompletedSwitch.setOnClickListener(v1 -> {
            if (filterController.isActivesOnly())
                filterController.activesAndCompleted();
            else
                filterController.activesOnly();
        });

        dueDateRangePickerButton = view.findViewById(R.id.pending_date_picker_button);
        dueDateRangePickerButton.setText(dueDateRange.toString());
        dueDateRangePickerButton.setOnClickListener(v -> {
            datePicker.show(getParentFragmentManager(), "DUE_DATE_RANGE_PICKER_DIALOG");
        });

        if (filterController.isPending())
            dueDateRangePickerButton.setVisibility(View.VISIBLE);
        else
            dueDateRangePickerButton.setVisibility(View.GONE);

        pendingSwitch = view.findViewById(R.id.pending_switch);
        pendingSwitch.setChecked(filterController.isPending());
        pendingSwitch.setOnClickListener(v -> {
            if (filterController.isPending()) {
                filterController.noPending();
                dueDateRangePickerButton.setVisibility(View.GONE);
            } else {
                filterController.pendingWithDueDateRange(dueDateRange);
                dueDateRangePickerButton.setVisibility(View.VISIBLE);
            }
        });
    }

    private MaterialDatePicker<Pair<Long, Long>> buildMaterialDatePicker() {
        Calendar calendarFirst = Calendar.getInstance();
        Calendar calendarSecond = Calendar.getInstance();
        if (Objects.nonNull(dueDateRange)) {
            calendarFirst.set(Calendar.YEAR, dueDateRange.start().getYear());
            calendarFirst.set(Calendar.MONTH, dueDateRange.start().getMonthValue() - 1); // +1 for internal month offset
            calendarFirst.set(Calendar.DAY_OF_MONTH, dueDateRange.start().getDayOfMonth());

            calendarSecond.set(Calendar.YEAR, dueDateRange.end().getYear());
            calendarSecond.set(Calendar.MONTH, dueDateRange.end().getMonthValue() - 1); // +1 for internal month offset
            calendarSecond.set(Calendar.DAY_OF_MONTH, dueDateRange.end().getDayOfMonth());
        }

        MaterialDatePicker<Pair<Long, Long>> datePicker = MaterialDatePicker.Builder.dateRangePicker()
                .setTitleText(R.string.due_date_picker)
                .setSelection(new Pair<>(calendarFirst.getTimeInMillis(), calendarSecond.getTimeInMillis()))
                .setInputMode(MaterialDatePicker.INPUT_MODE_CALENDAR)
                .build();

        datePicker.addOnPositiveButtonClickListener(dialog -> {
            if (Objects.nonNull(datePicker.getSelection())) {
                Pair<Long, Long> rangeLongMillis = datePicker.getSelection();
                Calendar first = Calendar.getInstance();
                first.setTimeInMillis(rangeLongMillis.first);
                LocalDate firstDate = LocalDate.of(
                        first.get(Calendar.YEAR),
                        first.get(Calendar.MONTH) + 1, // -1 for internal month offset
                        first.get(Calendar.DAY_OF_MONTH)
                );

                Calendar second = Calendar.getInstance();
                second.setTimeInMillis(rangeLongMillis.second);
                LocalDate secondDate = LocalDate.of(
                        second.get(Calendar.YEAR),
                        second.get(Calendar.MONTH) + 1, // -1 for internal month offset
                        second.get(Calendar.DAY_OF_MONTH)
                );

                dueDateRange = new DateRange(firstDate, secondDate);
                dueDateRangePickerButton.setText(dueDateRange.toString());
                KnownPreferencesManager.savePendingDueDateRange(dueDateRange);

                if (filterController.isPending()) {
                    filterController.noPending();
                    filterController.pendingWithDueDateRange(dueDateRange);
                }
            }
        });

        return datePicker;
    }

    @Override
    public void onPause() {
        filterController.removeOnApplyFiltersListener(this);
        allButton.removeOnCheckedChangeListener(allButtonOnCheckedChangeListener);
        anyButton.removeOnCheckedChangeListener(anyButtonOnCheckedChangeListener);
        super.onPause();
    }

    private final BiFactory<LinearLayout, ViewGroup.LayoutParams, Function<String, CheckBox>>
            tokenColumnFactory = (linearLayout, params) -> token -> {
        CheckBox checkBox = new CheckBox(requireContext());
        checkBox.setLayoutParams(params);
        checkBox.setText(token);
        checkBox.setChecked(filterController.isKeyword(token));
        checkBox.setOnClickListener(v -> {
            if (filterController.isKeyword(token))
                filterController.removeOfToken(token);
            else
                filterController.ofToken(token);
        });
        return checkBox;
    };


    private void rebuildTokenColumn() {
        tokenColumnFilterLinearLayout.removeAllViews();
        LinearLayout.LayoutParams keywordsRowParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        keywordsRowParams.setMargins(8, 16, 8, 4);
        List<CheckBox> checkBoxes = contexts.stream()
                .parallel()
                .sorted(String::compareToIgnoreCase)
                .map(tokenColumnFactory.build(tokenColumnFilterLinearLayout, keywordsRowParams))
                .collect(Collectors.toList());

        checkBoxes.addAll(projects.stream()
                .parallel()
                .sorted(String::compareToIgnoreCase)
                .map(tokenColumnFactory.build(tokenColumnFilterLinearLayout, keywordsRowParams))
                .collect(Collectors.toList()));

        checkBoxes.forEach(checkBox -> tokenColumnFilterLinearLayout.addView(checkBox));

        if (checkBoxes.isEmpty()) {
            TextView noTokensTextView = new TextView(requireContext());
            noTokensTextView.setLayoutParams(keywordsRowParams);
            noTokensTextView.setPadding(16, 16, 16, 16);
            noTokensTextView.setText(R.string.no_keyword_available_try_adding_contexts_or_projects_in_tasks);
            tokenColumnFilterLinearLayout.addView(noTokensTextView);
        }
    }

    private void syncTokenColumn() {
        ViewGroups.childrenStream(tokenColumnFilterLinearLayout)
                .filter(v -> v instanceof CheckBox)
                .map(v -> (CheckBox) v)
                .forEach(c -> c.setChecked(filterController.isKeyword(c.getText().toString())));

    }

    @Override
    public void onApplyFilters() {
        if (!filterController.getActiveKeywordsFilters().isEmpty()) {
            clearFiltersButton.setVisibility(View.VISIBLE);
        } else {
            clearFiltersButton.setVisibility(View.GONE);
        }

        showCompletedSwitch.setChecked(!filterController.isActivesOnly());
        pendingSwitch.setChecked(filterController.isPending());

        syncTokenColumn();

        // TODO: design a filtering technique that goes well with persistent selection
        if (selectionHolder.selectedCount() > 0)
            selectionHolder.clearSelection();
    }
}
