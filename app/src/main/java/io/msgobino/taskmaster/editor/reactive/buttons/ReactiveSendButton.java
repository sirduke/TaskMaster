package io.msgobino.taskmaster.editor.reactive.buttons;

import android.content.res.ColorStateList;
import android.view.View;
import android.widget.Button;

import com.google.android.material.button.MaterialButton;

import java.util.function.Consumer;
import java.util.function.Predicate;

import io.msgobino.taskmaster.TaskMasterApp;
import io.msgobino.taskmaster.data.core.Task;
import io.msgobino.taskmaster.editor.EditingData;

public class ReactiveSendButton extends ReactiveButton<EditingData> {
    private final Consumer<String> onSendResult;
    public static final Predicate<String> IS_EMPTY_TASK = taskContent -> taskContent.isEmpty()
            || taskContent.equals(Task.NULL_COMPLETED_TASK.content());

    public ReactiveSendButton(Button button, Consumer<String> onSendResult) {
        super(button);
        this.onSendResult = onSendResult;
        setState(State.INACTIVE);
    }


    @Override
    void setState(State state) {
        super.setState(state);
        MaterialButton materialButton = (MaterialButton) button;
        if (state == State.ACTIVE) {
            materialButton.setIconTint(ColorStateList.valueOf(TaskMasterApp.getColorByAttr(com.google.android.material.R.attr.colorOnTertiary)));
        } else {
            materialButton.setIconTint(ColorStateList.valueOf(TaskMasterApp.getColorByAttr(com.google.android.material.R.attr.colorTertiary)));
        }
    }

    @Override
    protected void buttonSetup(EditingData editingData) {
        String taskContent = editingData.getText().trim();
        if (IS_EMPTY_TASK.test(taskContent)) {
            setState(State.INACTIVE);
        } else {
            setState(State.ACTIVE);
        }
    }

    @Override
    protected void onClickReaction(View v, EditingData editingData) {
        String toSend = editingData.getText().trim();
        if (IS_EMPTY_TASK.test(toSend))
            return;
        onSendResult.accept(toSend);
    }

    @Override
    protected void visibilitySetup(EditingData editingData) {
        button.setVisibility(View.VISIBLE);
    }
}
