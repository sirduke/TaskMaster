package io.msgobino.taskmaster.editor.reactive;

public interface ReactiveItem<T> {
    void react(T t);
}
