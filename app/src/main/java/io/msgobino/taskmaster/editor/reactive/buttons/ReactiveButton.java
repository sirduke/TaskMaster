package io.msgobino.taskmaster.editor.reactive.buttons;

import android.view.View;
import android.widget.Button;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.TaskMasterApp;
import io.msgobino.taskmaster.editor.reactive.ReactiveItem;

public abstract class ReactiveButton<T> implements ReactiveItem<T>, Comparable<ReactiveButton<T>> {
    protected final Button button;
    protected State state = State.INACTIVE;

    protected ReactiveButton(Button button) {
        this.button = button;
    }


    void setState(State state) {
        this.state = state;
        if (state == State.ACTIVE) {
            button.setBackgroundColor(TaskMasterApp.getColorByAttr(R.attr.colorButtonOutlineAccent));
            button.setTextColor(TaskMasterApp.getColorByAttr(R.attr.colorButtonOutlineCompanion));
        } else {
            button.setBackgroundColor(TaskMasterApp.getColorByAttr(R.color.md_theme_transparent));
            button.setTextColor(TaskMasterApp.getColorByAttr(R.attr.colorButtonOutlineAccent));
        }
    }

    @Override
    public void react(T t) {
        visibilitySetup(t);
        if (button.getVisibility() == View.VISIBLE) {
            buttonSetup(t);
            button.setOnClickListener(v -> onClickReaction(v, t));
        }
    }

    protected abstract void buttonSetup(T t);

    protected abstract void onClickReaction(View v, T t);

    protected abstract void visibilitySetup(T t);

    @Override
    public int compareTo(ReactiveButton<T> o) {
        return new ButtonTextComparator().compare(this.button, o.button);
    }

    public enum State {
        ACTIVE,
        INACTIVE
    }

}
