package io.msgobino.taskmaster.editor.reactive.buttons;

import android.view.View;
import android.widget.Button;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.TaskMasterApp;
import io.msgobino.taskmaster.data.analyzer.TaskAnalyzer;
import io.msgobino.taskmaster.editor.CursorInfo;
import io.msgobino.taskmaster.editor.EditingData;
import io.msgobino.taskmaster.editor.textinjectors.TokenInjector;
import io.msgobino.taskmaster.utils.RegularExpressions;
import io.msgobino.taskmaster.utils.delegate.Setter;

public class ReactiveAddKeywordButton extends ReactiveButton<EditingData> {
    private final Setter<EditingData> dataEditor;
    private final String key;
    private String keyword;

    public ReactiveAddKeywordButton(Button button, Setter<EditingData> dataEditor, String key) {
        super(button);
        this.dataEditor = dataEditor;
        this.key = key;
    }

    @Override
    protected void buttonSetup(EditingData editingData) {
        String buttonText = TaskMasterApp.getStringById(R.string.add) + " " + keyword;
        button.setText(buttonText);

        if (TaskAnalyzer.analyze(editingData.getText()).contains(button.getText().toString())) {
            setState(State.ACTIVE);
        } else {
            setState(State.INACTIVE);
        }
    }

    private void updateKeyword(EditingData editingData) {
        String tokenOnCursor = editingData.getTokenOnCursor().trim();
        if (tokenOnCursor.startsWith(key)) {
            keyword = tokenOnCursor;
        } else {
            if (tokenOnCursor.matches(RegularExpressions.WORD)) {
                keyword = key + tokenOnCursor;
            } else {
                keyword = key;
            }
        }
    }

    @Override
    protected void onClickReaction(View v, EditingData editingData) {
        String tokenOnCursor = editingData.getTokenOnCursor();

        TokenInjector tokenInjector = new TokenInjector(editingData.getText(), editingData.getCursorInfo());
        CursorInfo updatedCursorInfo = editingData.getCursorInfo();
        if (state == State.INACTIVE) {
            int afterWordIndex;
            if (ReactiveButtons.buttonMatchesTokenOnCursor(keyword, tokenOnCursor))
                afterWordIndex = tokenInjector.inject(keyword);
            else
                afterWordIndex = tokenInjector.append(keyword);

            updatedCursorInfo = new CursorInfo(afterWordIndex, afterWordIndex);
        }
        dataEditor.set(new EditingData(tokenInjector.toString(), updatedCursorInfo));
    }

    @Override
    protected void visibilitySetup(EditingData editingData) {
        updateKeyword(editingData);
        boolean shouldBeInvisible;
        String tokenOnCursor = editingData.getTokenOnCursor().trim();
        shouldBeInvisible = button.getVisibility() == View.VISIBLE && tokenOnCursor.isEmpty()
                || !(tokenOnCursor.startsWith(key) || tokenOnCursor.matches(RegularExpressions.WORD))
                || TaskAnalyzer.analyze(editingData.getText()).contains(keyword);

        if (shouldBeInvisible) {
            button.setVisibility(View.GONE);
        } else {
            ReactiveButtons.setVisibilityIfActive(button, editingData.getText());
        }
    }
}
