package io.msgobino.taskmaster.editor.reactive.buttons;

import android.view.View;
import android.widget.Button;

import io.msgobino.taskmaster.data.analyzer.TaskAnalyzer;
import io.msgobino.taskmaster.data.modifier.TaskModifier;
import io.msgobino.taskmaster.editor.CursorInfo;
import io.msgobino.taskmaster.editor.EditingData;
import io.msgobino.taskmaster.editor.textinjectors.TokenInjector;
import io.msgobino.taskmaster.settings.KnownPreferencesManager;
import io.msgobino.taskmaster.utils.delegate.Setter;

// TODO: this should not inherit from an observing component
public class ReactiveKeywordButton extends ReactiveButton<EditingData> {
    private final Setter<EditingData> dataEditor;
    private final String keyword;

    protected ReactiveKeywordButton(Button button, Setter<EditingData> dataEditor) {
        super(button);
        this.dataEditor = dataEditor;
        this.keyword = button.getText().toString();
    }

    private void modifyText(String text) {
        TaskModifier modifier = TaskModifier.fromContent(text);
        if (TaskAnalyzer.analyze(text).contains(keyword)) {
            modifier.removeToken(keyword);
        } else {
            modifier.appendToken(keyword);
        }
        String newText = modifier + " ";
        dataEditor.set(new EditingData(newText, CursorInfo.after(newText)));
    }


    private void injectOnCursor(EditingData editingData) {
        String tokenOnCursor = editingData.getTokenOnCursor();

        TokenInjector tokenInjector = new TokenInjector(editingData.getText(), editingData.getCursorInfo());
        CursorInfo updatedCursorInfo;
        if (TaskAnalyzer.analyze(editingData.getText()).contains(keyword)) {
            int cursorPosition = tokenInjector.remove(keyword);
            updatedCursorInfo = new CursorInfo(cursorPosition, cursorPosition);
        } else {
            int afterWordIndex;
            if (state == State.INACTIVE && ReactiveButtons.buttonMatchesTokenOnCursor(keyword, tokenOnCursor))
                afterWordIndex = tokenInjector.inject(keyword);
            else
                afterWordIndex = tokenInjector.append(keyword);

            updatedCursorInfo = new CursorInfo(afterWordIndex, afterWordIndex);
        }
        dataEditor.set(new EditingData(tokenInjector.toString(), updatedCursorInfo));
    }

    @Override
    protected void buttonSetup(EditingData editingData) {
        TaskAnalyzer taskAnalyzer = TaskAnalyzer.analyze(editingData.getText());
        if (taskAnalyzer.contains(keyword))
            setState(State.ACTIVE);
        else
            setState(State.INACTIVE);
    }

    @Override
    protected void onClickReaction(View v, EditingData editingData) {
        boolean useDynamicEditing = KnownPreferencesManager.getIsDynamicEditing();
        if (useDynamicEditing) {
            injectOnCursor(editingData);
        } else {
            modifyText(editingData.getText());
        }
    }

    @Override
    protected void visibilitySetup(EditingData editingData) {
        button.setVisibility(View.VISIBLE);

        if (editingData.getTokenOnCursor().isEmpty())
            return;

        boolean useDynamicEditing = KnownPreferencesManager.getIsDynamicEditing();
        if (useDynamicEditing) {
            String tokenOnCursor = editingData.getTokenOnCursor();
            if (ReactiveButtons.buttonMatchesTokenOnCursor(keyword, tokenOnCursor))
                button.setVisibility(View.VISIBLE);
            else {
                button.setVisibility(View.GONE);
            }
        }
    }

    public static class Factory implements io.msgobino.taskmaster.utils.factory.Factory<Button, ReactiveKeywordButton> {
        private final Setter<EditingData> dataEditor;

        public Factory(Setter<EditingData> dataEditor) {
            this.dataEditor = dataEditor;
        }

        @Override
        public ReactiveKeywordButton build(Button button) {
            return new ReactiveKeywordButton(button, dataEditor);
        }
    }
}
