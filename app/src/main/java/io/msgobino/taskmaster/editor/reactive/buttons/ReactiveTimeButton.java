package io.msgobino.taskmaster.editor.reactive.buttons;

import android.view.View;
import android.widget.Button;

import com.google.android.material.timepicker.MaterialTimePicker;
import com.google.android.material.timepicker.TimeFormat;

import java.util.Objects;
import java.util.function.Consumer;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.data.analyzer.TaskAnalyzer;
import io.msgobino.taskmaster.data.analyzer.TaskTime;
import io.msgobino.taskmaster.data.modifier.TaskModifier;
import io.msgobino.taskmaster.editor.CursorInfo;
import io.msgobino.taskmaster.editor.EditingData;
import io.msgobino.taskmaster.settings.KnownPreferencesManager;
import io.msgobino.taskmaster.utils.delegate.Setter;

public class ReactiveTimeButton extends ReactiveButton<EditingData> {
    private TaskTime selectedTime;
    private final Setter<EditingData> dataEditor;
    private final Consumer<MaterialTimePicker> timePickerShowConsumer;

    public ReactiveTimeButton(Button button, Setter<EditingData> dataEditor, Consumer<MaterialTimePicker> timePickerShowConsumer) {
        super(button);
        this.dataEditor = dataEditor;
        this.timePickerShowConsumer = timePickerShowConsumer;
    }

    @Override
    protected void buttonSetup(EditingData editingData) {
        TaskAnalyzer analyzer = TaskAnalyzer.analyze(editingData.getText());
        TaskTime taskTime = analyzer.getTime();
        if (Objects.nonNull(taskTime)) {
            selectedTime = taskTime;
            button.setText(selectedTime.toString());
            setState(State.ACTIVE);
        } else {
            button.setText(R.string.time_button_starting_text);
            setState(State.INACTIVE);
        }
    }

    @Override
    protected void onClickReaction(View v, EditingData editingData) {
        boolean use24hFormat = KnownPreferencesManager.getIsTimePickerFormat24h();
        int timeFormat;
        if (use24hFormat)
            timeFormat = TimeFormat.CLOCK_24H;
        else
            timeFormat = TimeFormat.CLOCK_12H;

        TaskTime timeNow = TaskTime.now();
        MaterialTimePicker timePickerDialog = new MaterialTimePicker.Builder()
                .setTitleText(R.string.set_the_time_for_the_task)
                .setHour(timeNow.hour())
                .setMinute(timeNow.minute())
                .setTimeFormat(timeFormat)
                .setInputMode(MaterialTimePicker.INPUT_MODE_CLOCK)
                .build();
        timePickerDialog.addOnPositiveButtonClickListener(dialog -> {
                    selectedTime = new TaskTime(timePickerDialog.getHour(), timePickerDialog.getMinute());
                    String modifiedText = TaskModifier.fromContent(editingData.getText())
                            .setTime(selectedTime)
                            .toString() + " ";
                    dataEditor.set(new EditingData(
                            modifiedText,
                            CursorInfo.after(modifiedText)
                    ));
                }
        );
        timePickerShowConsumer.accept(timePickerDialog);
    }

    @Override
    protected void visibilitySetup(EditingData editingData) {
        ReactiveButtons.setVisibilityIfActive(button, editingData.getText());
    }
}
