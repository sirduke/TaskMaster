package io.msgobino.taskmaster.editor.reactive.buttons;

import android.view.View;
import android.widget.Button;

import java.time.LocalDate;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.data.analyzer.TaskAnalyzer;
import io.msgobino.taskmaster.data.modifier.TaskModifier;
import io.msgobino.taskmaster.editor.CursorInfo;
import io.msgobino.taskmaster.editor.EditingData;
import io.msgobino.taskmaster.utils.delegate.Setter;

public class ReactiveMarkCompletedButton extends ReactiveButton<EditingData> {
    private final Setter<EditingData> dataEditor;

    public ReactiveMarkCompletedButton(Button button, Setter<EditingData> dataEditor) {
        super(button);
        this.dataEditor = dataEditor;
    }

    @Override
    protected void buttonSetup(EditingData editingData) {
        if (TaskAnalyzer.analyze(editingData.getText()).isActive()) {
            button.setText(R.string.markcompleted_button_active);
            setState(State.INACTIVE);
        } else {
            button.setText(R.string.markcompleted_button_inactive);
            setState(State.ACTIVE);
        }
    }

    @Override
    protected void onClickReaction(View v, EditingData editingData) {
        String modifiedText = TaskModifier.fromContent(editingData.getText())
                .toggleCompleted(LocalDate.now())
                .toString() + " ";
        dataEditor.set(new EditingData(
                modifiedText,
                CursorInfo.after(modifiedText)
        ));
    }

    @Override
    protected void visibilitySetup(EditingData editingData) {
        button.setVisibility(View.VISIBLE);
    }
}
