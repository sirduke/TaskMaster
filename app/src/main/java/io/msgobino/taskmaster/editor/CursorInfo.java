package io.msgobino.taskmaster.editor;

import java.util.Objects;

import io.msgobino.taskmaster.utils.Metrics;

public record CursorInfo(int cursorStart, int cursorEnd) {
    public CursorInfo shift(int delta) {
        return new CursorInfo(cursorStart + delta, cursorEnd + delta);
    }

    public CursorInfo trim(int minAllowed, int maxAllowed) {
        int newStart = Metrics.trimValue(this.cursorStart, minAllowed, maxAllowed);
        int newEnd = Metrics.trimValue(this.cursorEnd, minAllowed, maxAllowed);
        return new CursorInfo(newStart, newEnd);
    }

    public static CursorInfo after(String text) {
        return new CursorInfo(text.length(), text.length());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CursorInfo that = (CursorInfo) o;
        return cursorStart == that.cursorStart && cursorEnd == that.cursorEnd;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cursorStart, cursorEnd);
    }
}
