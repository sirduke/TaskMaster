package io.msgobino.taskmaster.editor;


import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.material.textfield.TextInputEditText;

import java.util.Set;

import io.msgobino.taskmaster.settings.KnownPreferencesManager;
import io.msgobino.taskmaster.utils.InputFilters;
import io.msgobino.taskmaster.utils.Metrics;
import io.msgobino.taskmaster.utils.delegate.Setter;
import io.msgobino.taskmaster.utils.highlighters.SyntaxHighlighting;

public class TaskEditorTextField extends TextInputEditText {
    private static final int MAX_LINES_SMALL = 3;
    private static final int MAX_LINES_NORMAL = 5;
    private final MutableLiveData<EditingData> liveEditingData = new MutableLiveData<>(new EditingData("", new CursorInfo(0, 0)));
    private final boolean useInEditorHighlighting = KnownPreferencesManager.getIsInEditorHighlighting();

    public TaskEditorTextField(@NonNull Context context) {
        super(context);
    }

    public TaskEditorTextField(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        setMaxLinesBasedOnDpHeight(context);
    }

    private void setMaxLinesBasedOnDpHeight(@NonNull Context context) {
        float dpHeight = Metrics.deviceHeightDp(context);
        if (dpHeight < 800f) {
            setMaxLines(MAX_LINES_SMALL);
        } else {
            setMaxLines(MAX_LINES_NORMAL);
        }
    }

    public void compose(String initialText) {
        setFilters(new InputFilter[]{InputFilters.NEWLINE_FILTER});

        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (useInEditorHighlighting) {
                    SyntaxHighlighting.removeSpans(s, Set.of(ForegroundColorSpan.class, StyleSpan.class));
                    SyntaxHighlighting.highlight(s);
                }

                sendUpdatedText(s.toString());
            }
        });

        if (initialText.isEmpty()) {
            getDataEditor().set(new EditingData(
                    "",
                    new CursorInfo(0, 0)
            ));
        } else {
            String text = initialText + " ";
            getDataEditor().set(new EditingData(
                    text,
                    CursorInfo.after(text)
            ));
        }
    }

    private void sendUpdate(EditingData editingData) {
        liveEditingData.setValue(editingData);
    }

    private void sendUpdatedText(String text) {
        EditingData editingData = liveEditingData.getValue();
        liveEditingData.setValue(editingData.withUpdated(text));
    }

    private void sendUpdatedCursorInfo(CursorInfo cursorInfo) {
        EditingData editingData = liveEditingData.getValue();
        liveEditingData.setValue(editingData.withUpdated(cursorInfo));
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        super.onSelectionChanged(selStart, selEnd);
        if (liveEditingData != null)
            sendUpdatedCursorInfo(new CursorInfo(selStart, selEnd));
    }

    public LiveData<EditingData> liveEditingData() {
        return liveEditingData;
    }

    public Setter<EditingData> getDataEditor() {
        return editingData -> {
            setText(editingData.getText());
            setSelection(editingData.getCursorInfo().cursorStart(), editingData.getCursorInfo().cursorEnd());
            sendUpdate(editingData);
        };
    }

}
