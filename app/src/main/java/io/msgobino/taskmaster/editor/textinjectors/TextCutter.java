package io.msgobino.taskmaster.editor.textinjectors;

import io.msgobino.taskmaster.utils.Pair;

public interface TextCutter {
    default TextCut cut(String text) {
        Pair<Integer, Integer> cutLocation = locateCut(text);
        return new TextCut(
                text.substring(0, cutLocation.first()),
                text.substring(cutLocation.first(), cutLocation.second()),
                text.substring(cutLocation.second())
        );
    }
    Pair<Integer, Integer> locateCut(String text);
}
