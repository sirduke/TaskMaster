package io.msgobino.taskmaster.editor.textinjectors;

import androidx.annotation.NonNull;

public record TextCut(String start, String center, String end) {
    public static TextCut makeCut(String text, TextCutter textCutter) {
        return textCutter.cut(text);
    }

    public static TextCut empty() {
        return new TextCut("", "", "");
    }

    @NonNull
    @Override
    public String toString() {
        return start + center + end;
    }
}
