package io.msgobino.taskmaster.editor.reactive.buttons;

import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;

import com.google.android.material.button.MaterialButton;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.TaskMasterApp;
import io.msgobino.taskmaster.data.analyzer.TaskAnalyzer;
import io.msgobino.taskmaster.editor.EditingData;
import io.msgobino.taskmaster.editor.reactive.KeywordsProvider;
import io.msgobino.taskmaster.editor.reactive.ReactiveItem;
import io.msgobino.taskmaster.settings.KnownPreferencesManager;
import io.msgobino.taskmaster.utils.ViewGroups;

public class KeywordsRowManager implements ReactiveItem<EditingData> {
    private final LinearLayout linearLayout;
    LinearLayout.LayoutParams params;
    private Set<ReactiveButton<EditingData>> reactiveButtons = new TreeSet<>();
    private final ReactiveKeywordButton.Factory reactiveKeywordButtonFactory;
    private final Function<String, Button> buttonCreator;
    private final Consumer<Button> buttonConfigurator;
    private final KeywordsProvider keywordsProvider;

    public KeywordsRowManager(LinearLayout linearLayout, LinearLayout.LayoutParams params,
                              ReactiveKeywordButton.Factory reactiveKeywordButtonFactory,
                              KeywordsProvider keywordsProvider) {
        this.linearLayout = linearLayout;
        this.params = params;
        this.reactiveKeywordButtonFactory = reactiveKeywordButtonFactory;
        this.keywordsProvider = keywordsProvider;
        this.buttonCreator = token -> {
            int buttonStyle = R.style.EditTaskButtonOutline;
            Button button = new MaterialButton(new ContextThemeWrapper(linearLayout.getContext(), buttonStyle));
            button.setText(token);
            return button;
        };
        this.buttonConfigurator = b -> {
            b.setAllCaps(false);
            b.setTextColor(TaskMasterApp.Colors.BUTTON_OUTLINE_TEXT_COLOR.get());
            b.setLayoutParams(params);
        };
        buildFresh();
    }

    private void buildFresh() {
        linearLayout.removeAllViews();
        setupNoMatchesTextView();
        reactiveButtons = keywordsProvider.getKeywords().stream()
                .sorted(String::compareToIgnoreCase)
                .map(buttonCreator)
                .peek(linearLayout::addView)
                .peek(buttonConfigurator)
                .map(reactiveKeywordButtonFactory::build)
                .collect(Collectors.toCollection(TreeSet::new));
    }

    private void setupNoMatchesTextView() {
        TextView textView = new TextView(linearLayout.getContext());
        textView.setText(R.string.no_matches);
        textView.setLayoutParams(params);
        textView.setVisibility(View.GONE);
        textView.setTextSize(17.0f);
        linearLayout.addView(textView);
    }

    public void observe(LifecycleOwner lifecycleOwner, LiveData<EditingData> liveData) {
        liveData.observe(lifecycleOwner, this::react);
        liveData.observe(lifecycleOwner, editingData -> reactiveButtons.forEach(rb -> rb.react(editingData)));
    }

    @Override
    public void react(EditingData editingData) {
        if (TaskAnalyzer.analyze(editingData.getText()).isActive()) {
            boolean newKeywordsInEditorText = keywordsProvider.update(editingData);
            if (newKeywordsInEditorText)
                buildFresh();

            linearLayout.setVisibility(View.VISIBLE);

            boolean useDynamicEditing = KnownPreferencesManager.getIsDynamicEditing();
            if (useDynamicEditing) {
                String tokenOnCursor = editingData.getTokenOnCursor();

                manageLinearLayoutBasedOn(linearLayout, tokenOnCursor);
            }
        } else {
            linearLayout.setVisibility(View.GONE);
        }
    }

    private static void manageLinearLayoutBasedOn(LinearLayout linearLayout, String tokenOnCursor) {
        List<View> children = ViewGroups.children(linearLayout);
        // The first ViewGroup children of this LinearLayout is a text view that is visible in case
        // of no matching tokens
        TextView textView = (TextView) children.get(0);
        children.remove(textView);

        makeMatchingChildrenVisible(tokenOnCursor, children, textView);

        if (children.isEmpty()) {
            linearLayout.setVisibility(View.GONE);
        } else {
            linearLayout.setVisibility(View.VISIBLE);
        }
    }

    private static void makeMatchingChildrenVisible(String tokenOnCursor, List<View> children, TextView textView) {
        if (!tokenOnCursor.trim().isEmpty()) {
            children.forEach(child -> child.setVisibility(View.GONE));
            setMatchingKeywordButtonsAsVisibile(children, tokenOnCursor, textView);
        } else {
            textView.setVisibility(View.GONE);
            children.forEach(child -> child.setVisibility(View.VISIBLE));
        }
    }

    private static void setMatchingKeywordButtonsAsVisibile(List<View> buttonViews, String tokenOnCursor, TextView textView) {
        boolean noVisibleViews = buttonViews.stream()
                .map(v -> (Button) v)
                .filter(tb -> ReactiveButtons.buttonMatchesTokenOnCursor(tb.getText().toString(), tokenOnCursor))
                .peek(b -> b.setVisibility(View.VISIBLE))
                .collect(Collectors.toSet())
                .isEmpty();

        if (noVisibleViews)
            textView.setVisibility(View.VISIBLE);
        else
            textView.setVisibility(View.GONE);
    }
}
