package io.msgobino.taskmaster.editor.textinjectors;

import androidx.annotation.NonNull;

import java.util.regex.Pattern;

import io.msgobino.taskmaster.data.analyzer.TaskAnalyzer;
import io.msgobino.taskmaster.data.modifier.TaskModifier;
import io.msgobino.taskmaster.editor.CursorInfo;

public class TokenInjector {
    private String text;
    private final CursorInfo cursorInfo;
    private final TextCutter textCutter;

    public TokenInjector(String text, CursorInfo cursorInfo) {
        this.text = text;
        this.cursorInfo = cursorInfo;
        this.textCutter = new WordOnCursorTextCutter(cursorInfo);
    }

    @Override
    @NonNull
    public String toString() {
        return text;
    }

    public int inject(String token) {
        TextCut textCut = TextCut.makeCut(text, textCutter);
        String endSeparator = textCut.end().startsWith(" ")
                ? " "
                : "  ";
        TextCut withReplacement = new TextCut(
                textCut.start(),
                token,
                endSeparator + textCut.end()
        );
        text = withReplacement.toString();

        return textCut.start().length()
                + token.length()
                + endSeparator.length()
                - 1;
    }

    public int append(String token) {
        TextCut textCut = TextCut.makeCut(text, textCutter);
        String endSeparator = textCut.end().startsWith(" ")
                ? " "
                : "  ";

        String insertion = textCut.center().trim().isEmpty()
                ? token
                : textCut.center() + " " + token;

        TextCut withInsertion = new TextCut(
                textCut.start(),
                insertion,
                endSeparator + textCut.end()
        );
        text = withInsertion.toString();

        return textCut.start().length()
                + insertion.length()
                + endSeparator.length()
                - 1;
    }

    public int remove(String token) {
        int delta = 0;
        TextCut textCut = TextCut.makeCut(text, textCutter);
        int relativeToCenterCursorPosition = cursorInfo.cursorStart() - textCut.start().length();
        TaskAnalyzer startAnalyzer = TaskAnalyzer.analyze(textCut.start());
        String startText = textCut.start();
        String centerText = textCut.center();
        String endText = textCut.end();

        if (startAnalyzer.contains(token)) {
            TaskModifier startModifier = TaskModifier.fromContent(textCut.start());
            startModifier.removeToken(token);
            startText = startModifier + " ";
            delta = -(textCut.start().length() - startText.length());
        }

        TaskAnalyzer endAnalyzer = TaskAnalyzer.analyze(textCut.end());
        if (endAnalyzer.contains(token)) {
            TaskModifier endModifier = TaskModifier.fromContent(textCut.end());
            endModifier.removeToken(token);
            endText = endModifier.toString();
        }

        if (centerText.trim().matches(Pattern.quote(token))) {
            delta = delta - relativeToCenterCursorPosition;
            centerText = " ";
        }

        text = new TextCut(startText, centerText, endText).toString();

        return cursorInfo.shift(delta).trim(0, text.length()).cursorStart();
    }
}
