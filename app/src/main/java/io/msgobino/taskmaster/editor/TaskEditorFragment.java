package io.msgobino.taskmaster.editor;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.HashSet;
import java.util.Set;
import java.util.function.BiConsumer;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.data.analyzer.TaskAnalyzer;
import io.msgobino.taskmaster.editor.reactive.KeywordsProvider;
import io.msgobino.taskmaster.editor.reactive.buttons.ReactiveAddKeywordButton;
import io.msgobino.taskmaster.editor.reactive.buttons.ReactiveButton;
import io.msgobino.taskmaster.editor.reactive.buttons.ReactiveCreationDateButton;
import io.msgobino.taskmaster.editor.reactive.buttons.ReactiveDueDateButton;
import io.msgobino.taskmaster.editor.reactive.buttons.ReactiveKeywordButton;
import io.msgobino.taskmaster.editor.reactive.buttons.KeywordsRowManager;
import io.msgobino.taskmaster.editor.reactive.buttons.ReactiveMarkCompletedButton;
import io.msgobino.taskmaster.editor.reactive.buttons.ReactivePriorityButton;
import io.msgobino.taskmaster.editor.reactive.buttons.ReactiveSendButton;
import io.msgobino.taskmaster.editor.reactive.buttons.ReactiveTimeButton;
import io.msgobino.taskmaster.taskslist.TaskListFragment;
import io.msgobino.taskmaster.utils.delegate.Setter;
import io.msgobino.taskmaster.viewmodel.tasks.TasksViewModel;

public class TaskEditorFragment extends BottomSheetDialogFragment {
    public static final String TAG = "BOTTOM_SHEET_FRAGMENT";
    private String textToEdit;
    private final BiConsumer<String, String> onSendResults = (toEdit, newTask) -> {
        Bundle results = new Bundle();
        results.putString(TaskListFragment.BUNDLE_KEY_TARGET_TASK, toEdit);
        results.putString(TaskListFragment.BUNDLE_KEY_NEW_TASK, newTask);
        getParentFragmentManager().setFragmentResult(TaskListFragment.REQUEST_EDIT_TASK_KEY, results);
    };

    public TaskEditorFragment() {
    }

    public static TaskEditorFragment getInstance(String toEdit) {
        TaskEditorFragment taskEditorFragment = new TaskEditorFragment();
        Bundle bundle = new Bundle();
        bundle.putString("TO_EDIT", toEdit);
        taskEditorFragment.setArguments(bundle);
        return taskEditorFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        this.textToEdit = getArguments().getString("TO_EDIT");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.edit_bottom_sheet_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setupDialogHeight(view);

        TasksViewModel viewModel = new ViewModelProvider(requireActivity()).get(TasksViewModel.class);
        Set<String> contexts = viewModel.contexts();
        Set<String> projects = viewModel.projects();

        TaskEditorTextField taskEditorTextField = view.findViewById(R.id.edit_text_field);
        LiveData<EditingData> liveEditingData = taskEditorTextField.liveEditingData();
        Setter<EditingData> dataEditor = taskEditorTextField.getDataEditor();

        composeReactiveButtons(view, dataEditor, contexts, liveEditingData, projects);

        taskEditorTextField.compose(textToEdit);

        super.onViewCreated(view, savedInstanceState);
    }

    private void composeReactiveButtons(@NonNull View view, Setter<EditingData> dataEditor, Set<String> contexts, LiveData<EditingData> liveEditingData, Set<String> projects) {
        ReactiveKeywordButton.Factory reactiveKeywordButtonFactory = new ReactiveKeywordButton.Factory(dataEditor);
        LinearLayout.LayoutParams reactiveKeywordsRowParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        reactiveKeywordsRowParams.setMargins(8, 4, 8, 8);
        reactiveKeywordsRowParams.gravity = Gravity.CENTER_VERTICAL;
        KeywordsRowManager reactiveContextsRow = new KeywordsRowManager(
                view.findViewById(R.id.contexts_row_linear_layout),
                reactiveKeywordsRowParams,
                reactiveKeywordButtonFactory,
                new KeywordsProvider(contexts, editingData -> TaskAnalyzer.analyze(editingData.getText()).getContexts())
        );
        reactiveContextsRow.observe(getViewLifecycleOwner(), liveEditingData);

        KeywordsRowManager reactiveProjectsRow = new KeywordsRowManager(
                view.findViewById(R.id.projects_row_linear_layout),
                reactiveKeywordsRowParams,
                reactiveKeywordButtonFactory,
                new KeywordsProvider(projects, editingData -> TaskAnalyzer.analyze(editingData.getText()).getProjects())
        );
        reactiveProjectsRow.observe(getViewLifecycleOwner(), liveEditingData);

        Set<ReactiveButton<EditingData>> reactiveButtons = buildReactiveButtons(view, dataEditor, liveEditingData);
        liveEditingData.observe(getViewLifecycleOwner(), editingData -> reactiveButtons.forEach(rb -> rb.react(editingData)));
    }

    private void setupDialogHeight(@NonNull View view) {
        Dialog dialog = getDialog();
        if (dialog != null) {
            View bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
            bottomSheet.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
        }

        view.post(() -> {
            View parent = (View) view.getParent();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) (parent).getLayoutParams();
            CoordinatorLayout.Behavior behavior = params.getBehavior();
            BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) behavior;
            bottomSheetBehavior.setPeekHeight(view.getMeasuredHeight());
        });
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        // default dialog is okay
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        forceFullyExpandedState(dialog);
        return dialog;
    }

    private static void forceFullyExpandedState(Dialog dialog) {
        dialog.setOnShowListener(d -> {
            BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) d;
            @SuppressLint("WrongViewCast") FrameLayout bottomSheet
                    = (FrameLayout) bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
            BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
        });
    }

    private Set<ReactiveButton<EditingData>> buildReactiveButtons(@NonNull View view, Setter<EditingData> dataEditor, LiveData<EditingData> liveEditingData) {
        Set<ReactiveButton<EditingData>> reactiveButtons = new HashSet<>();
        reactiveButtons.add(new ReactivePriorityButton(
                view.findViewById(R.id.priority_button),
                dataEditor
        ));

        reactiveButtons.add(new ReactiveDueDateButton(
                view.findViewById(R.id.duedate_button),
                dataEditor,
                datePicker -> datePicker.show(getParentFragmentManager(), "DUE_DATE_PICKER_DIALOG")
        ));

        reactiveButtons.add(new ReactiveTimeButton(
                view.findViewById(R.id.time_button),
                dataEditor,
                timePicker -> timePicker.show(getParentFragmentManager(), "TIME_PICKER_DIALOG")
        ));

        reactiveButtons.add(new ReactiveAddKeywordButton(
                view.findViewById(R.id.add_context_button),
                dataEditor,
                "@"
        ));

        reactiveButtons.add(new ReactiveAddKeywordButton(
                view.findViewById(R.id.add_project_button),
                dataEditor,
                "+"
        ));

        reactiveButtons.add(new ReactiveCreationDateButton(
                view.findViewById(R.id.creationdate_button),
                dataEditor
        ));

        reactiveButtons.add(new ReactiveMarkCompletedButton(
                view.findViewById(R.id.markcompleted_button),
                dataEditor
        ));

        reactiveButtons.add(new ReactiveSendButton(
                view.findViewById(R.id.send_button),
                taskContent -> {
                    onSendResults.accept(textToEdit, taskContent);
                    getDialog().dismiss();
                }
        ));

        return reactiveButtons;
    }
}
