package io.msgobino.taskmaster.editor.reactive;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

import io.msgobino.taskmaster.editor.EditingData;

public class KeywordsProvider {
    private final Set<String> staticKeywordSource;
    private final Function<EditingData, Set<String>> keywordDetector;
    private Set<String> detectedKeywords;

    public KeywordsProvider(Set<String> staticKeywordSource, Function<EditingData, Set<String>> keywordDetector) {
        this.staticKeywordSource = staticKeywordSource;
        this.keywordDetector = keywordDetector;
        detectedKeywords = new HashSet<>();
        detectedKeywords.addAll(staticKeywordSource);
    }

    public boolean update(EditingData editingData) {
        Set<String> newKeywords = new HashSet<>(staticKeywordSource);
        newKeywords.addAll(keywordDetector.apply(editingData));

        if (detectedKeywords.equals(newKeywords)) {
            return false;
        } else {
            detectedKeywords = newKeywords;
            return true;
        }
    }

    public Set<String> getKeywords() {
        return detectedKeywords;
    }
}
