package io.msgobino.taskmaster.editor;

import io.msgobino.taskmaster.editor.textinjectors.CursorAnalyzers;

public class EditingData {
    private final String text;
    private final CursorInfo cursorInfo;

    public EditingData(String text, CursorInfo cursorInfo) {
        this.text = text;
        this.cursorInfo = cursorInfo;
    }

    public String getTokenOnCursor() {
        return CursorAnalyzers.getTokenOnCursor(text, cursorInfo);
    }

    public String getText() {
        return text;
    }

    public CursorInfo getCursorInfo() {
        return cursorInfo;
    }

    public EditingData withUpdated(String text) {
        return new EditingData(text, this.cursorInfo);
    }

    public EditingData withUpdated(CursorInfo cursorInfo) {
        return new EditingData(this.text, cursorInfo);
    }

}
