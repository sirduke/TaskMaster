package io.msgobino.taskmaster.editor.reactive.buttons;

import android.view.View;
import android.widget.Button;

import io.msgobino.taskmaster.data.analyzer.TaskAnalyzer;

public class ReactiveButtons {
    private ReactiveButtons() {
    }

    public static void setVisibilityIfActive(Button button, String taskContent) {
        TaskAnalyzer analyzer = TaskAnalyzer.analyze(taskContent);
        if (analyzer.isActive())
            button.setVisibility(View.VISIBLE);
        else
            button.setVisibility(View.GONE);
    }

    public static boolean buttonMatchesTokenOnCursor(String buttonText, String tokenOnCursor) {
        if (!tokenOnCursor.isEmpty()) {
            if (tokenOnCursor.startsWith(buttonText.substring(0, 1)))
                return buttonText.toLowerCase().startsWith(tokenOnCursor.toLowerCase());
            else
                return buttonText.toLowerCase().contains(tokenOnCursor.toLowerCase());
        } else {
            return false;
        }
    }
}
