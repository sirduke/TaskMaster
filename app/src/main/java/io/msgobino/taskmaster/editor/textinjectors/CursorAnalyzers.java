package io.msgobino.taskmaster.editor.textinjectors;

import io.msgobino.taskmaster.editor.CursorInfo;

public class CursorAnalyzers {
    private CursorAnalyzers() {
    }

    public static String getTokenOnCursor(String text, CursorInfo cursorInfo) {
        int start = 0;
        int end = text.length();

        if (cursorInfo.cursorStart() < 0 || cursorInfo.cursorStart() > text.length()) {
            return "";
        }

        if (start == end)
            return "";

        int index = cursorInfo.cursorStart();
        while (index > 0) {
            if (text.charAt(index - 1) == ' ') {
                start = index;
                break;
            }

            index--;
        }

        index = cursorInfo.cursorStart();
        while (index < text.length()) {
            if (text.charAt(index) == ' ') {
                end = index + 1;
                break;
            }

            index++;
        }

        return text.substring(start, end).trim();
    }
}
