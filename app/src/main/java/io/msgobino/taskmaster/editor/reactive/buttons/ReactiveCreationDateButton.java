package io.msgobino.taskmaster.editor.reactive.buttons;

import android.view.View;
import android.widget.Button;

import java.time.LocalDate;
import java.util.Objects;

import io.msgobino.taskmaster.data.analyzer.TaskAnalyzer;
import io.msgobino.taskmaster.data.modifier.TaskModifier;
import io.msgobino.taskmaster.editor.CursorInfo;
import io.msgobino.taskmaster.editor.EditingData;
import io.msgobino.taskmaster.utils.delegate.Setter;

public class ReactiveCreationDateButton extends ReactiveButton<EditingData> {
    private final Setter<EditingData> dataEditor;

    public ReactiveCreationDateButton(Button button, Setter<EditingData> dataEditor) {
        super(button);
        this.dataEditor = dataEditor;
    }

    @Override
    protected void buttonSetup(EditingData editingData) {
        if (Objects.nonNull(TaskAnalyzer.analyze(editingData.getText()).getCreationDate())) {
            setState(State.ACTIVE);
        } else {
            setState(State.INACTIVE);
        }
    }

    @Override
    protected void onClickReaction(View v, EditingData editingData) {
        TaskModifier modifier = TaskModifier.fromContent(editingData.getText());
        if (Objects.isNull(TaskAnalyzer.analyze(editingData.getText()).getCreationDate()))
            modifier.setCreationDate(LocalDate.now());
        else
            modifier.removeCreationDate();
        String modifiedText = modifier + " ";
        dataEditor.set(new EditingData(
                modifiedText,
                CursorInfo.after(modifiedText)
        ));
    }

    @Override
    protected void visibilitySetup(EditingData editingData) {
        ReactiveButtons.setVisibilityIfActive(button, editingData.getText());
    }
}
