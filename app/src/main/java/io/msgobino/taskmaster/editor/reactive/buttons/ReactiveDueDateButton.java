package io.msgobino.taskmaster.editor.reactive.buttons;

import android.view.View;
import android.widget.Button;

import com.google.android.material.datepicker.MaterialDatePicker;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Objects;
import java.util.function.Consumer;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.data.analyzer.TaskAnalyzer;
import io.msgobino.taskmaster.data.modifier.TaskModifier;
import io.msgobino.taskmaster.editor.CursorInfo;
import io.msgobino.taskmaster.editor.EditingData;
import io.msgobino.taskmaster.utils.delegate.Setter;

public class ReactiveDueDateButton extends ReactiveButton<EditingData> {
    private LocalDate selectedDueDate;
    private final Consumer<MaterialDatePicker<Long>> datePickerShowConsumer;
    private final Setter<EditingData> dataEditor;

    public ReactiveDueDateButton(Button hostedButton, Setter<EditingData> dataEditor, Consumer<MaterialDatePicker<Long>> datePickerShowConsumer) {
        super(hostedButton);
        this.dataEditor = dataEditor;
        this.datePickerShowConsumer = datePickerShowConsumer;
    }

    @Override
    protected void buttonSetup(EditingData editingData) {
        TaskAnalyzer analyzer = TaskAnalyzer.analyze(editingData.getText());
        LocalDate dueDate = analyzer.getDueDate();
        if (Objects.nonNull(dueDate)) {
            selectedDueDate = dueDate;
            button.setText(dueDate.toString());
            setState(State.ACTIVE);
        } else {
            button.setText(R.string.duedate_button_starting_text);
            setState(State.INACTIVE);
        }
    }

    @Override
    protected void onClickReaction(View v, EditingData editingData) {
        Calendar calendar = Calendar.getInstance();
        if (Objects.nonNull(selectedDueDate)) {
            calendar.set(Calendar.YEAR, selectedDueDate.getYear());
            calendar.set(Calendar.MONTH, selectedDueDate.getMonthValue() - 1); // +1 for internal month offset
            calendar.set(Calendar.DAY_OF_MONTH, selectedDueDate.getDayOfMonth());
        }
        MaterialDatePicker<Long> datePicker = MaterialDatePicker.Builder.datePicker()
                .setTitleText("Due date picker")
                .setSelection(calendar.getTimeInMillis())
                .setInputMode(MaterialDatePicker.INPUT_MODE_CALENDAR)
                .build();
        datePicker.addOnPositiveButtonClickListener(dialog -> {
            if (Objects.nonNull(datePicker.getSelection())) {
                long dateLongMillis = datePicker.getSelection();
                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(dateLongMillis);
                selectedDueDate = LocalDate.of(
                        c.get(Calendar.YEAR),
                        c.get(Calendar.MONTH) + 1, // -1 for internal month offset
                        c.get(Calendar.DAY_OF_MONTH)
                );
                String modifiedText = TaskModifier.fromContent(editingData.getText())
                        .setDueDate(selectedDueDate)
                        .toString() + " ";
                dataEditor.set(new EditingData(
                        modifiedText,
                        CursorInfo.after(modifiedText)
                ));
            }
        });

        datePickerShowConsumer.accept(datePicker);
    }

    @Override
    protected void visibilitySetup(EditingData editingData) {
        ReactiveButtons.setVisibilityIfActive(button, editingData.getText());
    }
}
