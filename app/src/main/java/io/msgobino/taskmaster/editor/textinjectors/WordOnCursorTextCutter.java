package io.msgobino.taskmaster.editor.textinjectors;

import io.msgobino.taskmaster.editor.CursorInfo;
import io.msgobino.taskmaster.utils.Pair;

public class WordOnCursorTextCutter implements TextCutter {
    private final CursorInfo cursorInfo;

    public WordOnCursorTextCutter(CursorInfo cursorInfo) {
        this.cursorInfo = cursorInfo;
    }

    @Override
    public Pair<Integer, Integer> locateCut(String text) {
        int highCut = 0;
        int lowCut = text.length();
        int cursorPosition = cursorInfo.cursorStart();

        if (lowCut == highCut)
            return new Pair<>(cursorInfo.cursorStart(), cursorInfo.cursorStart());

        if (cursorInfo.cursorStart() < 0) {
            cursorPosition = 0;
        }

        if (cursorInfo.cursorStart() > text.length()) {
            cursorPosition = text.length();
        }

        lowCut = determineLowCut(text, cursorPosition);
        highCut = determineHighCut(text, cursorPosition);

        return new Pair<>(lowCut, highCut);
    }

    private static int determineLowCut(String text, int cursorStart) {
        int index = cursorStart;
        while (index > 0) {
            if (text.charAt(index - 1) == ' ')
                return index;
            index--;
        }

        return 0;
    }

    private static int determineHighCut(String text, int cursorStart) {
        int i = cursorStart;
        while (i < text.length()) {
            if (text.charAt(i) == ' ')
                return i + 1;

            i++;
        }
        return text.length();
    }
}
