package io.msgobino.taskmaster.editor.reactive.buttons;

import android.content.Context;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.data.analyzer.TaskAnalyzer;
import io.msgobino.taskmaster.data.core.Priority;
import io.msgobino.taskmaster.data.modifier.TaskModifier;
import io.msgobino.taskmaster.editor.CursorInfo;
import io.msgobino.taskmaster.editor.EditingData;
import io.msgobino.taskmaster.utils.delegate.Setter;

public class ReactivePriorityButton extends ReactiveButton<EditingData> {
    private final Context popupMenuThemeWrapper;
    private final Setter<EditingData> dataEditor;

    public ReactivePriorityButton(Button hostedButton, Setter<EditingData> dataEditor) {
        super(hostedButton);
        this.dataEditor = dataEditor;
        popupMenuThemeWrapper = new ContextThemeWrapper(hostedButton.getContext(), R.style.PopupMenu);
    }

    @Override
    protected void buttonSetup(EditingData editingData) {
        Priority priority = TaskAnalyzer.analyze(editingData.getText()).getPriority();
        String priorityLetter = priority.asLetter();
        if (priorityLetter.isEmpty()) {
            button.setText(R.string.priority_menu_no_priority);
            setState(State.INACTIVE);
        } else {
            button.setText(priorityLetter);
            setState(State.ACTIVE);
        }
    }

    @Override
    protected void onClickReaction(View v, EditingData editingData) {
        PopupMenu popupMenu = new PopupMenu(popupMenuThemeWrapper, button);
        popupMenu.getMenuInflater().inflate(R.menu.priority_menu, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(onPriorityMenuClickListenerFactoryMethod(editingData.getText()));

        popupMenu.show();
    }

    @Override
    protected void visibilitySetup(EditingData editingData) {
        ReactiveButtons.setVisibilityIfActive(button, editingData.getText());
    }

    private PopupMenu.OnMenuItemClickListener onPriorityMenuClickListenerFactoryMethod(String text) {
        return item -> {
            int id = item.getItemId();
            TaskModifier modifier = TaskModifier.fromContent(text);
            Priority priority;
            if (id == R.id.priority_menu_no_priority) {
                priority = Priority.none();
            } else {
                priority = Priority.fromLetter(String.valueOf(item.getTitle()));
            }
            modifier.setPriority(priority);
            String modifiedText = modifier + " ";
            dataEditor.set(new EditingData(
                    modifiedText,
                    CursorInfo.after(modifiedText)
            ));
            return true;
        };
    }

    @Override
    public void setState(State state) {
        super.setState(state);
    }
}
