package io.msgobino.taskmaster.editor.reactive.buttons;

import android.widget.Button;

import java.util.Comparator;

public class ButtonTextComparator implements Comparator<Button> {
    @Override
    public int compare(Button o1, Button o2) {
        String buttonText1 = o1.getText().toString();
        String buttonText2 = o2.getText().toString();
        int ignoreCaseComparison = buttonText1.compareToIgnoreCase(buttonText2);
        return ignoreCaseComparison != 0
                ? ignoreCaseComparison
                : buttonText1.compareTo(buttonText2);
    }
}
