package io.msgobino.taskmaster.welcome.walkthrough;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.preference.PreferenceManager;

import com.google.android.material.progressindicator.LinearProgressIndicator;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.TaskMasterApp;
import io.msgobino.taskmaster.taskslist.TaskListActivity;
import io.msgobino.taskmaster.welcome.WelcomeActivity;

public class WalkthroughActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        TaskMasterApp.loadTheme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.walkthrough);
    }

    @Override
    protected void onStart() {
        boolean showWelcomeFirstTime = PreferenceManager.getDefaultSharedPreferences(this)
                .getBoolean(WelcomeActivity.SHOW_WELCOME_PREFERENCE_KEY, true);
        LinearProgressIndicator linearProgressIndicator = findViewById(R.id.linear_progress_indicator);
        Button goNextButton = findViewById(R.id.go_next_button);
        goNextButton.setAlpha(0.2f);
        goNextButton.setOnClickListener(v -> {
        });
        ImageView taskMasterLamp = findViewById(R.id.lamp);
        ScrollView scrollView = findViewById(R.id.scroll_view);
        scrollView.getViewTreeObserver()
                .addOnScrollChangedListener(() -> {
                    linearProgressIndicator.setProgressCompat(computeProgress(scrollView), true);
                    if (scrollView.getChildAt(0).getBottom()
                            <= (scrollView.getHeight() + scrollView.getScrollY())) {
                        goNextButton.setAlpha(1.0f);
                        goNextButton.setOnClickListener(v -> {
                            PreferenceManager.getDefaultSharedPreferences(this).edit()
                                    .putBoolean(WelcomeActivity.SHOW_WELCOME_PREFERENCE_KEY, false)
                                    .apply();
                            if (showWelcomeFirstTime) {
                                Intent getToTaskApp = new Intent(WalkthroughActivity.this, TaskListActivity.class);
                                getToTaskApp.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(getToTaskApp);
                                finish();
                            } else {
                                finish();
                            }
                        });
                        taskMasterLamp.setImageDrawable(AppCompatResources.getDrawable(this, R.drawable.ic_launcher_light_bulb_monochrome_rays_on_png));
                    }
                });
        super.onStart();
    }

    private int computeProgress(ScrollView scrollView) {
        int num = scrollView.getScrollY() + scrollView.getHeight();
        int den = scrollView.getChildAt(0).getHeight();
        return (int) (num / (double) den * 100);
    }
}
