package io.msgobino.taskmaster.welcome;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.documentfile.provider.DocumentFile;

import java.util.Objects;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.TaskMasterApp;
import io.msgobino.taskmaster.settings.KnownPreferencesManager;

public class FolderLoaderActivity extends AppCompatActivity {
    public static final int PICK_FILE = 0;
    private Button confirmButton;
    private TextView loaderResultTextView;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == PICK_FILE) {
            if (resultCode == RESULT_OK) {
                getContentResolver().takePersistableUriPermission(data.getData(),
                                Intent.FLAG_GRANT_READ_URI_PERMISSION
                                | Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                );
                KnownPreferencesManager.saveTodoTxtPath(data.getData().toString());
                confirmButton.setVisibility(View.VISIBLE);
                DocumentFile documentFile = DocumentFile.fromTreeUri(this, data.getData());
                String folderName = documentFile.getName();
                String displayedFolderName = Objects.nonNull(folderName)
                        ? folderName
                        : getString(R.string.no_folder_selected);
                String resultText = getString(R.string.loader_result_text_prefix) + ": " + displayedFolderName;
                loaderResultTextView.setText(resultText);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        TaskMasterApp.loadTheme(this);
        setContentView(R.layout.folder_loader_activity);
        String todoTxtPath = KnownPreferencesManager.getTodoTxtPath();
        Button pickFileButton = findViewById(R.id.folder_loader_sync_folder_button);
        pickFileButton.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
            intent.setFlags(
                    Intent.FLAG_GRANT_READ_URI_PERMISSION
                            | Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                            | Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION
                            | Intent.FLAG_GRANT_PREFIX_URI_PERMISSION
            );
            //noinspection deprecation
            startActivityForResult(intent, PICK_FILE);
        });

        confirmButton = findViewById(R.id.folder_loader_button_confirm);
        confirmButton.setOnClickListener(v -> finish());
        loaderResultTextView = findViewById(R.id.loader_result);
        if (Objects.nonNull(todoTxtPath)) {
            DocumentFile documentFile = DocumentFile.fromTreeUri(this, Uri.parse(todoTxtPath));
            String resultText = getString(R.string.loader_result_text_prefix) + ": " + documentFile.getName();
            loaderResultTextView.setText(resultText);
            confirmButton.setVisibility(View.VISIBLE);
        }

        getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                String todoTxtPath = KnownPreferencesManager.getTodoTxtPath();
                if (Objects.nonNull(todoTxtPath)) {
                    finish();
                } else {
                    finishAffinity();
                }
            }
        });

        super.onCreate(savedInstanceState);
    }
}
