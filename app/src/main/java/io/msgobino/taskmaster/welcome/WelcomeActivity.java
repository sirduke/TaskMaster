package io.msgobino.taskmaster.welcome;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.TaskMasterApp;
import io.msgobino.taskmaster.taskslist.TaskListActivity;

public class WelcomeActivity extends AppCompatActivity {
    public static final String SHOW_WELCOME_PREFERENCE_KEY = TaskMasterApp.getStringById(R.string.show_welcome_key);
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        TaskMasterApp.loadTheme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_activity);


        boolean showNextStartup = PreferenceManager.getDefaultSharedPreferences(this)
                .getBoolean(WelcomeActivity.SHOW_WELCOME_PREFERENCE_KEY, true);
        if (!showNextStartup) {
            Intent getToTaskApp = new Intent(WelcomeActivity.this, TaskListActivity.class);
            getToTaskApp.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(getToTaskApp);
            finish();
        }
    }
}
