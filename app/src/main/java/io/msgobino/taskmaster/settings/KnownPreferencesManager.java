package io.msgobino.taskmaster.settings;

import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.TaskMasterApp;
import io.msgobino.taskmaster.utils.DateRange;
import io.msgobino.taskmaster.viewmodel.tasks.FilterController;

public class KnownPreferencesManager {
    private KnownPreferencesManager() {
    }

    public static String getTodoTxtPath() {
        return PreferenceManager.getDefaultSharedPreferences(TaskMasterApp.getContext())
                .getString(KnownPreferences.TODO_TXT_PATH.key, Defaults.TODO_TXT_PATH);
    }

    public static void saveTodoTxtPath(String todoTxtPath) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(TaskMasterApp.getContext()).edit();
        editor.putString(KnownPreferencesManager.KnownPreferences.TODO_TXT_PATH.key(), todoTxtPath);
        editor.apply();
    }

    public static boolean getIsSyntaxHighlighting() {
        return PreferenceManager.getDefaultSharedPreferences(TaskMasterApp.getContext())
                .getBoolean(KnownPreferences.SYNTAX_HIGHLIGHTING.key, Defaults.SYNTAX_HIGHLIGHTING);
    }

    public static boolean getIsSearchHighlighting() {
        return PreferenceManager.getDefaultSharedPreferences(TaskMasterApp.getContext())
                .getBoolean(KnownPreferences.SEARCH_HIGHLIGHTING.key, Defaults.SEARCH_HIGHLIGHTING);
    }

    public static boolean getIsTimePickerFormat24h() {
        return PreferenceManager.getDefaultSharedPreferences(TaskMasterApp.getContext())
                .getBoolean(KnownPreferences.TIME_PICKER_FORMAT.key, Defaults.TIME_PICKER_FORMAT);
    }

    public static String getTodoTxtFilename() {
        return PreferenceManager.getDefaultSharedPreferences(TaskMasterApp.getContext())
                .getString(KnownPreferences.TODO_TXT_FILENAME.key, Defaults.TODO_TXT_FILENAME);
    }

    public static void saveTodoTxtFilename(String todoTxtFilename) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(TaskMasterApp.getContext()).edit();
        editor.putString(KnownPreferences.TODO_TXT_FILENAME.key(), todoTxtFilename);
        editor.apply();
    }

    public static FilterController.KeywordMode getKeywordMode() {
        try {
            return FilterController.KeywordMode.valueOf(PreferenceManager.getDefaultSharedPreferences(TaskMasterApp.getContext())
                    .getString(KnownPreferences.KEYWORD_MODE.key, Defaults.KEYWORD_MODE));
        } catch (IllegalArgumentException e) {
            return FilterController.KeywordMode.ANY;
        }
    }

    public static void saveKeywordMode(FilterController.KeywordMode keywordMode) {
        PreferenceManager.getDefaultSharedPreferences(TaskMasterApp.getContext()).edit()
                .putString(KnownPreferencesManager.KnownPreferences.KEYWORD_MODE.key(), keywordMode.toString())
                .apply();
    }

    public static boolean getIsDynamicEditing() {
        return PreferenceManager.getDefaultSharedPreferences(TaskMasterApp.getContext())
                .getBoolean(KnownPreferences.DYNAMIC_EDITING.key, Defaults.DYNAMIC_EDITING);
    }

    public static boolean getIsInEditorHighlighting() {
        return PreferenceManager.getDefaultSharedPreferences(TaskMasterApp.getContext())
                .getBoolean(KnownPreferences.IN_EDITOR_HIGHLIGHTING.key, Defaults.IN_EDITOR_HIGHLIGHTING);
    }

    public static boolean getIsAutomaticFileCleaning() {
        return PreferenceManager.getDefaultSharedPreferences(TaskMasterApp.getContext())
                .getBoolean(KnownPreferences.AUTOMATIC_FILE_CLEANING.key, Defaults.AUTOMATIC_FILE_CLEANING);
    }

    public static DateRange getPendingDueDateRange() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(TaskMasterApp.getContext());
        String dueDateRangeStartString = sharedPreferences.getString(KnownPreferences.DUE_DATE_RANGE_START.key, String.valueOf(Defaults.DUE_DATE_RANGE_START));
        String dueDateRangeEndString = sharedPreferences.getString(KnownPreferences.DUE_DATE_RANGE_END.key, String.valueOf(Defaults.DUE_DATE_RANGE_END));
        LocalDate dueDateStart;
        LocalDate dueDateEnd;
        int dueDateRangeStart;
        int dueDateRangeEnd;

        try {
            dueDateStart = LocalDate.parse(dueDateRangeStartString);
        } catch (DateTimeParseException e) {
            try {
                dueDateRangeStart = Integer.parseInt(dueDateRangeStartString);
            } catch (NumberFormatException ex) {
                dueDateRangeStart = Defaults.DUE_DATE_RANGE_START;
            }
            dueDateStart = LocalDate.now().plusDays(dueDateRangeStart);
        }

        try {
            dueDateEnd = LocalDate.parse(dueDateRangeEndString);
        } catch (DateTimeParseException e) {
            try {
                dueDateRangeEnd = Integer.parseInt(dueDateRangeEndString);
            } catch (NumberFormatException ex) {
                dueDateRangeEnd = Defaults.DUE_DATE_RANGE_END;
            }
            dueDateEnd = LocalDate.now().plusDays(dueDateRangeEnd);
        }

        return new DateRange(dueDateStart, dueDateEnd);
    }


    public static void savePendingDueDateRange(DateRange dueDateRange) {
        if (dueDateRange.startsToday()) {
            int dueDateRangeStart = dueDateRange.startDaysFromToday();
            int dueDateRangeEnd = dueDateRange.endDaysFromToday();
            if (dueDateRangeStart > dueDateRangeEnd) {
                dueDateRangeStart = Defaults.DUE_DATE_RANGE_START;
                dueDateRangeEnd = Defaults.DUE_DATE_RANGE_END;
            }
            PreferenceManager.getDefaultSharedPreferences(TaskMasterApp.getContext()).edit()
                    .putString(KnownPreferences.DUE_DATE_RANGE_START.key, String.valueOf(dueDateRangeStart))
                    .putString(KnownPreferences.DUE_DATE_RANGE_END.key, String.valueOf(dueDateRangeEnd))
                    .apply();
        } else {
            PreferenceManager.getDefaultSharedPreferences(TaskMasterApp.getContext()).edit()
                    .putString(KnownPreferences.DUE_DATE_RANGE_START.key, dueDateRange.start().toString())
                    .putString(KnownPreferences.DUE_DATE_RANGE_END.key, dueDateRange.end().toString())
                    .apply();
        }
    }

    public static TaskMasterApp.Theme getTheme() {
        String themeName = PreferenceManager.getDefaultSharedPreferences(TaskMasterApp.getContext())
                .getString(KnownPreferences.THEME.key, Defaults.THEME);

        try {
            return TaskMasterApp.Theme.valueOf(themeName);
        } catch (IllegalArgumentException e) {
            return TaskMasterApp.Theme.ULTRAMARINE;
        }
    }

    public static void saveTheme(TaskMasterApp.Theme theme) {
        PreferenceManager.getDefaultSharedPreferences(TaskMasterApp.getContext()).edit()
                .putString(KnownPreferences.THEME.key, theme.toString())
                .apply();
    }


    public enum KnownPreferences {
        TODO_TXT_PATH(TaskMasterApp.getStringById(R.string.todotxt_path_key)),
        SYNTAX_HIGHLIGHTING(TaskMasterApp.getStringById(R.string.syntax_highlighting_key)),
        TIME_PICKER_FORMAT(TaskMasterApp.getStringById(R.string.time_picker_format_key)),
        SEARCH_HIGHLIGHTING(TaskMasterApp.getStringById(R.string.search_highlighting_key)),
        TODO_TXT_FILENAME(TaskMasterApp.getStringById(R.string.todotxt_filename_key)),
        AUTOMATIC_FILE_CLEANING(TaskMasterApp.getStringById(R.string.automatic_file_cleaning_key)),

        THEME(TaskMasterApp.getStringById(R.string.theme_key)),

        WALKTHROUGH(TaskMasterApp.getStringById(R.string.walkthrough_key)),

        ABOUT(TaskMasterApp.getStringById(R.string.about_key)),

        KEYWORD_MODE(TaskMasterApp.getStringById(R.string.keyword_mode_key)),

        DYNAMIC_EDITING(TaskMasterApp.getStringById(R.string.dynamic_editing_key)),

        DUE_DATE_RANGE_START(TaskMasterApp.getStringById(R.string.due_date_range_start_key)),

        DUE_DATE_RANGE_END(TaskMasterApp.getStringById(R.string.due_date_range_end_key)),

        IN_EDITOR_HIGHLIGHTING(TaskMasterApp.getStringById(R.string.in_editor_highlighting_key));

        private final String key;

        KnownPreferences(String key) {
            this.key = key;
        }

        public String key() {
            return key;
        }
    }

    public static class Defaults {
        public static final String TODO_TXT_PATH = null;
        public static final String TODO_TXT_FILENAME = "todo.txt";
        public static final String FILE_SUFFIX = "." + TODO_TXT_FILENAME;
        public static final boolean SYNTAX_HIGHLIGHTING = true;
        public static final boolean SEARCH_HIGHLIGHTING = true;
        public static final boolean TIME_PICKER_FORMAT = true;
        public static final String THEME = "MATERIAL_YOU";
        public static final String KEYWORD_MODE = "ANY";
        public static final boolean DYNAMIC_EDITING = true;
        public static final int DUE_DATE_RANGE_START = 0;
        public static final int DUE_DATE_RANGE_END = 7;
        public static final boolean IN_EDITOR_HIGHLIGHTING = false;
        public static final boolean AUTOMATIC_FILE_CLEANING = true;
    }
}
