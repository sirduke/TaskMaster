package io.msgobino.taskmaster.settings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import java.util.Objects;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.TaskMasterApp;
import io.msgobino.taskmaster.about.AboutActivity;
import io.msgobino.taskmaster.welcome.FolderLoaderActivity;
import io.msgobino.taskmaster.welcome.walkthrough.WalkthroughActivity;

/**
 * @noinspection DataFlowIssue
 */
public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {
    @Override
    public void onCreatePreferences(@Nullable Bundle savedInstanceState, @Nullable String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);
        Preference todoTxtPath = findPreference(KnownPreferencesManager.KnownPreferences.TODO_TXT_PATH.key());

        todoTxtPath.setOnPreferenceClickListener(preference -> {
            Intent getToFolderLoaderPage = new Intent(requireContext(), FolderLoaderActivity.class);
            startActivity(getToFolderLoaderPage);
            return true;
        });

        boolean use24hFormat = KnownPreferencesManager.getIsTimePickerFormat24h();
        Preference the24hFormatPreference = findPreference(KnownPreferencesManager.KnownPreferences.TIME_PICKER_FORMAT.key());
        if (use24hFormat)
            the24hFormatPreference.setTitle(R.string.time_picker_uses_24h_format);
        else
            the24hFormatPreference.setTitle(R.string.time_picker_uses_12h_format);

        Preference walkthrough = findPreference(KnownPreferencesManager.KnownPreferences.WALKTHROUGH.key());
        walkthrough.setOnPreferenceClickListener(preference -> {
            Intent getToWalkthrough = new Intent(requireContext(), WalkthroughActivity.class);
            startActivity(getToWalkthrough);
            return true;
        });

        Preference about = findPreference(KnownPreferencesManager.KnownPreferences.ABOUT.key());
        about.setOnPreferenceClickListener(preference -> {
            Intent getToAboutPage = new Intent(requireContext(), AboutActivity.class);
            startActivity(getToAboutPage);
            return true;
        });
    }

    @Override
    public void onResume() {
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        super.onResume();
    }

    @Override
    public void onPause() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, @Nullable String key) {
        if (Objects.equals(key, KnownPreferencesManager.KnownPreferences.TIME_PICKER_FORMAT.key())) {
            boolean use24hFormat = KnownPreferencesManager.getIsTimePickerFormat24h();

            Preference the24hFormatPreference = findPreference(KnownPreferencesManager.KnownPreferences.TIME_PICKER_FORMAT.key());
            if (use24hFormat)
                the24hFormatPreference.setTitle(R.string.time_picker_uses_24h_format);
            else
                the24hFormatPreference.setTitle(R.string.time_picker_uses_12h_format);
        } else if (Objects.equals(key, KnownPreferencesManager.KnownPreferences.THEME.key())) {
            TaskMasterApp.updateTheme();
            TaskMasterApp.loadTheme((AppCompatActivity) requireActivity());
            TaskMasterApp.setSplashScreenTheme((AppCompatActivity) requireActivity());
            requireActivity().recreate();
        }
    }
}
