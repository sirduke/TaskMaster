package io.msgobino.taskmaster.adapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import io.msgobino.taskmaster.adapter.diff.AddDiff;
import io.msgobino.taskmaster.adapter.diff.ChangeDiff;
import io.msgobino.taskmaster.adapter.diff.DelDiff;
import io.msgobino.taskmaster.adapter.diff.Diff;
import io.msgobino.taskmaster.adapter.diff.MoveDiff;
import io.msgobino.taskmaster.data.core.Task;
import io.msgobino.taskmaster.utils.factory.Factory;

public class AdapterUtils {

    private AdapterUtils() {
    }

    public static List<Diff> makeDiff(List<Task> first, List<Task> second) {
        List<Diff> diffs = new ArrayList<>();

        // Creates remaining diffs by transforming first list in second list and adding diffs on-the-fly
        List<Task> helperList = new ArrayList<>(first);

        int deletionOffset = 0;
        for (Task task : first) {
            if (!second.contains(task)) {
                helperList.remove(task);
                diffs.add(new DelDiff(first.indexOf(task) + deletionOffset));
                deletionOffset = deletionOffset - 1;
            }
        }

        for (Task task : second) {
            if (helperList.contains(task)) {
                int firstIndexOf = helperList.indexOf(task);
                int secondIndexOf = second.indexOf(task);
                if (firstIndexOf != secondIndexOf) {
                    diffs.add(new MoveDiff(firstIndexOf, secondIndexOf));
                    helperList.remove(task);
                    if (secondIndexOf < helperList.size()) {
                        helperList.add(secondIndexOf, task);
                    } else {
                        helperList.add(task);
                    }
                }
            } else {
                int secondIndexOf = second.indexOf(task);
                diffs.add(new AddDiff(secondIndexOf));
                if (secondIndexOf < helperList.size()) {
                    helperList.add(secondIndexOf, task);
                } else {
                    helperList.add(task);
                }
            }
        }

        return diffs;
    }

    public static List<ChangeDiff> makeSearchSyntaxHighlightDiff(List<Task> tasks, String query) {
        List<String> queryTokens = Arrays.asList(query.split(" "));
        // If task contains any token from search query, add to list
        Factory<String, Predicate<Task>> checkerFactory = token -> task -> task.contains(token);
        List<Predicate<Task>> checkers = new ArrayList<>();
        queryTokens.forEach(token -> checkers.add(checkerFactory.build(token)));

        return tasks.stream()
                .filter(task -> checkers.stream().anyMatch(checker -> checker.test(task)))
                .map(highlightedTask -> new ChangeDiff(tasks.indexOf(highlightedTask)))
                .collect(Collectors.toList());
    }
}
