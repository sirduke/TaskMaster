package io.msgobino.taskmaster.adapter.diff;

public abstract class Diff {
    private final int position;

    public int getPosition() {
        return position;
    }

    protected Diff(int position) {
        this.position = position;
    }
}
