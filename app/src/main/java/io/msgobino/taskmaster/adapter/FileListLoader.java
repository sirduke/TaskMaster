package io.msgobino.taskmaster.adapter;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import io.msgobino.taskmaster.utils.storage.StorageDestination;
import io.msgobino.taskmaster.utils.storage.SyncStorageDirectory;

public class FileListLoader {
    private final MutableLiveData<List<StorageDestination>> liveStorageDestinations = new MutableLiveData<>(new ArrayList<>());
    private final MutableLiveData<Boolean> liveIsBusyListingFiles = new MutableLiveData<>(false);
    private final SyncStorageDirectory syncStorageDirectory;


    public FileListLoader(SyncStorageDirectory syncStorageDirectory) {
        this.syncStorageDirectory = syncStorageDirectory;
    }

    public List<StorageDestination> getList() {
        return liveStorageDestinations.getValue();
    }

    public StorageDestination get(int position) {
        return liveStorageDestinations.getValue().get(position);
    }

    public int size() {
        return liveStorageDestinations.getValue().size();
    }

    public LiveData<Boolean> getIsBusyListingFiles() {
        return liveIsBusyListingFiles;
    }

    public LiveData<List<StorageDestination>> getLiveStorageDestinations() {
        return liveStorageDestinations;
    }

    public void load() {
        liveIsBusyListingFiles.postValue(true);
        List<StorageDestination> destinations = syncStorageDirectory.getAllDestinations();
        destinations.sort(Comparator.naturalOrder());
        liveStorageDestinations.postValue(destinations);
        liveIsBusyListingFiles.postValue(false);
    }

}
