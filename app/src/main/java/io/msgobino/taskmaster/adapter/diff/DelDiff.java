package io.msgobino.taskmaster.adapter.diff;

import androidx.annotation.NonNull;

public class DelDiff extends Diff {
    @NonNull
    @Override
    public String toString() {
        return "DelDiff -> Position " + getPosition();
    }

    public DelDiff(int position) {
        super(position);
    }
}
