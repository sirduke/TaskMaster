package io.msgobino.taskmaster.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.progressindicator.CircularProgressIndicator;

import java.io.IOException;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Predicate;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.TaskMasterApp;
import io.msgobino.taskmaster.data.analyzer.TaskAnalyzer;
import io.msgobino.taskmaster.data.core.TasksList;
import io.msgobino.taskmaster.utils.messages.Formatters;
import io.msgobino.taskmaster.utils.storage.StorageDestination;

public class FileListAdapter extends RecyclerView.Adapter<FileListAdapter.FileViewHolder> {
    private final FileListLoader fileListLoader;
    private final LifecycleOwner lifecycleOwner;
    private final ConcurrentMap<Integer, String> fileInfoCacheMap = new ConcurrentHashMap<>();
    private final Set<FileViewHolder> collectedViewHolders = new HashSet<>();
    private String filename = null;

    @SuppressLint("NotifyDataSetChanged")
    public FileListAdapter(FileListLoader fileListLoader, LifecycleOwner lifecycleOwner) {
        this.fileListLoader = fileListLoader;
        this.lifecycleOwner = lifecycleOwner;
        fileListLoader.getLiveStorageDestinations().observe(lifecycleOwner, list -> notifyDataSetChanged());
        TaskMasterApp.defer(fileListLoader::load);
    }

    @Override
    public void onViewAttachedToWindow(@NonNull FileViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        collectedViewHolders.add(holder);
    }

    @Override
    public void onViewRecycled(@NonNull FileViewHolder holder) {
        super.onViewRecycled(holder);
        collectedViewHolders.remove(holder);
    }

    public void updateHighlight(String filename) {
        this.filename = filename;
        collectedViewHolders.forEach(FileViewHolder::evaluateHighlighting);
    }

    @NonNull
    @Override
    public FileViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.file_list_item_layout, parent, false);
        view.setBackgroundColor(TaskMasterApp.getColorByAttr(R.attr.colorSurfaceContainerHigh));
        return new FileViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FileViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return fileListLoader.size();
    }

    public String getPrefix(int position) {
        return formatFilename(fileListLoader.get(position));
    }

    private String formatFilename(StorageDestination storageDestination) {
        return Formatters.extractFilenamePrefix(storageDestination.getFilename());
    }

    private OnClickListener onClickListener = (v, p) -> {
    };

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @FunctionalInterface
    public interface OnClickListener {
        void onClick(View view, int position);
    }

    public class FileViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView filenameTextView;
        private final TextView linesCountTextView;
        private final CircularProgressIndicator cip;
        private final MutableLiveData<Boolean> liveIsBusyScrapingFile = new MutableLiveData<>(true);
        private final MutableLiveData<String> liveStats = new MutableLiveData<>(TaskMasterApp.getStringById(R.string.no_items));
        private final Predicate<String> shouldHighlight = fn -> fn != null
                && Formatters.extractFilenamePrefix(getFilename()).equals(fn);

        public FileViewHolder(View view) {
            super(view);
            filenameTextView = view.findViewById(R.id.text_view_file_list_item);
            linesCountTextView = view.findViewById(R.id.file_lines_count_text_view);
            cip = view.findViewById(R.id.file_inspector_circular_progress_indicator);

            liveIsBusyScrapingFile.observe(lifecycleOwner, isBusy -> {
                if (isBusy) {
                    cip.setVisibility(View.VISIBLE);
                    linesCountTextView.setVisibility(View.GONE);
                } else {
                    cip.setVisibility(View.GONE);
                    linesCountTextView.setVisibility(View.VISIBLE);
                }
            });
            cip.setVisibility(View.GONE);

            liveStats.observe(lifecycleOwner, stats -> {
                linesCountTextView.setText(stats);
                linesCountTextView.setVisibility(View.VISIBLE);
            });

            view.setOnClickListener(this);
        }

        public void bind(int position) {
            setText(fileListLoader.get(position).getFilename());
            scrapeDestination(fileListLoader.get(position));
            evaluateHighlighting();
        }

        public String getFilename() {
            return filenameTextView.getText().toString();
        }

        public void setText(String text) {
            filenameTextView.setText(text);
        }

        public void scrapeDestination(StorageDestination storageDestination) {
            TaskMasterApp.defer(() -> {
                String cachedResult = fileInfoCacheMap.get(getBindingAdapterPosition());
                if (cachedResult == null || cachedResult.equals(TaskMasterApp.getStringById(R.string.no_items))) {
                    liveIsBusyScrapingFile.postValue(true);
                    try {
                        TasksList supportTasksList = TasksList.fromStorageDestination(storageDestination);
                        long tasksCount = supportTasksList.size();
                        long activeTasksCount = supportTasksList.stream()
                                .filter(task -> TaskAnalyzer.analyze(task).isActive())
                                .count();
                        String formattedStats = formatStats(tasksCount, activeTasksCount);
                        liveStats.postValue(formattedStats);
                        fileInfoCacheMap.put(getBindingAdapterPosition(), formattedStats);
                    } catch (IOException e) {
                        liveStats.postValue(TaskMasterApp.getStringById(R.string.no_items));
                    }
                    liveIsBusyScrapingFile.postValue(false);
                } else {
                    liveStats.postValue(cachedResult);
                }
            });
        }

        private String formatStats(long tasksCount, long activeTasksCount) {
            return activeTasksCount + " " + Formatters.surroundBrackets(String.valueOf(tasksCount));
        }

        @Override
        public void onClick(View view) {
            onClickListener.onClick(view, getBindingAdapterPosition());
        }

        public void evaluateHighlighting() {
            if (Objects.nonNull(filename))
                setHighlighting(shouldHighlight.test(filename));
        }

        private void setHighlighting(boolean highlight) {
            if (highlight) {
                filenameTextView.setTextColor(TaskMasterApp.getColorByAttr(com.google.android.material.R.attr.colorTertiary));
                linesCountTextView.setTextColor(TaskMasterApp.getColorByAttr(com.google.android.material.R.attr.colorTertiary));
            } else {
                filenameTextView.setTextColor(TaskMasterApp.getColorByAttr(com.google.android.material.R.attr.colorOnBackground));
                linesCountTextView.setTextColor(TaskMasterApp.getColorByAttr(com.google.android.material.R.attr.colorOnBackground));
            }
        }
    }
}
