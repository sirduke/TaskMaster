package io.msgobino.taskmaster.adapter.diff;

import androidx.annotation.NonNull;

public class AddDiff extends Diff {
    @NonNull
    @Override
    public String toString() {
        return "AddDiff -> Position " + getPosition();
    }

    public AddDiff(int position) {
        super(position);
    }
}
