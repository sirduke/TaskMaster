package io.msgobino.taskmaster.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.adapter.diff.AddDiff;
import io.msgobino.taskmaster.adapter.diff.ChangeDiff;
import io.msgobino.taskmaster.adapter.diff.DelDiff;
import io.msgobino.taskmaster.adapter.diff.Diff;
import io.msgobino.taskmaster.adapter.diff.MoveDiff;
import io.msgobino.taskmaster.data.core.Task;
import io.msgobino.taskmaster.settings.KnownPreferencesManager;
import io.msgobino.taskmaster.taskslist.ScrollManager;
import io.msgobino.taskmaster.utils.highlighters.SyntaxHighlighting;

public class TaskListAdapter extends ListAdapter<Task> {
    private final Context context;
    private List<Task> outputList = new ArrayList<>();
    private final AdapterDiffManager diffManager;

    private BiConsumer<View, Integer> positionModifier = (v, i) -> {
    };
    private OnItemClickListener onItemClickListener = (view, position) -> {
    };
    private OnItemLongClickListener onItemLongClickListener = (view, position) -> {
    };
    private String searchQuery;

    public TaskListAdapter(
            Context context,
            LiveData<List<Task>> liveList,
            LifecycleOwner lifecycleOwner,
            ScrollManager scrollManager) {
        this.context = context;
        this.diffManager = new AdapterDiffManager(this, scrollManager);

        Observer<List<Task>> listObserver = tasks -> {
            List<Diff> diffs = AdapterUtils.makeDiff(outputList, tasks);
            outputList = tasks;

            diffManager.notifyDataSetDiffChanges(diffs);
        };
        liveList.observe(lifecycleOwner, listObserver);

        this.searchQuery = "";
    }

    @Override
    public void onSelectionChange(List<Integer> positions) {
        positions.forEach(this::notifyItemChanged);
    }

    private class TasksListViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, View.OnLongClickListener {
        private final TextView textView;

        TasksListViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.text_view_file_list_item);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void bind(int position) {
            Task boundTask = outputList.get(position);
            textView.setText(boundTask.content(), TextView.BufferType.EDITABLE);
            SyntaxHighlighting.removeSpans(textView.getEditableText(), Set.of(ForegroundColorSpan.class, StyleSpan.class));
            if (KnownPreferencesManager.getIsSyntaxHighlighting())
                SyntaxHighlighting.highlight(textView.getEditableText());
            if (KnownPreferencesManager.getIsSearchHighlighting())
                SyntaxHighlighting.highlightQuery(textView.getEditableText(), searchQuery);
        }

        public void applyExternalPositionModifier(int position) {
            positionModifier.accept(itemView, position);
        }

        @Override
        public boolean onLongClick(View view) {
            onItemLongClickListener.onLongClick(view, getBindingAdapterPosition());
            return true;
        }

        @Override
        public boolean onLongClickUseDefaultHapticFeedback(@NonNull View v) {
            return View.OnLongClickListener.super.onLongClickUseDefaultHapticFeedback(v);
        }

        @Override
        public void onClick(View view) {
            onItemClickListener.onClick(view, getBindingAdapterPosition());
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TasksListViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.task_list_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        TasksListViewHolder tasksListViewHolder = (TasksListViewHolder) holder;
        tasksListViewHolder.bind(position);
        tasksListViewHolder.applyExternalPositionModifier(position);
    }

    @Override
    public void setExternalPositionModifier(BiConsumer<View, Integer> positionModifier) {
        this.positionModifier = positionModifier;
    }

    @Override
    public int getItemCount() {
        return outputList.size();
    }

    @Override
    public Task getItem(int position) {
        return outputList.get(position);
    }

    @Override
    public int positionOf(Task task) {
        return outputList.indexOf(task);
    }


    @Override
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    @Override
    public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener) {
        this.onItemLongClickListener = onItemLongClickListener;
    }

    @Override
    public void highlightQuery(String query) {
        this.searchQuery = query.trim();

        // Each time the search query changes and highlights a new text -- but the item in the list
        // does not change position -- such item must be located and updated to show the new
        // highlighting
        List<ChangeDiff> changeDiffs = AdapterUtils.makeSearchSyntaxHighlightDiff(outputList, searchQuery);
        diffManager.notifySearchHighlightChanges(changeDiffs);
    }

    @Override
    public List<Task> getAdaptedList() {
        return outputList;
    }

    private record AdapterDiffManager(RecyclerView.Adapter<RecyclerView.ViewHolder> adapter,
                                      ScrollManager scrollManager) {
        public static final int NOTIFY_ITEM_CHANGED_THRESHOLD = 8;

        @SuppressLint("NotifyDataSetChanged")
        public void notifyDataSetDiffChanges(List<Diff> diffChanges) {
            /* The following threshold has been added in order to possibly avoid inconsistency bugs
             * which apparently affect RecyclerView instances. Notifying a lot of item changes may
             * produce undefined behaviors, making the application crash. Setting a threshold for the
             * maximum number of diff changes to notify can be a good trade-off between disabling
             * animations entirely and risking sudden application crashes.
             */
            if (diffChanges.size() < NOTIFY_ITEM_CHANGED_THRESHOLD) {
                diffChanges.forEach(diff -> {
                    if (diff.getPosition() == 0 && scrollManager.liveScrollOffsetWhenIdle().getValue() == 0) {
                        scrollManager.scrollToTop();
                    }

                    if (diff instanceof AddDiff) {
                        adapter.notifyItemInserted(diff.getPosition());
                    } else if (diff instanceof DelDiff) {
                        adapter.notifyItemRemoved(diff.getPosition());
                    } else if (diff instanceof ChangeDiff) {
                        adapter.notifyItemChanged(diff.getPosition());
                    } else if (diff instanceof MoveDiff moveDiff) {
                        adapter.notifyItemMoved(moveDiff.getPosition(), moveDiff.getAfterPosition());
                    }
                });
            } else {
                adapter.notifyDataSetChanged();
            }
        }

        public void notifySearchHighlightChanges(List<ChangeDiff> searchHighlightChanges) {
            searchHighlightChanges.forEach(changeDiff -> adapter.notifyItemChanged(changeDiff.getPosition()));
        }
    }
}
