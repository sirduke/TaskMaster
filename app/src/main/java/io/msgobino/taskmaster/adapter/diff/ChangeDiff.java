package io.msgobino.taskmaster.adapter.diff;

import androidx.annotation.NonNull;

public class ChangeDiff extends Diff {

    @NonNull
    @Override
    public String toString() {
        return "ChangeDiff -> Position " + getPosition();
    }

    public ChangeDiff(int position) {
        super(position);
    }
}
