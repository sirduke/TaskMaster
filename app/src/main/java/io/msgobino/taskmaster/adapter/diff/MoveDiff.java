package io.msgobino.taskmaster.adapter.diff;

import androidx.annotation.NonNull;

public class MoveDiff extends Diff {
    private final int afterPosition;

    public MoveDiff(int beforePosition, int afterPosition) {
        super(beforePosition);
        this.afterPosition = afterPosition;
    }

    @NonNull
    @Override
    public String toString() {
        return "MoveDiff -> Position " + getPosition() + " -> " + getAfterPosition();
    }

    public int getAfterPosition() {
        return afterPosition;
    }
}
