package io.msgobino.taskmaster.adapter;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.function.BiConsumer;

import io.msgobino.taskmaster.data.core.Task;
import io.msgobino.taskmaster.selection.SelectionHolder;

public abstract class ListAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements SelectionHolder.OnSelectionChangeListener {
    abstract public void setExternalPositionModifier(BiConsumer<View, Integer> positionModifier);

    abstract public T getItem(int position);

    abstract public int positionOf(T task);

    abstract public void highlightQuery(String query);

    abstract public List<Task> getAdaptedList();

    abstract public void setOnItemClickListener(OnItemClickListener onItemClickListener);

    abstract public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener);

    @FunctionalInterface
    public interface OnItemClickListener {
        void onClick(View view, int position);
    }

    @FunctionalInterface
    public interface OnItemLongClickListener {
        void onLongClick(View view, int position);
    }
}
