package io.msgobino.taskmaster.utils.factory;

@FunctionalInterface
public interface BiFactory<K, L, P> {
    P build(K k, L l);
}
