package io.msgobino.taskmaster.utils;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class ViewGroups {
    private ViewGroups() {
    }

    public static Stream<View> childrenStream(@NonNull ViewGroup group) {
        List<View> collectedViews = new ArrayList<>();
        for (int i = 0; i < group.getChildCount(); i++) {
            collectedViews.add(group.getChildAt(i));
        }
        return collectedViews.stream();
    }

    public static List<View> children(@NonNull ViewGroup group) {
        List<View> collectedViews = new ArrayList<>();
        for (int i = 0; i < group.getChildCount(); i++) {
            collectedViews.add(group.getChildAt(i));
        }
        return collectedViews;
    }
}

