package io.msgobino.taskmaster.utils.dialogs;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.textview.MaterialTextView;

import java.util.function.Consumer;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.TaskMasterApp;
import io.msgobino.taskmaster.adapter.FileListAdapter;
import io.msgobino.taskmaster.adapter.FileListLoader;
import io.msgobino.taskmaster.settings.KnownPreferencesManager;
import io.msgobino.taskmaster.utils.InputFilters;
import io.msgobino.taskmaster.utils.storage.SyncStorageDirectory;

public class FilePickerDialogBuilder {
    private final Context context;
    private final LifecycleOwner lifecycleOwner;
    private final Consumer<String> onPositiveClickInputConsumer;

    public FilePickerDialogBuilder(Context context, LifecycleOwner lifecycleOwner, Consumer<String> onPositiveClickInputConsumer) {
        this.context = context;
        this.lifecycleOwner = lifecycleOwner;
        this.onPositiveClickInputConsumer = onPositiveClickInputConsumer;
    }

    public AlertDialog build() {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(context);
        builder.setTitle(R.string.pick_file);
        builder.setIcon(R.drawable.file_open_24);

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View dialogView = setupFilePickerDialogView(layoutInflater, lifecycleOwner);
        builder.setView(dialogView);

        EditText editText = dialogView.findViewById(R.id.file_text_input_edit_text);
        builder.setPositiveButton(R.string.ok, (dialog, which) -> onPositiveClickInputConsumer.accept(editText.getText().toString()));
        builder.setNegativeButton(R.string.cancel, (dialog, which) -> dialog.cancel());

        return builder.create();
    }

    private static View setupFilePickerDialogView(LayoutInflater layoutInflater, LifecycleOwner lifecycleOwner) {
        View dialogView = layoutInflater.inflate(R.layout.file_list_layout, null);

        EditText editText = dialogView.findViewById(R.id.file_text_input_edit_text);

        editText.setFilters(new InputFilter[]{
                InputFilters.NEWLINE_FILTER,
                InputFilters.SPACE_FILTER
        });

        String nameFromPreferences = KnownPreferencesManager.getTodoTxtFilename();
        String initialText = nameFromPreferences.equals(KnownPreferencesManager.Defaults.TODO_TXT_FILENAME)
                ? ""
                : nameFromPreferences.replace(KnownPreferencesManager.Defaults.FILE_SUFFIX, "");
        editText.setText(initialText);

        RecyclerView recyclerView = dialogView.findViewById(R.id.file_list_recyclerview);

        String encodedUri = KnownPreferencesManager.getTodoTxtPath();
        SyncStorageDirectory syncDir = SyncStorageDirectory.fromEncodedUri(encodedUri);
        FileListLoader fileListLoader = new FileListLoader(syncDir);
        FileListAdapter fileListAdapter = new FileListAdapter(fileListLoader, lifecycleOwner);
        fileListAdapter.setOnClickListener((view, position) -> editText.setText(fileListAdapter.getPrefix(position)));
        fileListAdapter.updateHighlight(editText.getText().toString());

        recyclerView.setAdapter(fileListAdapter);

        LinearProgressIndicator lip = dialogView.findViewById(R.id.file_list_linear_progress_indicator);
        fileListLoader.getIsBusyListingFiles().observe(lifecycleOwner, isBusyListingFiles -> {
            if (isBusyListingFiles)
                lip.setVisibility(View.VISIBLE);
            else
                lip.setVisibility(View.GONE);
        });

        MaterialTextView filenameTextView = dialogView.findViewById(R.id.file_name_text_view);

        String initialFilenameIndicatorText = TaskMasterApp.getStringById(R.string.file_name) + ": " + nameFromPreferences;
        filenameTextView.setText(initialFilenameIndicatorText);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    String text = TaskMasterApp.getStringById(R.string.file_name) + ": " + s + KnownPreferencesManager.Defaults.FILE_SUFFIX;
                    filenameTextView.setText(text);
                } else {
                    String text = TaskMasterApp.getStringById(R.string.file_name) + ": " + KnownPreferencesManager.Defaults.TODO_TXT_FILENAME;
                    filenameTextView.setText(text);
                }
                fileListAdapter.updateHighlight(s.toString());
            }
        });

        return dialogView;
    }
}
