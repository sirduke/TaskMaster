package io.msgobino.taskmaster.utils;

import android.os.Handler;
import android.view.View;

import androidx.lifecycle.Observer;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import io.msgobino.taskmaster.utils.factory.Factory;

public class Animators {
    private Animators() {
    }

    public static class ScrollArrow {

        public static final int UP_ARROW_SHOW_DELAY = 1500;
        public static final int UP_ARROW_HIDE_DELAY = 500;
        public static final int UP_ARROW_Y_OFFSET = 3000;
        public static final Factory<FloatingActionButton, Observer<Integer>> IDLE_SCROLL_POSITION_OBSERVER_FACTORY
                = upArrowFab -> integer -> {
            if (integer > UP_ARROW_Y_OFFSET) {
                new Handler().postDelayed(
                        () -> animateUpArrowFab(upArrowFab, Status.VISIBLE),
                        UP_ARROW_SHOW_DELAY
                );
            } else {
                new Handler().postDelayed(
                        () -> animateUpArrowFab(upArrowFab, Status.GONE),
                        UP_ARROW_HIDE_DELAY
                );
            }
        };
        public static final int UP_ARROW_ANIM_IN_DURATION = 200;
        public static final int UP_ARROW_ANIM_OUT_DURATION = 200;

        public static void animateUpArrowFab(FloatingActionButton upArrowFab, Status status) {
            switch (status) {
                case VISIBLE -> {
                    upArrowFab.setVisibility(View.VISIBLE);
                    upArrowFab.animate()
                            .alpha(0.6f)
                            .setDuration(UP_ARROW_ANIM_IN_DURATION);
                }
                case GONE -> upArrowFab.animate()
                        .alpha(0.0f)
                        .setDuration(UP_ARROW_ANIM_OUT_DURATION)
                        .withEndAction(() -> upArrowFab.setVisibility(View.GONE));
            }
        }

        public enum Status {
            VISIBLE,
            GONE
        }
    }
}
