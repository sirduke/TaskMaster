package io.msgobino.taskmaster.utils.factory;

@FunctionalInterface
public interface Factory<K, P> {
    P build(K key);
}
