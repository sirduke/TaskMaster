package io.msgobino.taskmaster.utils;

import java.util.regex.Pattern;

public class RegularExpressions {
    public static final String DATE_REGEX = "\\d{4}-\\d{2}-\\d{2}";
    public static final String CONTEXT_REGEX = "@\\w+";
    public static final String PROJECT_REGEX = "\\+\\w+";
    public static final String CUSTOM_ATTR_REGEX = "\\w+:(\\S+)";
    public static final String PRIORITY_REGEX = "\\([A-Z]\\)";
    public static final String CASE_INSENSITIVE_PREFIX = "(?i)";
    public static final String WORD = "[a-zA-Z]+";

    public static class Precompiled {
        public static final Pattern CUSTOM_ATTR_PATTERN = Pattern.compile(RegularExpressions.CUSTOM_ATTR_REGEX);
        public static final Pattern CONTEXT_PATTERN = Pattern.compile(RegularExpressions.CONTEXT_REGEX);
        public static final Pattern PROJECT_PATTERN = Pattern.compile(RegularExpressions.PROJECT_REGEX);
    }

    private RegularExpressions(){}

    public static String toIgnorecaseRegex(String regex) {
        return CASE_INSENSITIVE_PREFIX + regex;
    }
}
