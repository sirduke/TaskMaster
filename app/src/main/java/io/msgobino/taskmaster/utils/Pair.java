package io.msgobino.taskmaster.utils;

public class Pair<T, S> {
    private final T t;
    private final S s;
    public Pair(T t, S s) {
        this.t = t;
        this.s = s;
    }
    public T first() {
        return t;
    }
    public S second() {
        return s;
    }
}
