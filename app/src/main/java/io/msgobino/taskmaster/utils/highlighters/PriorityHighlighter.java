package io.msgobino.taskmaster.utils.highlighters;

import android.text.Spannable;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.msgobino.taskmaster.TaskMasterApp;
import io.msgobino.taskmaster.data.analyzer.TaskAnalyzer;
import io.msgobino.taskmaster.utils.RegularExpressions;

public class PriorityHighlighter implements SyntaxHighlighter {
    @Override
    public void highlight(Spannable span) {
        String trimmedText = span.toString().trim();
        TaskAnalyzer analyzer = TaskAnalyzer.analyze(trimmedText);
        TaskAnalyzer.TaskShape taskShape = analyzer.getTaskShape();
        if (taskShape == TaskAnalyzer.TaskShape.PRI || taskShape == TaskAnalyzer.TaskShape.PRI_DATE) {
            Pattern pattern = Pattern.compile(RegularExpressions.PRIORITY_REGEX);
            Matcher matcher = pattern.matcher(span.toString());
            if (matcher.find()) {
                int start = matcher.start();
                int end = matcher.end();
                span.setSpan(new ForegroundColorSpan(TaskMasterApp.Colors.PRIORITY_COLOR.get()), start, end, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                span.setSpan(new StyleSpan(TaskMasterApp.TextStyles.PRIORITY_TYPEFACE.get()), start, end, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }
        }
    }
}
