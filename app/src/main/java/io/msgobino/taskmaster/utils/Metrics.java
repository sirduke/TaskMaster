package io.msgobino.taskmaster.utils;

import android.content.Context;
import android.util.DisplayMetrics;

public class Metrics {
    private Metrics() {
    }

    public static int trimValue(int value, int min, int max) {
        if (value < min)
            return min;
        else
            return Math.min(value, max);
    }


    public static float deviceHeightDp(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.heightPixels / displayMetrics.density;
    }


    public static float deviceWidthDp(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.widthPixels / displayMetrics.density;
    }
}
