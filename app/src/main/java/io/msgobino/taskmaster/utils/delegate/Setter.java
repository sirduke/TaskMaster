package io.msgobino.taskmaster.utils.delegate;

@FunctionalInterface
public interface Setter<T> {
    void set(T t);
}
