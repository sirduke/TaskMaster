package io.msgobino.taskmaster.utils.messages;

import io.msgobino.taskmaster.settings.KnownPreferencesManager;

public class Formatters {
    private Formatters() {
    }

    public static String surroundAdd(String toSurround, String toSurroundWith) {
        return toSurroundWith + toSurround + toSurroundWith;
    }

    public static String surroundBrackets(String toSurround) {
        return "(" + toSurround + ")";
    }

    public static String extractFilenamePrefix(String filename) {
        return filename.equals(KnownPreferencesManager.Defaults.TODO_TXT_FILENAME)
                ? ""
                : filename.replace(KnownPreferencesManager.Defaults.FILE_SUFFIX, "");
    }

    public static String appendFilenameSuffix(String prefix) {
        if (prefix.isEmpty())
            return KnownPreferencesManager.Defaults.TODO_TXT_FILENAME;
        else
            return prefix + KnownPreferencesManager.Defaults.FILE_SUFFIX;
    }

    ;
}
