package io.msgobino.taskmaster.utils.dialogs;

import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.List;
import java.util.function.Function;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.data.core.Task;

public class ConfirmationDialogBuilder {
    private final Context buildContext;
    private final List<Task> confirmedSelection;
    private final Function<Task, String> dialogTitleSingularGenerator;
    private final Function<Integer, String> dialogTitlePluralGenerator;
    private final Function<Task, String> dialogMessageSingularGenerator;
    private final Function<Integer, String> dialogMessagePluralGenerator;
    private final DialogInterface.OnClickListener onPositiveClickListener;
    private final DialogInterface.OnClickListener onNegativeClickListener;

    public ConfirmationDialogBuilder(Context buildContext, List<Task> confirmedSelection, Function<Task, String> dialogTitleSingularGenerator, Function<Integer, String> dialogTitlePluralGenerator, Function<Task, String> dialogMessageSingularGenerator, Function<Integer, String> dialogMessagePluralGenerator, DialogInterface.OnClickListener onPositiveClickListener, DialogInterface.OnClickListener onNegativeClickListener) {
        this.buildContext = buildContext;
        this.confirmedSelection = confirmedSelection;
        this.dialogTitleSingularGenerator = dialogTitleSingularGenerator;
        this.dialogTitlePluralGenerator = dialogTitlePluralGenerator;
        this.dialogMessageSingularGenerator = dialogMessageSingularGenerator;
        this.dialogMessagePluralGenerator = dialogMessagePluralGenerator;
        this.onPositiveClickListener = onPositiveClickListener;
        this.onNegativeClickListener = onNegativeClickListener;
    }

    public AlertDialog build() {
        String dialogTitle;
        String dialogMessage;
        if (confirmedSelection.size() < 2) {
            dialogTitle = dialogTitleSingularGenerator.apply(confirmedSelection.get(0));
            dialogMessage = dialogMessageSingularGenerator.apply(confirmedSelection.get(0));
        } else {
            dialogTitle = dialogTitlePluralGenerator.apply(confirmedSelection.size());
            dialogMessage = dialogMessagePluralGenerator.apply(confirmedSelection.size());
        }

        MaterialAlertDialogBuilder dialogBuilder = new MaterialAlertDialogBuilder(buildContext)
                .setTitle(dialogTitle)
                .setMessage(dialogMessage);

        dialogBuilder.setPositiveButton(R.string.ok, onPositiveClickListener);
        dialogBuilder.setNegativeButton(R.string.cancel, onNegativeClickListener);

        return dialogBuilder.create();
    }
}
