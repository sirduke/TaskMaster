package io.msgobino.taskmaster.utils.highlighters;

import android.text.Spannable;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;

import io.msgobino.taskmaster.TaskMasterApp;
import io.msgobino.taskmaster.data.analyzer.TaskAnalyzer;
import io.msgobino.taskmaster.utils.RegularExpressions;
import io.msgobino.taskmaster.utils.factory.Factory;

public class SyntaxHighlighting {
    public static final Factory<String, SyntaxHighlighter> SEARCH_QUERY_HIGHLIGHTER_FACTORY =
            query -> new KeywordHighlighter(
                    query,
                    TaskMasterApp.Colors.SEARCH_QUERY_COLOR,
                    TaskMasterApp.TextStyles.SEARCH_QUERY_TYPEFACE,
                    span -> (start, end) -> !Objects.equals(start, end)
            );

    public static void highlight(Spannable spannable) {
        List<SyntaxHighlighter> highlighters;
        if (!TaskAnalyzer.analyze(spannable.toString()).isActive()) {
            highlighters = List.of(span -> span.setSpan(new ForegroundColorSpan(TaskMasterApp.Colors.COMPLETED_TEXT_COLOR.get()), 0, span.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE));
        } else {
            highlighters = List.of(
                    span -> span.setSpan(new ForegroundColorSpan(TaskMasterApp.Colors.DEFAULT_TEXT_COLOR.get()), 0, span.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE),
                    new PriorityHighlighter(),
                    new CreationDateHighlighter(),
                    new KeywordHighlighter(RegularExpressions.CUSTOM_ATTR_REGEX, TaskMasterApp.Colors.CUSTOM_ATTR_COLOR, TaskMasterApp.TextStyles.CUSTOM_ATTR_TYPEFACE),
                    new KeywordHighlighter(RegularExpressions.PROJECT_REGEX, TaskMasterApp.Colors.PROJECT_COLOR, TaskMasterApp.TextStyles.PROJECT_TYPEFACE),
                    new KeywordHighlighter(RegularExpressions.CONTEXT_REGEX, TaskMasterApp.Colors.CONTEXT_COLOR, TaskMasterApp.TextStyles.CONTEXT_TYPEFACE)
            );
        }


        highlighters.forEach(h -> h.highlight(spannable));
    }

    public static void highlightQuery(Spannable span, String query) {
        List<String> queryTokens = Arrays.asList(query.split(" "));

        queryTokens.forEach(token -> SEARCH_QUERY_HIGHLIGHTER_FACTORY
                .build(RegularExpressions.toIgnorecaseRegex(Pattern.quote(token)))
                .highlight(span));
    }

    public static void removeSpans(Spannable spannable, Set<Class<?>> spans) {
        for (Class<?> span : spans) {
            Object[] detectedSpans = spannable.getSpans(0, spannable.length(), span);
            for (Object detectedSpan : detectedSpans) {
                spannable.removeSpan(detectedSpan);
            }
        }
    }

}
