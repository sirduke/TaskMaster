package io.msgobino.taskmaster.utils.highlighters;

import android.text.Spannable;

@FunctionalInterface
public interface SyntaxHighlighter {
    void highlight(Spannable span);
}
