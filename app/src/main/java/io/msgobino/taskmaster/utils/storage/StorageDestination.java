package io.msgobino.taskmaster.utils.storage;

import android.net.Uri;

import java.io.InputStream;
import java.io.OutputStream;

public interface StorageDestination extends Comparable<StorageDestination> {
    InputStream inputStream();

    OutputStream outputStream();

    void createDestination();

    Uri getFileUriInDir();

    String getFilename();
}
