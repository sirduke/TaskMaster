package io.msgobino.taskmaster.utils;

import android.text.InputFilter;

import io.msgobino.taskmaster.utils.factory.Factory;

public class InputFilters {
    private InputFilters() {
    }

    public static final Factory<String, InputFilter> TOKEN_REMOVING_INPUT_FILTER_FACTORY =
            chars -> (source, start,
                      end, destination,
                      destinationStart,
                      destinationEnd) -> {
                if (source != null) {
                    String s = source.toString();
                    if (s.contains(chars)) {
                        return s.replaceAll(chars, "");
                    }
                }
                return null;
            };

    public static final InputFilter NEWLINE_FILTER = TOKEN_REMOVING_INPUT_FILTER_FACTORY.build("\n");
    public static final InputFilter SPACE_FILTER = TOKEN_REMOVING_INPUT_FILTER_FACTORY.build(" ");
}
