package io.msgobino.taskmaster.utils.storage;

import android.net.Uri;
import android.util.Log;

import androidx.documentfile.provider.DocumentFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.msgobino.taskmaster.TaskMasterApp;

public class SyncStorageDirectory {
    private final DocumentFile dataDir;
    private final Uri uri;

    public SyncStorageDirectory(DocumentFile dataDir, Uri uri) {
        this.dataDir = dataDir;
        this.uri = uri;
    }

    public static SyncStorageDirectory fromEncodedUri(String encodedUri) {
        Uri uri = Uri.parse(encodedUri);
        DocumentFile dataDir = getDataDirFromUri(uri);
        return new SyncStorageDirectory(dataDir, uri);
    }


    public Uri getDirectoryUri() {
        return uri;
    }

    public StorageDestination buildStorageDestination(String todoTxtFileName) throws IOException {
        return new SyncableStorageDestination(this, todoTxtFileName);
    }

    public DocumentFile findFile(String displayName) {
        return dataDir.findFile(displayName);
    }

    public DocumentFile createFile(String mimeType, String displayName) {
        return dataDir.createFile(mimeType, displayName);
    }

    public boolean deleteFile(String displayName) {
        DocumentFile fileToDelete = dataDir.findFile(displayName);
        return fileToDelete != null && fileToDelete.delete();
    }

    public boolean exists() {
        return dataDir.exists();
    }


    private static DocumentFile getDataDirFromUri(Uri folderUri) {
        return DocumentFile.fromTreeUri(TaskMasterApp.getContext(), folderUri);
    }

    public List<String> listTodoTxtFiles() {
        List<String> fileNames = new ArrayList<>();
        DocumentFile[] files = dataDir.listFiles();

        for (DocumentFile file : files) {
            if (isTodoTxtFile(file)) {
                fileNames.add(file.getName());
            }
        }

        return fileNames;
    }

    public List<StorageDestination> getAllDestinations() {
        List<StorageDestination> destinations = new ArrayList<>();
        DocumentFile[] files = dataDir.listFiles();

        for (DocumentFile file : files) {
            if (isTodoTxtFile(file)) {
                try {
                    destinations.add(new SyncableStorageDestination(this, file.getName()));
                } catch (IOException e) {
                    Log.d("ERROR", e.getMessage() + "\nInvalid file name, not adding to destinations list.");
                }
            }
        }

        return destinations;
    }

    private static boolean isTodoTxtFile(DocumentFile file) {
        if (file.getName() != null)
            return file.getName().endsWith(".todo.txt") || file.getName().equals("todo.txt");
        else
            return false;
    }
}
