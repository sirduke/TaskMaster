package io.msgobino.taskmaster.utils.factory;

@FunctionalInterface
public interface TriFactory<J, K, L, P> {
    P build(J j, K k, L l);
}
