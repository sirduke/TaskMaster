package io.msgobino.taskmaster.utils.messages;

import android.view.View;

import com.google.android.material.snackbar.Snackbar;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.TaskMasterApp;
import io.msgobino.taskmaster.utils.factory.BiFactory;
import io.msgobino.taskmaster.utils.factory.TriFactory;
import io.msgobino.taskmaster.viewmodel.tasks.UndoController;

public class Snackbars {
    public static final int MESSAGE_MAX_LENGTH = 60;
    public static final String MESSAGE_SHORTENED_ENDING = "...";
    public static final TriFactory<View, String, UndoController, Snackbar> UNDO_SNACKBAR_FACTORY
            = (view, message, undoController) -> Snackbar.make(
                    view,
                    formatSnackbarMessage(message, MESSAGE_MAX_LENGTH, MESSAGE_SHORTENED_ENDING),
                    Snackbar.LENGTH_LONG
            ).setBackgroundTint(TaskMasterApp.getColorByAttr(R.attr.colorSnackbar))
            .setTextColor(TaskMasterApp.getColorByAttr(R.attr.colorOnSnackbar))
            .setActionTextColor(TaskMasterApp.getColorByAttr(com.google.android.material.R.attr.colorSecondary))
            .setAction(R.string.undo, v -> undoController.undo());

    public static final BiFactory<View, String, Snackbar> INFO_SNACKBAR_FACTORY
            = (view, message) -> Snackbar.make(view,
                    formatSnackbarMessage(message, MESSAGE_MAX_LENGTH, MESSAGE_SHORTENED_ENDING),
                    Snackbar.LENGTH_LONG
            ).setBackgroundTint(TaskMasterApp.getColorByAttr(R.attr.colorSnackbar))
            .setTextColor(TaskMasterApp.getColorByAttr(R.attr.colorOnSnackbar))
            .setActionTextColor(TaskMasterApp.getColorByAttr(com.google.android.material.R.attr.colorSecondary));

    public static String formatSnackbarMessage(String message, int maxLength, String ending) {
        if (message.length() > maxLength) {
            int actualMaxLength = maxLength - ending.length();
            String shortened = message.substring(0, actualMaxLength).trim();
            return shortened + ending;
        } else {
            return message;
        }
    }
}
