package io.msgobino.taskmaster.utils.storage;

import android.net.Uri;

import androidx.documentfile.provider.DocumentFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.TaskMasterApp;

public class SyncableStorageDestination implements StorageDestination {
    public static final int MAX_RETRY_ATTEMPTS = 5;
    private final SyncStorageDirectory syncDir;
    private final String fileName;
    private int retryAttempts = 0;

    public SyncableStorageDestination(SyncStorageDirectory syncDir, String fileName) throws IOException {
        this.syncDir = syncDir;
        if (fileName.trim().isEmpty())
            throw new IOException("Invalid file name");
        this.fileName = fileName;
    }

    @Override
    public InputStream inputStream() {
        try {
            InputStream is = TaskMasterApp.getContext()
                    .getContentResolver()
                    .openInputStream(getFileUriInDir());
            retryAttempts = 0;
            return is;
        } catch (FileNotFoundException e) {
            createDestination();
            retryAttempts++;
            if (retryAttempts < MAX_RETRY_ATTEMPTS)
                return inputStream();
            else
                throw new RuntimeException(e);
        }
    }

    @Override
    public OutputStream outputStream() {
        try {
            OutputStream os = TaskMasterApp.getContext()
                    .getContentResolver()
                    .openOutputStream(getFileUriInDir(), "wt");
            retryAttempts = 0;
            return os;
        } catch (FileNotFoundException e) {
            createDestination();
            retryAttempts++;
            if (retryAttempts < MAX_RETRY_ATTEMPTS)
                return outputStream();
            else
                throw new RuntimeException(e);
        }
    }

    @Override
    public void createDestination() {
        syncDir.createFile("text/plain", fileName);
    }

    @Override
    public Uri getFileUriInDir() {
        DocumentFile todoTxtDocumentFile = syncDir.findFile(fileName);
        if (Objects.isNull(todoTxtDocumentFile)) {
            createDestination();
            todoTxtDocumentFile = syncDir.findFile(fileName);
        }
        if (todoTxtDocumentFile != null) {
            return todoTxtDocumentFile.getUri();
        } else {
            return Uri.EMPTY;
        }
    }

    @Override
    public String getFilename() {
        return fileName;
    }

    @Override
    public int compareTo(StorageDestination o) {
        String default_filename = TaskMasterApp.getStringById(R.string.todo_txt_default_filename);
        if (getFilename().equals(default_filename))
            return -1;
        else if (o.getFilename().equals(default_filename))
            return 1;
        else
            return getFilename().compareToIgnoreCase(o.getFilename());
    }
}
