package io.msgobino.taskmaster.utils.highlighters;

import android.text.Spannable;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;

import java.util.function.BiFunction;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.msgobino.taskmaster.utils.factory.Factory;

public class KeywordHighlighter implements SyntaxHighlighter {
    private final String regex;
    private final Supplier<Integer> colorSupplier;
    private final Supplier<Integer> styleSupplier;
    public static final Factory<Spannable, BiFunction<Integer, Integer, Boolean>> IS_TOKEN_CHECKER_FACTORY = span ->
            (start, end) -> (start < 1 || span.toString().charAt(start - 1) == ' ')
                    && (end >= span.toString().length() || span.toString().charAt(end) == ' ');
    private Factory<Spannable, BiFunction<Integer, Integer, Boolean>> checkerFactory = IS_TOKEN_CHECKER_FACTORY;

    public KeywordHighlighter(String regex, Supplier<Integer> colorSupplier, Supplier<Integer> styleSupplier) {
        this.regex = regex;
        this.colorSupplier = colorSupplier;
        this.styleSupplier = styleSupplier;
    }

    public KeywordHighlighter(String regex,
                              Supplier<Integer> colorSupplier,
                              Supplier<Integer> styleSupplier,
                              Factory<Spannable, BiFunction<Integer, Integer, Boolean>> checkerFactory
    ) {
        this.regex = regex;
        this.colorSupplier = colorSupplier;
        this.styleSupplier = styleSupplier;
        this.checkerFactory = checkerFactory;
    }

    @Override
    public void highlight(Spannable span) {
        if (!regex.isEmpty()) {
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(span.toString());
            while (matcher.find()) {
                int start = matcher.start();
                int end = matcher.end();

                boolean validKeyword = checkerFactory.build(span)
                        .apply(start, end);

                if (validKeyword) {
                    if (colorSupplier.get() != null)
                        span.setSpan(new ForegroundColorSpan(colorSupplier.get()), start, end, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                    if (styleSupplier.get() != null)
                        span.setSpan(new StyleSpan(styleSupplier.get()), start, end, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                }
            }
        }
    }
}
