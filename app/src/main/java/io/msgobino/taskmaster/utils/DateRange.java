package io.msgobino.taskmaster.utils;

import androidx.annotation.NonNull;

import java.time.LocalDate;

public record DateRange(LocalDate start, LocalDate end) {
    public DateRange(LocalDate start, LocalDate end) {
        this.start = start;
        if (start.isAfter(end))
            this.end = start;
        else
            this.end = end;
    }

    public boolean isInRange(LocalDate localDate) {
        return (localDate.equals(start) || localDate.isAfter(start))
                && (localDate.equals(end) || localDate.isBefore(end));
    }

    public boolean singleDayRange() {
        return start.equals(end);
    }

    public boolean startsToday() {
        return start.equals(LocalDate.now());
    }

    public int startDaysFromToday() {
        return (int) (start.toEpochDay() - LocalDate.now().toEpochDay());
    }

    public int endDaysFromToday() {
        return (int) (end.toEpochDay() - LocalDate.now().toEpochDay());
    }

    @NonNull
    @Override
    public String toString() {
        return singleDayRange()
                ? start.toString()
                : start.toString() + " " + end.toString();
    }
}
