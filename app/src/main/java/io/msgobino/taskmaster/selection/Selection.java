package io.msgobino.taskmaster.selection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

public class Selection {
    private final Set<Integer> selectedItems;
    private int maxSelected = 0;

    public Selection() {
        this.selectedItems = new HashSet<>();
    }

    public boolean selected(int position) {
        return selectedItems.contains(position);
    }

    public void select(int position) {
        if (maxSelected < position)
            maxSelected = position;
        selectedItems.add(position);
    }

    public void deselect(int position) {
        selectedItems.remove(position);
    }

    public void selectUpTo(int upTo) {
        if (maxSelected < upTo)
            maxSelected = upTo;
        for (int i = 0; i < upTo; i++) {
            selectedItems.add(i);
        }
    }

    public List<Integer> getSelectionPositions() {
        List<Integer> selections = new ArrayList<>();
        IntStream.range(0, maxSelected + 1)
                .filter(this::selected)
                .forEach(selections::add);
        return selections;
    }

    public void clear() {
        selectedItems.clear();
    }

    public int count() {
        return selectedItems.size();
    }
}
