package io.msgobino.taskmaster.selection;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import io.msgobino.taskmaster.adapter.ListAdapter;
import io.msgobino.taskmaster.data.core.Task;

public class SelectionHolder {
    private ListAdapter<Task> decoratedAdapter;
    private final Selection selection;
    private final Set<OnSelectionChangeListener> listeners = new HashSet<>();

    public SelectionHolder() {
        this.selection = new Selection();
    }

    public void attachListAdapter(ListAdapter<Task> adapter) {
        decoratedAdapter = adapter;
    }

    public List<Task> getSelectedItems() {
        if (decoratedAdapter == null)
            return List.of();
        return decoratedAdapter.getAdaptedList().stream()
                .filter(this::selected)
                .collect(Collectors.toList());

    }

    public boolean selected(int position) {
        return selection.selected(position);
    }

    public boolean selected(Task task) {
        if (decoratedAdapter == null)
            return false;
        return selection.selected(decoratedAdapter.positionOf(task));
    }

    public void selectItem(int position) {
        selection.select(position);
        listeners.forEach(listener -> listener.onSelectionChange(List.of(position)));
    }

    public void deselectItem(int position) {
        selection.deselect(position);
        listeners.forEach(listener -> listener.onSelectionChange(List.of(position)));
    }

    public void selectAll() {
        if (decoratedAdapter == null)
            return;
        List<Integer> previousSelections = selection.getSelectionPositions();
        int upTo = decoratedAdapter.getItemCount();
        selection.selectUpTo(upTo);
        List<Integer> selectionsDiff = selection.getSelectionPositions();
        selectionsDiff.removeAll(previousSelections);
        listeners.forEach(listener -> listener.onSelectionChange(selectionsDiff));
    }

    public void clearSelection() {
        List<Integer> previousPositions = selection.getSelectionPositions();
        selection.clear();
        listeners.forEach(listener -> listener.onSelectionChange(previousPositions));
    }

    public int selectedCount() {
        return selection.count();
    }

    public void addOnSelectionChangeListener(OnSelectionChangeListener onSelectionChangeListener) {
        listeners.add(onSelectionChangeListener);
    }

    public void removeOnSelectionChangeListener(OnSelectionChangeListener onSelectionChangeListener) {
        listeners.remove(onSelectionChangeListener);
    }

    public interface OnSelectionChangeListener {
        void onSelectionChange(List<Integer> positions);
    }
}
