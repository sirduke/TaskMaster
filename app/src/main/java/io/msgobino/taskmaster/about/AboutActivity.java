package io.msgobino.taskmaster.about;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.Objects;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.TaskMasterApp;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        TaskMasterApp.loadTheme(this);
        setContentView(R.layout.about_activity);

        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        Toolbar toolbar = findViewById(R.id.about_toolbar);
        setSupportActionBar(toolbar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        toolbar.setNavigationOnClickListener(
                v -> finish()
        );
        super.onStart();
    }
}
