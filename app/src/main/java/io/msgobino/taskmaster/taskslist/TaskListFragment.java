package io.msgobino.taskmaster.taskslist;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Comparator;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.TaskMasterApp;
import io.msgobino.taskmaster.adapter.ListAdapter;
import io.msgobino.taskmaster.adapter.TaskListAdapter;
import io.msgobino.taskmaster.data.core.Task;
import io.msgobino.taskmaster.data.core.TaskUtils;
import io.msgobino.taskmaster.data.grader.CachedFuzzyTaskGrader;
import io.msgobino.taskmaster.data.grader.GraderComparator;
import io.msgobino.taskmaster.selection.SelectionHolder;
import io.msgobino.taskmaster.utils.Animators;
import io.msgobino.taskmaster.utils.messages.Formatters;
import io.msgobino.taskmaster.utils.messages.Snackbars;
import io.msgobino.taskmaster.viewmodel.search.SearchViewModel;
import io.msgobino.taskmaster.viewmodel.selection.SelectionViewModel;
import io.msgobino.taskmaster.viewmodel.tasks.SortController;
import io.msgobino.taskmaster.viewmodel.tasks.TaskController;
import io.msgobino.taskmaster.viewmodel.tasks.TasksViewModel;
import io.msgobino.taskmaster.viewmodel.tasks.UndoController;

public class TaskListFragment extends Fragment {
    public static final String TAG = "TASK_LIST_FRAGMENT";
    private TaskController taskController;
    private UndoController historyController;
    private SortController sortController;
    private ListAdapter<Task> listAdapter;
    private SelectionHolder selectionHolder;
    private Consumer<Task> openTaskEditorFor = task -> {
    };
    private ScrollManager scrollManager;

    public TaskListFragment() {
    }

    public static final String REQUEST_EDIT_TASK_KEY = "EDIT_TASK";
    public static final String BUNDLE_KEY_TARGET_TASK = "TARGET_TASK_CONTENT";
    public static final String BUNDLE_KEY_NEW_TASK = "NEW_TASK_CONTENT";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setupEditorFragmentResultListener();

        super.onCreate(savedInstanceState);
    }

    private void setupEditorFragmentResultListener() {
        BiConsumer<Task, Task> taskEditorStringProcessor = (toEdit, newTask) -> {
            String actualDescription = getString(R.string.creating_task) + " " + Formatters.surroundAdd(newTask.content(), "'");
            if (!toEdit.equals(Task.NULL_TASK))
                actualDescription = getString(R.string.editing_task) + " " + Formatters.surroundAdd(toEdit.content(), "'");

            String prefix;
            if (toEdit.equals(Task.NULL_TASK)) {
                taskController.createTask(newTask);
                prefix = getString(R.string.created_task);
            } else {
                taskController.replaceTask(toEdit, newTask);
                prefix = getString(R.string.edited_with);
            }

            Snackbars.UNDO_SNACKBAR_FACTORY.build(requireView(), prefix + " " + Formatters.surroundAdd(newTask.content(), "'"), historyController)
                    .show();

            historyController.takeSnapshot(actualDescription);
        };

        getParentFragmentManager().setFragmentResultListener(REQUEST_EDIT_TASK_KEY, this, (requestKey, result) -> {
            String targetTaskContent = result.getString(BUNDLE_KEY_TARGET_TASK, Task.NULL_TASK.content())
                    .trim();
            String newTaskContent = result.getString(BUNDLE_KEY_NEW_TASK, Task.NULL_TASK.content())
                    .trim();
            taskEditorStringProcessor.accept(TaskUtils.formatted(targetTaskContent), TaskUtils.formatted(newTaskContent));
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.task_list_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        TasksViewModel viewModel = new ViewModelProvider(requireActivity()).get(TasksViewModel.class);
        taskController = viewModel.getTaskController();
        historyController = viewModel.getHistoryController();
        sortController = viewModel.getSortController();

        RecyclerView recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        scrollManager = new ScrollManager(recyclerView);
        FloatingActionButton upArrowFab = view.findViewById(R.id.recyclerview_up_arrow_fab);
        upArrowFab.setOnClickListener(v -> {
            Animators.ScrollArrow.animateUpArrowFab(upArrowFab, Animators.ScrollArrow.Status.GONE);
            scrollManager.scrollToTop();
        });
        scrollManager.liveScrollOffsetWhenIdle().observe(getViewLifecycleOwner(),
                Animators.ScrollArrow.IDLE_SCROLL_POSITION_OBSERVER_FACTORY.build(upArrowFab));

        listAdapter = new TaskListAdapter(
                view.getContext(),
                viewModel.getLiveList(),
                this,
                scrollManager);

        listAdapter.setOnItemClickListener(onAdapterItemClickListener);
        listAdapter.setOnItemLongClickListener(onAdapterItemLongClickListener);

        SelectionViewModel selectionViewModel = new ViewModelProvider(requireActivity()).get(SelectionViewModel.class);
        selectionViewModel.attachListAdapter(listAdapter);
        selectionHolder = selectionViewModel.getSelectionHolder();

        listAdapter.setExternalPositionModifier((v, p) -> {
            if (selectionHolder.selectedCount() > 0 && selectionHolder.selected(p))
                v.setBackgroundColor(TaskMasterApp.getColorByAttr(R.attr.selectedListItemBackground));
            else
                v.setBackgroundColor(TaskMasterApp.getColorByAttr(R.attr.defaultListItemBackground));
        });

        recyclerView.setAdapter(listAdapter);

        SearchViewModel searchViewModel = new ViewModelProvider(requireActivity()).get(SearchViewModel.class);
        searchViewModel.getLiveSearchQuery().observe(getViewLifecycleOwner(), this::onSearchQueryChanged);

        onSearchQueryChanged(searchViewModel.getLiveSearchQuery().getValue());

        super.onViewCreated(view, savedInstanceState);
    }


    public void setOpenEditorConsumer(Consumer<Task> openEditorConsumer) {
        this.openTaskEditorFor = openEditorConsumer;
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onStart() {
        listAdapter.notifyDataSetChanged();
        super.onStart();
    }

    private final ListAdapter.OnItemClickListener onAdapterItemClickListener = (view, position) -> {
        if (selectionHolder.selectedCount() > 0) {
            if (selectionHolder.selected(position)) {
                selectionHolder.deselectItem(position);
            } else {
                selectionHolder.selectItem(position);
            }

        } else {
            openTaskEditorFor.accept(listAdapter.getItem(position));
        }
    };

    private final ListAdapter.OnItemLongClickListener onAdapterItemLongClickListener
            = (view, position) -> selectionHolder.selectItem(position);

    private void onSearchQueryChanged(String searchQuery) {
        scrollManager.scrollToTop();
        setComparatorForSearchQuery(searchQuery);
        listAdapter.highlightQuery(searchQuery);
    }

    private void setComparatorForSearchQuery(String searchQuery) {
        Comparator<Task> comparator;
        if (searchQuery.isEmpty())
            comparator = Comparator.naturalOrder();
        else
            comparator = new GraderComparator(new CachedFuzzyTaskGrader(searchQuery))
                    .thenComparing(Comparator.naturalOrder());
        sortController.sortWith(comparator);
    }
}
