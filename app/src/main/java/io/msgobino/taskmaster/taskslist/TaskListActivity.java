package io.msgobino.taskmaster.taskslist;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Objects;

import io.msgobino.taskmaster.R;
import io.msgobino.taskmaster.TaskMasterApp;
import io.msgobino.taskmaster.data.core.Task;
import io.msgobino.taskmaster.editor.TaskEditorFragment;
import io.msgobino.taskmaster.selection.SelectionHolder;
import io.msgobino.taskmaster.settings.KnownPreferencesManager;
import io.msgobino.taskmaster.topbar.TopBarFragment;
import io.msgobino.taskmaster.utils.messages.Snackbars;
import io.msgobino.taskmaster.viewmodel.search.SearchViewModel;
import io.msgobino.taskmaster.viewmodel.selection.SelectionViewModel;
import io.msgobino.taskmaster.viewmodel.tasks.TasksViewModel;
import io.msgobino.taskmaster.welcome.FolderLoaderActivity;

public class TaskListActivity extends AppCompatActivity {
    private TasksViewModel viewModel;
    private SelectionViewModel selectionViewModel;
    private SelectionHolder.OnSelectionChangeListener hideFabOnSelectionChangeListener;
    public static final int FAB_ANIMATION_DURATION = 200;
    private DrawerLayout drawerLayout;
    private TaskMasterApp.Theme theme;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TaskMasterApp.loadTheme(this);
        theme = TaskMasterApp.getCurrentTheme();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String todoTxtFolderEncodedUri = KnownPreferencesManager.getTodoTxtPath();
        if (Objects.isNull(todoTxtFolderEncodedUri)) {
            getToFolderLoaderPage();
        }

        viewModel = new ViewModelProvider(this).get(TasksViewModel.class);
        viewModel.getHistoryController()
                .setNotificationsProducer(s -> Snackbars.INFO_SNACKBAR_FACTORY.build((View) findViewById(R.id.central_task_list_fragment_container), s).show());
        viewModel.setOnSyncFailureStrategy(e -> {
            Toast.makeText(this, getString(R.string.failed_to_sync_due_to) + " " + e.getMessage(), Toast.LENGTH_LONG)
                    .show();
            getToFolderLoaderPage();
        });

        new ViewModelProvider(this).get(SearchViewModel.class);
        selectionViewModel = new ViewModelProvider(this).get(SelectionViewModel.class);
        selectionViewModel.init();

        getOnBackPressedDispatcher().addCallback(this, new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                SearchView searchView = findViewById(R.id.app_bar_search);
                if (Objects.nonNull(drawerLayout) && drawerLayout.isOpen()) {
                    drawerLayout.closeDrawers();
                } else if (selectionViewModel.getSelectionHolder().selectedCount() > 0) {
                    selectionViewModel.getSelectionHolder().clearSelection();
                } else if (Objects.nonNull(searchView) && !searchView.isIconified()) {
                    searchView.setQuery("", true);
                    searchView.setIconified(true);
                } else {
                    finishAffinity();
                }
            }
        });

        LinearLayout noItemsLayout = findViewById(R.id.no_item_linear_layout);
        viewModel.getLiveList().observe(this, taskList -> {
            if (taskList.isEmpty())
                noItemsLayout.setVisibility(View.VISIBLE);
            else if (noItemsLayout.getVisibility() == View.VISIBLE)
                noItemsLayout.setVisibility(View.GONE);
        });
    }


    private void openTaskEditorFor(Task task) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment previous = getSupportFragmentManager().findFragmentByTag(TaskEditorFragment.TAG);
        if (previous != null) {
            transaction.remove(previous);
        }
        transaction.addToBackStack(null);

        BottomSheetDialogFragment dialogFragment = TaskEditorFragment.getInstance(task.content());
        dialogFragment.show(transaction, TaskEditorFragment.TAG);
    }

    private void getToFolderLoaderPage() {
        Intent getToFolderLoaderPage = new Intent(TaskListActivity.this, FolderLoaderActivity.class);
        startActivity(getToFolderLoaderPage);
    }

    @Override
    protected void onStart() {
        TaskListFragment taskListFragment = (TaskListFragment) getSupportFragmentManager().findFragmentByTag(TaskListFragment.TAG);
        taskListFragment.setOpenEditorConsumer(this::openTaskEditorFor);

        drawerLayout = findViewById(R.id.drawer_layout);
        TopBarFragment topBarFragment = (TopBarFragment) getSupportFragmentManager().findFragmentByTag(TopBarFragment.TAG);
        topBarFragment.setSyncDrawerWithActionBar(toolbar -> {
            if (toolbar != null) {
                if (Objects.nonNull(drawerLayout)) {
                    ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.opens_the_drawer, R.string.closes_the_drawer);
                    drawerLayout.addDrawerListener(actionBarDrawerToggle);
                    actionBarDrawerToggle.syncState();
                }
            }
        });

        viewModel.loadFromSettings();
        super.onStart();
    }

    @Override
    protected void onResume() {
        if (TaskMasterApp.getCurrentTheme() != theme) {
            recreate();
        }

        FloatingActionButton addTaskFab = findViewById(R.id.fab_add_task);
        addTaskFab.setOnClickListener(v -> openTaskEditorFor(Task.NULL_TASK));
        hideFabOnSelectionChangeListener = positions -> {
            if (selectionViewModel.getSelectionHolder().selectedCount() > 0) {
                addTaskFab.setAlpha(1.0f);
                addTaskFab.animate()
                        .alpha(0.0f)
                        .setDuration(FAB_ANIMATION_DURATION)
                        .withEndAction(() -> addTaskFab.setVisibility(View.GONE));
            } else {
                addTaskFab.setAlpha(0.0f);
                addTaskFab.setVisibility(View.VISIBLE);
                addTaskFab.animate()
                        .alpha(1.0f)
                        .setDuration(FAB_ANIMATION_DURATION);
            }
        };
        selectionViewModel.getSelectionHolder()
                .addOnSelectionChangeListener(hideFabOnSelectionChangeListener);
        viewModel.fetch();
        super.onResume();
    }

    @Override
    protected void onPause() {
        selectionViewModel.getSelectionHolder()
                .removeOnSelectionChangeListener(hideFabOnSelectionChangeListener);
        super.onPause();
    }

}
