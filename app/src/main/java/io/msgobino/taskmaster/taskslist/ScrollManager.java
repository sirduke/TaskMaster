package io.msgobino.taskmaster.taskslist;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

public class ScrollManager {
    private final RecyclerView recyclerView;
    private int offset = 0;
    private final MutableLiveData<Integer> realTimeScrollOffset = new MutableLiveData<>(0);
    private final MutableLiveData<Integer> scrollOffsetWhenIdle = new MutableLiveData<>(0);

    public ScrollManager(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        RecyclerView.OnScrollListener scrollWatchdog = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                offset = recyclerView.computeVerticalScrollOffset();
                // sends event to livedata observers only when offset reaches zero
                if (offset == 0) {
                    scrollOffsetWhenIdle.postValue(offset);
                }
                realTimeScrollOffset.postValue(offset);
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                // sends event to livedata observers only when scroll state is stopped
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    scrollOffsetWhenIdle.setValue(offset);
                }
            }
        };
        recyclerView.addOnScrollListener(scrollWatchdog);
    }

    public LiveData<Integer> liveRealTimeScrollOffset() {
        return realTimeScrollOffset;
    }

    public LiveData<Integer> liveScrollOffsetWhenIdle() {
        return scrollOffsetWhenIdle;
    }

    public void scrollToTop() {
        scrollToPosition(0);
    }

    public void scrollToPosition(int position) {
        recyclerView.scrollToPosition(position);
        realTimeScrollOffset.postValue(position);
        scrollOffsetWhenIdle.postValue(position);
    }


}
