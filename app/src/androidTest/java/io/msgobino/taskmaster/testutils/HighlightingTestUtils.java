package io.msgobino.taskmaster.testutils;

import android.content.Context;
import android.text.Spannable;
import android.widget.TextView;

import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Assert;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import io.msgobino.taskmaster.utils.highlighters.SyntaxHighlighter;

public class HighlightingTestUtils {
    private HighlightingTestUtils() {
    }

    public static <T> List<T> getSpansList(Spannable span, int start, int end, Class<T> cls) {
        Object[] foregroundColorSpans = span.getSpans(start, end, cls);
        return Arrays.stream(foregroundColorSpans)
                .map(o -> (T) o)
                .collect(Collectors.toList());
    }

    public static Spannable highlightWith(String text, SyntaxHighlighter highlighter) {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        TextView textView = new TextView(appContext);
        textView.setText(text, TextView.BufferType.EDITABLE);
        highlighter.highlight(textView.getEditableText());

        return textView.getEditableText();
    }

    public static void checkSpans(Spannable span, int start, int end, int howManyExpected, List<Class<?>> classes) {
        classes.forEach(clazz -> Assert.assertEquals(howManyExpected,
                HighlightingTestUtils.getSpansList(span, start, end, clazz).size()));
    }
}
