package io.msgobino.taskmaster;

import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.time.LocalDate;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.msgobino.taskmaster.data.core.Task;
import io.msgobino.taskmaster.testutils.HighlightingTestUtils;
import io.msgobino.taskmaster.utils.highlighters.CreationDateHighlighter;
import io.msgobino.taskmaster.utils.highlighters.KeywordHighlighter;
import io.msgobino.taskmaster.utils.highlighters.PriorityHighlighter;
import io.msgobino.taskmaster.utils.highlighters.SyntaxHighlighting;

@RunWith(AndroidJUnit4.class)
public class HighlightingTests {
    @Test
    public void priorityHighlighting() {
        Task taskWithPriority = new Task("(A) this task has a priority");
        Spannable span = HighlightingTestUtils.highlightWith(taskWithPriority.content(), new PriorityHighlighter());
        HighlightingTestUtils.checkSpans(span, 0, 3, 1, List.of(ForegroundColorSpan.class, StyleSpan.class));
        HighlightingTestUtils.checkSpans(span, 3, span.length(), 0, List.of(ForegroundColorSpan.class, StyleSpan.class));
    }

    @Test
    public void priorityNoHighlighting() {
        Task taskWithoutPriority = new Task("this task does not have a priority");
        Spannable span = HighlightingTestUtils.highlightWith(taskWithoutPriority.content(), new PriorityHighlighter());
        HighlightingTestUtils.checkSpans(span, 0, span.length(), 0, List.of(ForegroundColorSpan.class, StyleSpan.class));
    }


    @Test
    public void priorityHighlightingWithBait() {
        Task taskWithoutPriority = new Task("this (C) task does not have a priority");
        Spannable span = HighlightingTestUtils.highlightWith(taskWithoutPriority.content(), new PriorityHighlighter());
        HighlightingTestUtils.checkSpans(span, 0, span.length(), 0, List.of(ForegroundColorSpan.class, StyleSpan.class));
    }

    @Test
    public void priorityHighlightingMalformed() {
        Task taskWithoutPriority = new Task("(C)this task does not have a priority because it's malformed");
        Spannable span = HighlightingTestUtils.highlightWith(taskWithoutPriority.content(), new PriorityHighlighter());
        HighlightingTestUtils.checkSpans(span, 0, span.length(), 0, List.of(ForegroundColorSpan.class, StyleSpan.class));
    }

    @Test
    public void creationDateHighlighting() {
        Task taskWithCreationDate = new Task(LocalDate.now() + " this task has a creation date");
        Spannable span = HighlightingTestUtils.highlightWith(taskWithCreationDate.content(), new CreationDateHighlighter());

        HighlightingTestUtils.checkSpans(span, 0, 10, 1, List.of(ForegroundColorSpan.class, StyleSpan.class));
        HighlightingTestUtils.checkSpans(span, 10, span.length(), 0, List.of(ForegroundColorSpan.class, StyleSpan.class));
    }

    @Test
    public void creationDateNoHighlighting() {
        Task taskWithoutCreationDate = new Task("this task does not have a creation date");
        Spannable span = HighlightingTestUtils.highlightWith(taskWithoutCreationDate.content(), new CreationDateHighlighter());

        HighlightingTestUtils.checkSpans(span, 0, span.length(), 0, List.of(ForegroundColorSpan.class, StyleSpan.class));
    }

    @Test
    public void creationDateHighlightingWithPriority() {
        Task taskWithCreationDate = new Task("(B) " + LocalDate.now() + " this task has a creation date and a priority");
        Spannable span = HighlightingTestUtils.highlightWith(taskWithCreationDate.content(), new CreationDateHighlighter());

        HighlightingTestUtils.checkSpans(span, 4, 14, 1, List.of(ForegroundColorSpan.class, StyleSpan.class));
        HighlightingTestUtils.checkSpans(span, 14, span.length(), 0, List.of(ForegroundColorSpan.class, StyleSpan.class));
    }

    @Test
    public void creationDateHighlightingWithBait() {
        Task taskWithoutCreationDate = new Task("this " + LocalDate.now() + " task does not have a creation date");
        Spannable span = HighlightingTestUtils.highlightWith(taskWithoutCreationDate.content(), new CreationDateHighlighter());

        HighlightingTestUtils.checkSpans(span, 0, span.length(), 0, List.of(ForegroundColorSpan.class, StyleSpan.class));
    }

    @Test
    public void creationDateHighlightingMalformed() {
        Task taskWithoutCreationDate = new Task(LocalDate.now() + "this task does not have a creation date because it's malformed");
        Spannable span = HighlightingTestUtils.highlightWith(taskWithoutCreationDate.content(), new CreationDateHighlighter());

        HighlightingTestUtils.checkSpans(span, 0, span.length(), 0, List.of(ForegroundColorSpan.class, StyleSpan.class));
    }

    @Test
    public void keywordHighlighting() {
        String keyword = "keyword";
        Task taskWithKeyword = new Task("This task has a special keyword to match");
        Spannable span = HighlightingTestUtils.highlightWith(taskWithKeyword.content(), new CreationDateHighlighter());

        Pattern pattern = Pattern.compile(keyword);
        Matcher matcher = pattern.matcher(taskWithKeyword.content());
        matcher.find();
        int keywordStart = matcher.start();
        int keywordEnd = matcher.end();

        new KeywordHighlighter(keyword, () -> 0, () -> 0)
                .highlight(span);

        HighlightingTestUtils.checkSpans(span, keywordStart, keywordEnd, 1, List.of(ForegroundColorSpan.class, StyleSpan.class));
        HighlightingTestUtils.checkSpans(span, keywordEnd, span.length(), 0, List.of(ForegroundColorSpan.class, StyleSpan.class));
    }

    @Test
    public void textHighlighter() {
        String keyword = "a";
        Task taskWithKeyword = new Task("This task has a special keyword to match");
        Spannable span = HighlightingTestUtils.highlightWith(taskWithKeyword.content(), new CreationDateHighlighter());

        new KeywordHighlighter(keyword, () -> 0, () -> 0, key -> (a, b) -> true)
                .highlight(span);

        HighlightingTestUtils.checkSpans(span, 0, span.length(), 5, List.of(ForegroundColorSpan.class, StyleSpan.class));

        Pattern pattern = Pattern.compile(keyword);
        Matcher matcher = pattern.matcher(taskWithKeyword.content());
        while (matcher.find()) {
            int keywordStart = matcher.start();
            int keywordEnd = matcher.end();

            HighlightingTestUtils.checkSpans(span, keywordStart, keywordEnd, 1, List.of(ForegroundColorSpan.class, StyleSpan.class));
        }
    }


    @Test
    public void completedHighlighter() {
        Task completedTask = new Task("x This task has been completed");
        Spannable span = HighlightingTestUtils.highlightWith(completedTask.content(), SyntaxHighlighting::highlight);

        HighlightingTestUtils.checkSpans(span, 0, span.length(), 1, List.of(ForegroundColorSpan.class));
        HighlightingTestUtils.checkSpans(span, 0, span.length(), 0, List.of(StyleSpan.class));
    }
}
