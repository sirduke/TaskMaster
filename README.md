**TaskMaster**, a small [todo.txt](https://github.com/todotxt/todo.txt) manager
app for Android.

I started this app as an university project (University of Trieste). It has
been funny and I decided to further work on it.

The application has few features by design, but provides a nice and solid
workflow for inserting and retrieving tasks.

Bring your own synchronization system, if needed. I personally suggest
[syncthing](https://github.com/syncthing/syncthing).

# Features

- Compose tasks with ease using dynamic buttons designed to quickly insert
  content
- Rapidly find tasks with search or included filters
- Manage tasks across files
- Dark mode
- Multiple themes to suit your tastes

![](./assets/readme-main-screens.jpg)

# Roadmap

- [X] First Java version
- [X] Add empty-content background graphics
- [X] Improve file picker design
- [ ] Add more notable themes
- [ ] **Publish to F-Droid**
- [ ] Migrate Java code to Kotlin
- [ ] Custom regex-based filters
- [ ] Bulk editor
- [ ] Design automatic notification system based on task content
- [ ] Migrate auxiliary screens to Jetpack Compose
- [ ] ...
- [ ] Migrate to Jetpack Compose if opportune
